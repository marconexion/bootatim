/*
    / \     _   |_|
   / _ \  _| |_  _  _____
  / /_\ \(_   _)| ||     |
 / _____ \ | |  | || | | |
/_/     \_\\__) |_||_|_|_|
    (C)2018 Atim

Description: Application

License:

Maintainer: Youen Lebret

 */

/****************************************************************************************
 * Include files
 ****************************************************************************************/
#include "ble.h"
#include "ble_status.h"
#include "bluenrg1_api.h"
#include "bluenrg1_events.h"
#include "bluenrg1_gap.h"
#include "bluenrg1_hal.h"
#include "bluenrg1_gatt_server.h"
#include <string.h>

/****************************************************************************************
 * Macro definitions
 ****************************************************************************************/


/****************************************************************************************
 * Define definitions
 ****************************************************************************************/


/****************************************************************************************
 * Static/global definitions
 ****************************************************************************************/


/****************************************************************************************
 * Static Function prototypes
 ****************************************************************************************/


/****************************************************************************************
 * Static Function Definition
 ****************************************************************************************/


/****************************************************************************************
 * Public Function Definitions
 ****************************************************************************************/


/****************************************************************************************
 * Define definitions
 ****************************************************************************************/


/****************************************************************************************
 * Type definitions
 ****************************************************************************************/


/****************************************************************************************
 * Function prototypes
 ****************************************************************************************/
SECTION(".ble_api")
tBleStatus BLE_hci_disconnect(uint16_t Connection_Handle,
                          uint8_t Reason)
{
    tBleStatus lReturn;

    lReturn = hci_disconnect(Connection_Handle,
            Reason);

    return lReturn;
}

SECTION(".ble_api")
tBleStatus BLE_hci_read_remote_version_information(uint16_t Connection_Handle)
{
    tBleStatus lReturn;

    lReturn = hci_read_remote_version_information(Connection_Handle);

    return lReturn;
}

SECTION(".ble_api")
tBleStatus BLE_hci_set_event_mask(uint8_t Event_Mask[8])
{
    tBleStatus lReturn;

    lReturn = hci_set_event_mask(Event_Mask);

    return lReturn;
}

SECTION(".ble_api")
tBleStatus BLE_hci_reset(void)
{
    tBleStatus lReturn;

    lReturn = hci_reset();

    return lReturn;
}

SECTION(".ble_api")
tBleStatus BLE_hci_read_transmit_power_level(uint16_t Connection_Handle,
                                         uint8_t Type,
                                         uint8_t *Transmit_Power_Level)
{
    tBleStatus lReturn;

    lReturn = hci_read_transmit_power_level(Connection_Handle,
            Type,
            Transmit_Power_Level);

    return lReturn;
}

SECTION(".ble_api")
tBleStatus BLE_hci_read_local_version_information(uint8_t *HCI_Version,
                                              uint16_t *HCI_Revision,
                                              uint8_t *LMP_PAL_Version,
                                              uint16_t *Manufacturer_Name,
                                              uint16_t *LMP_PAL_Subversion)
{
    tBleStatus lReturn;

    lReturn = hci_read_local_version_information(HCI_Version,
            HCI_Revision,
            LMP_PAL_Version,
            Manufacturer_Name,
            LMP_PAL_Subversion);

    return lReturn;
}

SECTION(".ble_api")
tBleStatus BLE_hci_read_local_supported_commands(uint8_t Supported_Commands[64])
{
    tBleStatus lReturn;

    lReturn = hci_read_local_supported_commands(Supported_Commands);

    return lReturn;
}

SECTION(".ble_api")
tBleStatus BLE_hci_read_local_supported_features(uint8_t LMP_Features[8])
{
    tBleStatus lReturn;

    lReturn = hci_read_local_supported_features(LMP_Features);

    return lReturn;
}

SECTION(".ble_api")
tBleStatus BLE_hci_read_bd_addr(uint8_t BD_ADDR[6])
{
    tBleStatus lReturn;

    lReturn = hci_read_bd_addr(BD_ADDR);

    return lReturn;
}

SECTION(".ble_api")
tBleStatus BLE_hci_read_rssi(uint16_t Connection_Handle,
                         uint8_t *RSSI)
{
    tBleStatus lReturn;

    lReturn = hci_read_rssi(Connection_Handle,
            RSSI);

    return lReturn;
}

SECTION(".ble_api")
tBleStatus BLE_hci_le_set_event_mask(uint8_t LE_Event_Mask[8])
{
    tBleStatus lReturn;

    lReturn = hci_le_set_event_mask(LE_Event_Mask);

    return lReturn;
}

SECTION(".ble_api")
tBleStatus BLE_hci_le_read_buffer_size(uint16_t *HC_LE_ACL_Data_Packet_Length,
                                   uint8_t *HC_Total_Num_LE_ACL_Data_Packets)
{
    tBleStatus lReturn;

    lReturn = hci_le_read_buffer_size(HC_LE_ACL_Data_Packet_Length,
            HC_Total_Num_LE_ACL_Data_Packets);

    return lReturn;
}

SECTION(".ble_api")
tBleStatus BLE_hci_le_read_local_supported_features(uint8_t LE_Features[8])
{
    tBleStatus lReturn;

    lReturn = hci_le_read_local_supported_features(LE_Features);

    return lReturn;
}

SECTION(".ble_api")
tBleStatus BLE_hci_le_set_random_address(uint8_t Random_Address[6])
{
    tBleStatus lReturn;

    lReturn = hci_le_set_random_address(Random_Address);

    return lReturn;
}

SECTION(".ble_api")
tBleStatus BLE_hci_le_set_advertising_parameters(uint16_t Advertising_Interval_Min,
                                             uint16_t Advertising_Interval_Max,
                                             uint8_t Advertising_Type,
                                             uint8_t Own_Address_Type,
                                             uint8_t Peer_Address_Type,
                                             uint8_t Peer_Address[6],
                                             uint8_t Advertising_Channel_Map,
                                             uint8_t Advertising_Filter_Policy)
{
    tBleStatus lReturn;

    lReturn = hci_le_set_advertising_parameters(Advertising_Interval_Min,
            Advertising_Interval_Max,
            Advertising_Type,
            Own_Address_Type,
            Peer_Address_Type,
            Peer_Address,
            Advertising_Channel_Map,
            Advertising_Filter_Policy);

    return lReturn;
}

SECTION(".ble_api")
tBleStatus BLE_hci_le_read_advertising_channel_tx_power(uint8_t *Transmit_Power_Level)
{
    tBleStatus lReturn;

    lReturn = hci_le_read_advertising_channel_tx_power(Transmit_Power_Level);

    return lReturn;
}

SECTION(".ble_api")
tBleStatus BLE_hci_le_set_advertising_data(uint8_t Advertising_Data_Length,
                                       uint8_t Advertising_Data[31])
{
    tBleStatus lReturn;

    lReturn = hci_le_set_advertising_data(Advertising_Data_Length,
            Advertising_Data);

    return lReturn;
}

SECTION(".ble_api")
tBleStatus BLE_hci_le_set_scan_response_data(uint8_t Scan_Response_Data_Length,
                                         uint8_t Scan_Response_Data[31])
{
    tBleStatus lReturn;

    lReturn = hci_le_set_scan_response_data(Scan_Response_Data_Length,
            Scan_Response_Data);

    return lReturn;
}

SECTION(".ble_api")
tBleStatus BLE_hci_le_set_advertise_enable(uint8_t Advertising_Enable)
{
    tBleStatus lReturn;

    lReturn = hci_le_set_advertise_enable(Advertising_Enable);

    return lReturn;
}

SECTION(".ble_api")
tBleStatus BLE_hci_le_set_scan_parameters(uint8_t LE_Scan_Type,
                                      uint16_t LE_Scan_Interval,
                                      uint16_t LE_Scan_Window,
                                      uint8_t Own_Address_Type,
                                      uint8_t Scanning_Filter_Policy)
{
    tBleStatus lReturn;

    lReturn = hci_le_set_scan_parameters(LE_Scan_Type,
            LE_Scan_Interval,
            LE_Scan_Window,
            Own_Address_Type,
            Scanning_Filter_Policy);

    return lReturn;
}

SECTION(".ble_api")
tBleStatus BLE_hci_le_set_scan_enable(uint8_t LE_Scan_Enable,
                                  uint8_t Filter_Duplicates)
{
    tBleStatus lReturn;

    lReturn = hci_le_set_scan_enable(LE_Scan_Enable,
            Filter_Duplicates);

    return lReturn;
}

SECTION(".ble_api")
tBleStatus BLE_hci_le_create_connection(uint16_t LE_Scan_Interval,
                                    uint16_t LE_Scan_Window,
                                    uint8_t Initiator_Filter_Policy,
                                    uint8_t Peer_Address_Type,
                                    uint8_t Peer_Address[6],
                                    uint8_t Own_Address_Type,
                                    uint16_t Conn_Interval_Min,
                                    uint16_t Conn_Interval_Max,
                                    uint16_t Conn_Latency,
                                    uint16_t Supervision_Timeout,
                                    uint16_t Minimum_CE_Length,
                                    uint16_t Maximum_CE_Length)
{
    tBleStatus lReturn;

    lReturn = hci_le_create_connection(LE_Scan_Interval,
            LE_Scan_Window,
            Initiator_Filter_Policy,
            Peer_Address_Type,
            Peer_Address,
            Own_Address_Type,
            Conn_Interval_Min,
            Conn_Interval_Max,
            Conn_Latency,
            Supervision_Timeout,
            Minimum_CE_Length,
            Maximum_CE_Length);

    return lReturn;
}

SECTION(".ble_api")
tBleStatus BLE_hci_le_create_connection_cancel(void)
{
    tBleStatus lReturn;

    lReturn = hci_le_create_connection_cancel();

    return lReturn;
}

SECTION(".ble_api")
tBleStatus BLE_hci_le_read_white_list_size(uint8_t *White_List_Size)
{
    tBleStatus lReturn;

    lReturn = hci_le_read_white_list_size(White_List_Size);

    return lReturn;
}

SECTION(".ble_api")
tBleStatus BLE_hci_le_clear_white_list(void)
{
    tBleStatus lReturn;

    lReturn = hci_le_clear_white_list();

    return lReturn;
}

SECTION(".ble_api")
tBleStatus BLE_hci_le_add_device_to_white_list(uint8_t Address_Type,
                                           uint8_t Address[6])
{
    tBleStatus lReturn;

    lReturn = hci_le_add_device_to_white_list(Address_Type,
            Address);

    return lReturn;
}

SECTION(".ble_api")
tBleStatus BLE_hci_le_remove_device_from_white_list(uint8_t Address_Type,
                                                uint8_t Address[6])
{
    tBleStatus lReturn;

    lReturn = hci_le_remove_device_from_white_list(Address_Type,
            Address);

    return lReturn;
}

SECTION(".ble_api")
tBleStatus BLE_hci_le_connection_update(uint16_t Connection_Handle,
                                    uint16_t Conn_Interval_Min,
                                    uint16_t Conn_Interval_Max,
                                    uint16_t Conn_Latency,
                                    uint16_t Supervision_Timeout,
                                    uint16_t Minimum_CE_Length,
                                    uint16_t Maximum_CE_Length)
{
    tBleStatus lReturn;

    lReturn = hci_le_connection_update(Connection_Handle,
            Conn_Interval_Min,
            Conn_Interval_Max,
            Conn_Latency,
            Supervision_Timeout,
            Minimum_CE_Length,
            Maximum_CE_Length);

    return lReturn;
}

SECTION(".ble_api")
tBleStatus BLE_hci_le_set_host_channel_classification(uint8_t LE_Channel_Map[5])
{
    tBleStatus lReturn;

    lReturn = hci_le_set_host_channel_classification(LE_Channel_Map);

    return lReturn;
}

SECTION(".ble_api")
tBleStatus BLE_hci_le_read_channel_map(uint16_t Connection_Handle,
                                   uint8_t LE_Channel_Map[5])
{
    tBleStatus lReturn;

    lReturn = hci_le_read_channel_map(Connection_Handle,
            LE_Channel_Map);

    return lReturn;
}

SECTION(".ble_api")
tBleStatus BLE_hci_le_read_remote_used_features(uint16_t Connection_Handle)
{
    tBleStatus lReturn;

    lReturn = hci_le_read_remote_used_features(Connection_Handle);

    return lReturn;
}

SECTION(".ble_api")
tBleStatus BLE_hci_le_encrypt(uint8_t Key[16],
                          uint8_t Plaintext_Data[16],
                          uint8_t Encrypted_Data[16])
{
    tBleStatus lReturn;

    lReturn = hci_le_encrypt(Key,
            Plaintext_Data,
            Encrypted_Data);

    return lReturn;
}

SECTION(".ble_api")
tBleStatus BLE_hci_le_rand(uint8_t Random_Number[8])
{
    tBleStatus lReturn;

    lReturn = hci_le_rand(Random_Number);

    return lReturn;
}

SECTION(".ble_api")
tBleStatus BLE_hci_le_start_encryption(uint16_t Connection_Handle,
                                   uint8_t Random_Number[8],
                                   uint16_t Encrypted_Diversifier,
                                   uint8_t Long_Term_Key[16])
{
    tBleStatus lReturn;

    lReturn = hci_le_start_encryption(Connection_Handle,
            Random_Number,
            Encrypted_Diversifier,
            Long_Term_Key);

    return lReturn;
}

SECTION(".ble_api")
tBleStatus BLE_hci_le_long_term_key_request_reply(uint16_t Connection_Handle,
                                              uint8_t Long_Term_Key[16])
{
    tBleStatus lReturn;

    lReturn = hci_le_long_term_key_request_reply(Connection_Handle,
            Long_Term_Key);

    return lReturn;
}

SECTION(".ble_api")
tBleStatus BLE_hci_le_long_term_key_requested_negative_reply(uint16_t Connection_Handle)
{
    tBleStatus lReturn;

    lReturn = hci_le_long_term_key_requested_negative_reply(Connection_Handle);

    return lReturn;
}

SECTION(".ble_api")
tBleStatus BLE_hci_le_read_supported_states(uint8_t LE_States[8])
{
    tBleStatus lReturn;

    lReturn = hci_le_read_supported_states(LE_States);

    return lReturn;
}

SECTION(".ble_api")
tBleStatus BLE_hci_le_set_data_length(uint16_t Connection_Handle,
                                  uint16_t TxOctets,
                                  uint16_t TxTime)
{
    tBleStatus lReturn;

    lReturn = hci_le_set_data_length(Connection_Handle,
            TxOctets,
            TxTime);

    return lReturn;
}

SECTION(".ble_api")
tBleStatus BLE_hci_le_read_suggested_default_data_length(uint16_t *SuggestedMaxTxOctets,
                                                     uint16_t *SuggestedMaxTxTime)
{
    tBleStatus lReturn;

    lReturn = hci_le_read_suggested_default_data_length(SuggestedMaxTxOctets,
            SuggestedMaxTxTime);

    return lReturn;
}

SECTION(".ble_api")
tBleStatus BLE_hci_le_write_suggested_default_data_length(uint16_t SuggestedMaxTxOctets,
                                                      uint16_t SuggestedMaxTxTime)
{
    tBleStatus lReturn;

    lReturn = hci_le_write_suggested_default_data_length(SuggestedMaxTxOctets,
            SuggestedMaxTxTime);

    return lReturn;
}

SECTION(".ble_api")
tBleStatus BLE_hci_le_read_local_p256_public_key(void)
{
    tBleStatus lReturn;

    lReturn = hci_le_read_local_p256_public_key();

    return lReturn;
}

SECTION(".ble_api")
tBleStatus BLE_hci_le_generate_dhkey(uint8_t Remote_P256_Public_Key[64])
{
    tBleStatus lReturn;

    lReturn = hci_le_generate_dhkey(Remote_P256_Public_Key);

    return lReturn;
}

SECTION(".ble_api")
tBleStatus BLE_hci_le_add_device_to_resolving_list(uint8_t Peer_Identity_Address_Type,
                                               uint8_t Peer_Identity_Address[6],
                                               uint8_t Peer_IRK[16],
                                               uint8_t Local_IRK[16])
{
    tBleStatus lReturn;

    lReturn = hci_le_add_device_to_resolving_list(Peer_Identity_Address_Type,
            Peer_Identity_Address,
            Peer_IRK,
            Local_IRK);

    return lReturn;
}

SECTION(".ble_api")
tBleStatus BLE_hci_le_remove_device_from_resolving_list(uint8_t Peer_Identity_Address_Type,
                                                    uint8_t Peer_Identity_Address[6])
{
    tBleStatus lReturn;

    lReturn = hci_le_remove_device_from_resolving_list(Peer_Identity_Address_Type,
            Peer_Identity_Address);

    return lReturn;
}

SECTION(".ble_api")
tBleStatus BLE_hci_le_clear_resolving_list(void)
{
    tBleStatus lReturn;

    lReturn = hci_le_clear_resolving_list();

    return lReturn;
}

SECTION(".ble_api")
tBleStatus BLE_hci_le_read_resolving_list_size(uint8_t *Resolving_List_Size)
{
    tBleStatus lReturn;

    lReturn = hci_le_read_resolving_list_size(Resolving_List_Size);

    return lReturn;
}

SECTION(".ble_api")
tBleStatus BLE_hci_le_read_peer_resolvable_address(uint8_t Peer_Identity_Address_Type,
                                               uint8_t Peer_Identity_Address[6],
                                               uint8_t Peer_Resolvable_Address[6])
{
    tBleStatus lReturn;

    lReturn = hci_le_read_peer_resolvable_address(Peer_Identity_Address_Type,
            Peer_Identity_Address,
            Peer_Resolvable_Address);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_hci_le_read_local_resolvable_address(uint8_t Peer_Identity_Address_Type,
                                                uint8_t Peer_Identity_Address[6],
                                                uint8_t Local_Resolvable_Address[6])
{
    tBleStatus lReturn;

    lReturn = hci_le_read_local_resolvable_address(Peer_Identity_Address_Type,
            Peer_Identity_Address,
            Local_Resolvable_Address);

    return lReturn;
}
SECTION(".ble_api")tBleStatus BLE_hci_le_set_address_resolution_enable(uint8_t Address_Resolution_Enable)
{
    tBleStatus lReturn;

    lReturn = hci_le_set_address_resolution_enable(Address_Resolution_Enable);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_hci_le_set_resolvable_private_address_timeout(uint16_t RPA_Timeout)
{
    tBleStatus lReturn;

    lReturn = hci_le_set_resolvable_private_address_timeout(RPA_Timeout);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_hci_le_read_maximum_data_length(uint16_t *supportedMaxTxOctets,
                                           uint16_t *supportedMaxTxTime,
                                           uint16_t *supportedMaxRxOctets,
                                           uint16_t *supportedMaxRxTime)
{
    tBleStatus lReturn;

    lReturn = hci_le_read_maximum_data_length(supportedMaxTxOctets,
            supportedMaxTxTime,
            supportedMaxRxOctets,
            supportedMaxRxTime);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_hci_le_receiver_test(uint8_t RX_Frequency)
{
    tBleStatus lReturn;

    lReturn = hci_le_receiver_test(RX_Frequency);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_hci_le_transmitter_test(uint8_t TX_Frequency,
                                   uint8_t Length_Of_Test_Data,
                                   uint8_t Packet_Payload)
{
    tBleStatus lReturn;

    lReturn = hci_le_transmitter_test(TX_Frequency,
            Length_Of_Test_Data,
            Packet_Payload);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_hci_le_test_end(uint16_t *Number_Of_Packets)
{
    tBleStatus lReturn;

    lReturn = hci_le_test_end(Number_Of_Packets);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_hal_get_fw_build_number(uint16_t *Build_Number)
{
    tBleStatus lReturn;

    lReturn = aci_hal_get_fw_build_number(Build_Number);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_hal_write_config_data(uint8_t Offset,
                                     uint8_t Length,
                                     uint8_t Value[])
{
    tBleStatus lReturn;

    lReturn = aci_hal_write_config_data(Offset,
            Length,
            Value);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_hal_read_config_data(uint8_t Offset,
                                    uint8_t *Data_Length,
                                    uint8_t Data[])
{
    tBleStatus lReturn;

    lReturn = aci_hal_read_config_data(Offset,
            Data_Length,
            Data);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_hal_set_tx_power_level(uint8_t En_High_Power,
                                      uint8_t PA_Level)
{
    tBleStatus lReturn;

    lReturn = aci_hal_set_tx_power_level(En_High_Power,
            PA_Level);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_hal_le_tx_test_packet_number(uint32_t *Number_Of_Packets)
{
    tBleStatus lReturn;

    lReturn = aci_hal_le_tx_test_packet_number(Number_Of_Packets);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_hal_tone_start(uint8_t RF_Channel,
                              uint8_t Offset)
{
    tBleStatus lReturn;

    lReturn = aci_hal_tone_start(RF_Channel,
            Offset);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_hal_tone_stop(void)
{
    tBleStatus lReturn;

    lReturn = aci_hal_tone_stop();

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_hal_get_link_status(uint8_t Link_Status[8],
                                   uint16_t Link_Connection_Handle[16 / 2])
{
    tBleStatus lReturn;

    lReturn = aci_hal_get_link_status(Link_Status,
            Link_Connection_Handle);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_hal_set_radio_activity_mask(uint16_t Radio_Activity_Mask)
{
    tBleStatus lReturn;

    lReturn = aci_hal_set_radio_activity_mask(Radio_Activity_Mask);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_hal_get_anchor_period(uint32_t *Anchor_Period,
                                     uint32_t *Max_Free_Slot)
{
    tBleStatus lReturn;

    lReturn = aci_hal_get_anchor_period(Anchor_Period,
            Max_Free_Slot);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_hal_set_event_mask(uint32_t Event_Mask)
{
    tBleStatus lReturn;

    lReturn = aci_hal_set_event_mask(Event_Mask);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gap_set_non_discoverable(void)
{
    tBleStatus lReturn;

    lReturn = aci_gap_set_non_discoverable();

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gap_set_limited_discoverable(uint8_t Advertising_Type,
                                            uint16_t Advertising_Interval_Min,
                                            uint16_t Advertising_Interval_Max,
                                            uint8_t Own_Address_Type,
                                            uint8_t Advertising_Filter_Policy,
                                            uint8_t Local_Name_Length,
                                            uint8_t Local_Name[],
                                            uint8_t Service_Uuid_length,
                                            uint8_t Service_Uuid_List[],
                                            uint16_t Slave_Conn_Interval_Min,
                                            uint16_t Slave_Conn_Interval_Max)
{
    tBleStatus lReturn;

    lReturn = aci_gap_set_limited_discoverable(Advertising_Type,
            Advertising_Interval_Min,
            Advertising_Interval_Max,
            Own_Address_Type,
            Advertising_Filter_Policy,
            Local_Name_Length,
            Local_Name,
            Service_Uuid_length,
            Service_Uuid_List,
            Slave_Conn_Interval_Min,
            Slave_Conn_Interval_Max);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gap_set_discoverable(uint8_t Advertising_Type,
                                    uint16_t Advertising_Interval_Min,
                                    uint16_t Advertising_Interval_Max,
                                    uint8_t Own_Address_Type,
                                    uint8_t Advertising_Filter_Policy,
                                    uint8_t Local_Name_Length,
                                    uint8_t Local_Name[],
                                    uint8_t Service_Uuid_length,
                                    uint8_t Service_Uuid_List[],
                                    uint16_t Slave_Conn_Interval_Min,
                                    uint16_t Slave_Conn_Interval_Max)
{
    tBleStatus lReturn;

    lReturn = aci_gap_set_discoverable(Advertising_Type,
            Advertising_Interval_Min,
            Advertising_Interval_Max,
            Own_Address_Type,
            Advertising_Filter_Policy,
            Local_Name_Length,
            Local_Name,
            Service_Uuid_length,
            Service_Uuid_List,
            Slave_Conn_Interval_Min,
            Slave_Conn_Interval_Max);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gap_set_direct_connectable(uint8_t Own_Address_Type,
                                          uint8_t Directed_Advertising_Type,
                                          uint8_t Direct_Address_Type,
                                          uint8_t Direct_Address[6],
                                          uint16_t Advertising_Interval_Min,
                                          uint16_t Advertising_Interval_Max)
{
    tBleStatus lReturn;

    lReturn = aci_gap_set_direct_connectable(Own_Address_Type,
            Directed_Advertising_Type,
            Direct_Address_Type,
            Direct_Address,
            Advertising_Interval_Min,
            Advertising_Interval_Max);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gap_set_io_capability(uint8_t IO_Capability)
{
    tBleStatus lReturn;

    lReturn = aci_gap_set_io_capability(IO_Capability);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gap_set_authentication_requirement(uint8_t Bonding_Mode,
                                                  uint8_t MITM_Mode,
                                                  uint8_t SC_Support,
                                                  uint8_t KeyPress_Notification_Support,
                                                  uint8_t Min_Encryption_Key_Size,
                                                  uint8_t Max_Encryption_Key_Size,
                                                  uint8_t Use_Fixed_Pin,
                                                  uint32_t Fixed_Pin,
                                                  uint8_t Identity_Address_Type)
{
    tBleStatus lReturn;

    lReturn = aci_gap_set_authentication_requirement(Bonding_Mode,
            MITM_Mode,
            SC_Support,
            KeyPress_Notification_Support,
            Min_Encryption_Key_Size,
            Max_Encryption_Key_Size,
            Use_Fixed_Pin,
            Fixed_Pin,
            Identity_Address_Type);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gap_set_authorization_requirement(uint16_t Connection_Handle,
                                                 uint8_t Authorization_Enable)
{
    tBleStatus lReturn;

    lReturn = aci_gap_set_authorization_requirement(Connection_Handle,
            Authorization_Enable);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gap_pass_key_resp(uint16_t Connection_Handle,
                                 uint32_t Pass_Key)
{
    tBleStatus lReturn;

    lReturn = aci_gap_pass_key_resp(Connection_Handle,
            Pass_Key);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gap_authorization_resp(uint16_t Connection_Handle,
                                      uint8_t Authorize)
{
    tBleStatus lReturn;

    lReturn = aci_gap_authorization_resp(Connection_Handle,
            Authorize);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gap_init(uint8_t Role,
                        uint8_t privacy_enabled,
                        uint8_t device_name_char_len,
                        uint16_t *Service_Handle,
                        uint16_t *Dev_Name_Char_Handle,
                        uint16_t *Appearance_Char_Handle)
{
    tBleStatus lReturn;

    lReturn = aci_gap_init(Role,
            privacy_enabled,
            device_name_char_len,
            Service_Handle,
            Dev_Name_Char_Handle,
            Appearance_Char_Handle);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gap_set_non_connectable(uint8_t Advertising_Event_Type,
                                       uint8_t Own_Address_Type)
{
    tBleStatus lReturn;

    lReturn = aci_gap_set_non_connectable(Advertising_Event_Type,
            Own_Address_Type);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gap_set_undirected_connectable(uint16_t Advertising_Interval_Min,
                                              uint16_t Advertising_Interval_Max,
                                              uint8_t Own_Address_Type,
                                              uint8_t Adv_Filter_Policy)
{
    tBleStatus lReturn;

    lReturn = aci_gap_set_undirected_connectable(Advertising_Interval_Min,
            Advertising_Interval_Max,
            Own_Address_Type,
            Adv_Filter_Policy);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gap_slave_security_req(uint16_t Connection_Handle)
{
    tBleStatus lReturn;

    lReturn = aci_gap_slave_security_req(Connection_Handle);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gap_update_adv_data(uint8_t AdvDataLen,
                                   uint8_t AdvData[])
{
    tBleStatus lReturn;

    lReturn = aci_gap_update_adv_data(AdvDataLen,
            AdvData);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gap_delete_ad_type(uint8_t ADType)
{
    tBleStatus lReturn;

    lReturn = aci_gap_delete_ad_type(ADType);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gap_get_security_level(uint16_t Connection_Handle,
                                      uint8_t *Security_Mode,
                                      uint8_t *Security_Level)
{
    tBleStatus lReturn;

    lReturn = aci_gap_get_security_level(Connection_Handle,
            Security_Mode,
            Security_Level);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gap_set_event_mask(uint16_t GAP_Evt_Mask)
{
    tBleStatus lReturn;

    lReturn = aci_gap_set_event_mask(GAP_Evt_Mask);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gap_configure_whitelist(void)
{
    tBleStatus lReturn;

    lReturn = aci_gap_configure_whitelist();

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gap_terminate(uint16_t Connection_Handle,
                             uint8_t Reason)
{
    tBleStatus lReturn;

    lReturn = aci_gap_terminate(Connection_Handle,
            Reason);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gap_clear_security_db(void)
{
    tBleStatus lReturn;

    lReturn = aci_gap_clear_security_db();

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gap_allow_rebond(uint16_t Connection_Handle)
{
    tBleStatus lReturn;

    lReturn = aci_gap_allow_rebond(Connection_Handle);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gap_start_limited_discovery_proc(uint16_t LE_Scan_Interval,
                                                uint16_t LE_Scan_Window,
                                                uint8_t Own_Address_Type,
                                                uint8_t Filter_Duplicates)
{
    tBleStatus lReturn;

    lReturn = aci_gap_start_limited_discovery_proc(LE_Scan_Interval,
            LE_Scan_Window,
            Own_Address_Type,
            Filter_Duplicates);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gap_start_general_discovery_proc(uint16_t LE_Scan_Interval,
                                                uint16_t LE_Scan_Window,
                                                uint8_t Own_Address_Type,
                                                uint8_t Filter_Duplicates)
{
    tBleStatus lReturn;

    lReturn = aci_gap_start_general_discovery_proc(LE_Scan_Interval,
            LE_Scan_Window,
            Own_Address_Type,
            Filter_Duplicates);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gap_start_name_discovery_proc(uint16_t LE_Scan_Interval,
                                             uint16_t LE_Scan_Window,
                                             uint8_t Peer_Address_Type,
                                             uint8_t Peer_Address[6],
                                             uint8_t Own_Address_Type,
                                             uint16_t Conn_Interval_Min,
                                             uint16_t Conn_Interval_Max,
                                             uint16_t Conn_Latency,
                                             uint16_t Supervision_Timeout,
                                             uint16_t Minimum_CE_Length,
                                             uint16_t Maximum_CE_Length)
{
    tBleStatus lReturn;

    lReturn = aci_gap_start_name_discovery_proc(LE_Scan_Interval,
            LE_Scan_Window,
            Peer_Address_Type,
            Peer_Address,
            Own_Address_Type,
            Conn_Interval_Min,
            Conn_Interval_Max,
            Conn_Latency,
            Supervision_Timeout,
            Minimum_CE_Length,
            Maximum_CE_Length);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gap_start_auto_connection_establish_proc(uint16_t LE_Scan_Interval,
                                                        uint16_t LE_Scan_Window,
                                                        uint8_t Own_Address_Type,
                                                        uint16_t Conn_Interval_Min,
                                                        uint16_t Conn_Interval_Max,
                                                        uint16_t Conn_Latency,
                                                        uint16_t Supervision_Timeout,
                                                        uint16_t Minimum_CE_Length,
                                                        uint16_t Maximum_CE_Length,
                                                        uint8_t Num_of_Whitelist_Entries,
                                                        Whitelist_Entry_t Whitelist_Entry[])
{
    tBleStatus lReturn;

    lReturn = aci_gap_start_auto_connection_establish_proc(LE_Scan_Interval,
            LE_Scan_Window,
            Own_Address_Type,
            Conn_Interval_Min,
            Conn_Interval_Max,
            Conn_Latency,
            Supervision_Timeout,
            Minimum_CE_Length,
            Maximum_CE_Length,
            Num_of_Whitelist_Entries,
            Whitelist_Entry);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gap_start_general_connection_establish_proc(uint8_t LE_Scan_Type,
                                                           uint16_t LE_Scan_Interval,
                                                           uint16_t LE_Scan_Window,
                                                           uint8_t Own_Address_Type,
                                                           uint8_t Scanning_Filter_Policy,
                                                           uint8_t Filter_Duplicates)
{
    tBleStatus lReturn;

    lReturn = aci_gap_start_general_connection_establish_proc(LE_Scan_Type,
            LE_Scan_Interval,
            LE_Scan_Window,
            Own_Address_Type,
            Scanning_Filter_Policy,
            Filter_Duplicates);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gap_start_selective_connection_establish_proc(uint8_t LE_Scan_Type,
                                                             uint16_t LE_Scan_Interval,
                                                             uint16_t LE_Scan_Window,
                                                             uint8_t Own_Address_Type,
                                                             uint8_t Scanning_Filter_Policy,
                                                             uint8_t Filter_Duplicates,
                                                             uint8_t Num_of_Whitelist_Entries,
                                                             Whitelist_Entry_t Whitelist_Entry[])
{
    tBleStatus lReturn;

    lReturn = aci_gap_start_selective_connection_establish_proc(LE_Scan_Type,
            LE_Scan_Interval,
            LE_Scan_Window,
            Own_Address_Type,
            Scanning_Filter_Policy,
            Filter_Duplicates,
            Num_of_Whitelist_Entries,
            Whitelist_Entry);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gap_create_connection(uint16_t LE_Scan_Interval,
                                     uint16_t LE_Scan_Window,
                                     uint8_t Peer_Address_Type,
                                     uint8_t Peer_Address[6],
                                     uint8_t Own_Address_Type,
                                     uint16_t Conn_Interval_Min,
                                     uint16_t Conn_Interval_Max,
                                     uint16_t Conn_Latency,
                                     uint16_t Supervision_Timeout,
                                     uint16_t Minimum_CE_Length,
                                     uint16_t Maximum_CE_Length)
{
    tBleStatus lReturn;

    lReturn = aci_gap_create_connection(LE_Scan_Interval,
            LE_Scan_Window,
            Peer_Address_Type,
            Peer_Address,
            Own_Address_Type,
            Conn_Interval_Min,
            Conn_Interval_Max,
            Conn_Latency,
            Supervision_Timeout,
            Minimum_CE_Length,
            Maximum_CE_Length);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gap_terminate_gap_proc(uint8_t Procedure_Code)
{
    tBleStatus lReturn;

    lReturn = aci_gap_terminate_gap_proc(Procedure_Code);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gap_start_connection_update(uint16_t Connection_Handle,
                                           uint16_t Conn_Interval_Min,
                                           uint16_t Conn_Interval_Max,
                                           uint16_t Conn_Latency,
                                           uint16_t Supervision_Timeout,
                                           uint16_t Minimum_CE_Length,
                                           uint16_t Maximum_CE_Length)
{
    tBleStatus lReturn;

    lReturn = aci_gap_start_connection_update(Connection_Handle,
            Conn_Interval_Min,
            Conn_Interval_Max,
            Conn_Latency,
            Supervision_Timeout,
            Minimum_CE_Length,
            Maximum_CE_Length);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gap_send_pairing_req(uint16_t Connection_Handle,
                                    uint8_t Force_Rebond)
{
    tBleStatus lReturn;

    lReturn = aci_gap_send_pairing_req(Connection_Handle,
            Force_Rebond);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gap_resolve_private_addr(uint8_t Address[6],
                                        uint8_t Actual_Address[6])
{
    tBleStatus lReturn;

    lReturn = aci_gap_resolve_private_addr(Address,
            Actual_Address);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gap_set_broadcast_mode(uint16_t Advertising_Interval_Min,
                                      uint16_t Advertising_Interval_Max,
                                      uint8_t Advertising_Type,
                                      uint8_t Own_Address_Type,
                                      uint8_t Adv_Data_Length,
                                      uint8_t Adv_Data[],
                                      uint8_t Num_of_Whitelist_Entries,
                                      Whitelist_Entry_t Whitelist_Entry[])
{
    tBleStatus lReturn;

    lReturn = aci_gap_set_broadcast_mode(Advertising_Interval_Min,
            Advertising_Interval_Max,
            Advertising_Type,
            Own_Address_Type,
            Adv_Data_Length,
            Adv_Data,
            Num_of_Whitelist_Entries,
            Whitelist_Entry);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gap_start_observation_proc(uint16_t LE_Scan_Interval,
                                          uint16_t LE_Scan_Window,
                                          uint8_t LE_Scan_Type,
                                          uint8_t Own_Address_Type,
                                          uint8_t Filter_Duplicates,
                                          uint8_t Scanning_Filter_Policy)
{
    tBleStatus lReturn;

    lReturn = aci_gap_start_observation_proc(LE_Scan_Interval,
            LE_Scan_Window,
            LE_Scan_Type,
            Own_Address_Type,
            Filter_Duplicates,
            Scanning_Filter_Policy);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gap_get_bonded_devices(uint8_t *Num_of_Addresses,
                                      Bonded_Device_Entry_t Bonded_Device_Entry[])
{
    tBleStatus lReturn;

    lReturn = aci_gap_get_bonded_devices(Num_of_Addresses,
            Bonded_Device_Entry);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gap_is_device_bonded(uint8_t Peer_Address_Type,
                                    uint8_t Peer_Address[6])
{
    tBleStatus lReturn;

    lReturn = aci_gap_is_device_bonded(Peer_Address_Type,
            Peer_Address);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gap_numeric_comparison_value_confirm_yesno(uint16_t Connection_Handle,
                                                          uint8_t Confirm_Yes_No)
{
    tBleStatus lReturn;

    lReturn = aci_gap_numeric_comparison_value_confirm_yesno(Connection_Handle,
            Confirm_Yes_No);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gap_passkey_input(uint16_t Connection_Handle,
                                 uint8_t Input_Type)
{
    tBleStatus lReturn;

    lReturn = aci_gap_passkey_input(Connection_Handle,
            Input_Type);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gap_get_oob_data(uint8_t OOB_Data_Type,
                                uint8_t *Address_Type,
                                uint8_t Address[6],
                                uint8_t *OOB_Data_Len,
                                uint8_t OOB_Data[16])
{
    tBleStatus lReturn;

    lReturn = aci_gap_get_oob_data(OOB_Data_Type,
            Address_Type,
            Address,
            OOB_Data_Len,
            OOB_Data);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gap_set_oob_data(uint8_t Device_Type,
                                uint8_t Address_Type,
                                uint8_t Address[6],
                                uint8_t OOB_Data_Type,
                                uint8_t OOB_Data_Len,
                                uint8_t OOB_Data[16])
{
    tBleStatus lReturn;

    lReturn = aci_gap_set_oob_data(Device_Type,
            Address_Type,
            Address,
            OOB_Data_Type,
            OOB_Data_Len,
            OOB_Data);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gap_add_devices_to_resolving_list(uint8_t Num_of_Resolving_list_Entries,
                                                 Whitelist_Identity_Entry_t Whitelist_Identity_Entry[],
                                                 uint8_t Clear_Resolving_List)
{
    tBleStatus lReturn;

    lReturn = aci_gap_add_devices_to_resolving_list(Num_of_Resolving_list_Entries,
            Whitelist_Identity_Entry,
            Clear_Resolving_List);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gap_remove_bonded_device(uint8_t Peer_Identity_Address_Type,
                                        uint8_t Peer_Identity_Address[6])
{
    tBleStatus lReturn;

    lReturn = aci_gap_remove_bonded_device(Peer_Identity_Address_Type,
            Peer_Identity_Address);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gatt_init(void)
{
    tBleStatus lReturn;

    lReturn = aci_gatt_init();

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gatt_add_service(uint8_t Service_UUID_Type,
                                Service_UUID_t *Service_UUID,
                                uint8_t Service_Type,
                                uint8_t Max_Attribute_Records,
                                uint16_t *Service_Handle)
{
    tBleStatus lReturn;

    lReturn = aci_gatt_add_service(Service_UUID_Type,
            Service_UUID,
            Service_Type,
            Max_Attribute_Records,
            Service_Handle);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gatt_include_service(uint16_t Service_Handle,
                                    uint16_t Include_Start_Handle,
                                    uint16_t Include_End_Handle,
                                    uint8_t Include_UUID_Type,
                                    Include_UUID_t *Include_UUID,
                                    uint16_t *Include_Handle)
{
    tBleStatus lReturn;

    lReturn = aci_gatt_include_service(Service_Handle,
            Include_Start_Handle,
            Include_End_Handle,
            Include_UUID_Type,
            Include_UUID,
            Include_Handle);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gatt_add_char(uint16_t Service_Handle,
                             uint8_t Char_UUID_Type,
                             Char_UUID_t *Char_UUID,
                             uint16_t Char_Value_Length,
                             uint8_t Char_Properties,
                             uint8_t Security_Permissions,
                             uint8_t GATT_Evt_Mask,
                             uint8_t Enc_Key_Size,
                             uint8_t Is_Variable,
                             uint16_t *Char_Handle)
{
    tBleStatus lReturn;

    lReturn = aci_gatt_add_char(Service_Handle,
            Char_UUID_Type,
            Char_UUID,
            Char_Value_Length,
            Char_Properties,
            Security_Permissions,
            GATT_Evt_Mask,
            Enc_Key_Size,
            Is_Variable,
            Char_Handle);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gatt_add_char_desc(uint16_t Service_Handle,
                                  uint16_t Char_Handle,
                                  uint8_t Char_Desc_Uuid_Type,
                                  Char_Desc_Uuid_t *Char_Desc_Uuid,
                                  uint8_t Char_Desc_Value_Max_Len,
                                  uint8_t Char_Desc_Value_Length,
                                  uint8_t Char_Desc_Value[],
                                  uint8_t Security_Permissions,
                                  uint8_t Access_Permissions,
                                  uint8_t GATT_Evt_Mask,
                                  uint8_t Enc_Key_Size,
                                  uint8_t Is_Variable,
                                  uint16_t *Char_Desc_Handle)
{
    tBleStatus lReturn;

    lReturn = aci_gatt_add_char_desc(Service_Handle,
            Char_Handle,
            Char_Desc_Uuid_Type,
            Char_Desc_Uuid,
            Char_Desc_Value_Max_Len,
            Char_Desc_Value_Length,
            Char_Desc_Value,
            Security_Permissions,
            Access_Permissions,
            GATT_Evt_Mask,
            Enc_Key_Size,
            Is_Variable,
            Char_Desc_Handle);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gatt_update_char_value(uint16_t Service_Handle,
                                      uint16_t Char_Handle,
                                      uint8_t Val_Offset,
                                      uint8_t Char_Value_Length,
                                      uint8_t Char_Value[])
{
    tBleStatus lReturn;

    lReturn = aci_gatt_update_char_value(Service_Handle,
            Char_Handle,
            Val_Offset,
            Char_Value_Length,
            Char_Value);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gatt_del_char(uint16_t Serv_Handle,
                             uint16_t Char_Handle)
{
    tBleStatus lReturn;

    lReturn = aci_gatt_del_char(Serv_Handle,
            Char_Handle);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gatt_del_service(uint16_t Serv_Handle)
{
    tBleStatus lReturn;

    lReturn = aci_gatt_del_service(Serv_Handle);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gatt_del_include_service(uint16_t Serv_Handle,
                                        uint16_t Include_Handle)
{
    tBleStatus lReturn;

    lReturn = aci_gatt_del_include_service(Serv_Handle,
            Include_Handle);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gatt_set_event_mask(uint32_t GATT_Evt_Mask)
{
    tBleStatus lReturn;

    lReturn = aci_gatt_set_event_mask(GATT_Evt_Mask);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gatt_exchange_config(uint16_t Connection_Handle)
{
    tBleStatus lReturn;

    lReturn = aci_gatt_exchange_config(Connection_Handle);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_att_find_info_req(uint16_t Connection_Handle,
                                 uint16_t Start_Handle,
                                 uint16_t End_Handle)
{
    tBleStatus lReturn;

    lReturn = aci_att_find_info_req(Connection_Handle,
            Start_Handle,
            End_Handle);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_att_find_by_type_value_req(uint16_t Connection_Handle,
                                          uint16_t Start_Handle,
                                          uint16_t End_Handle,
                                          uint16_t UUID,
                                          uint8_t Attribute_Val_Length,
                                          uint8_t Attribute_Val[])
{
    tBleStatus lReturn;

    lReturn = aci_att_find_by_type_value_req(Connection_Handle,
            Start_Handle,
            End_Handle,
            UUID,
            Attribute_Val_Length,
            Attribute_Val);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_att_read_by_type_req(uint16_t Connection_Handle,
                                    uint16_t Start_Handle,
                                    uint16_t End_Handle,
                                    uint8_t UUID_Type,
                                    UUID_t *UUID)
{
    tBleStatus lReturn;

    lReturn = aci_att_read_by_type_req(Connection_Handle,
            Start_Handle,
            End_Handle,
            UUID_Type,
            UUID);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_att_read_by_group_type_req(uint16_t Connection_Handle,
                                          uint16_t Start_Handle,
                                          uint16_t End_Handle,
                                          uint8_t UUID_Type,
                                          UUID_t *UUID)
{
    tBleStatus lReturn;

    lReturn = aci_att_read_by_group_type_req(Connection_Handle,
            Start_Handle,
            End_Handle,
            UUID_Type,
            UUID);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_att_prepare_write_req(uint16_t Connection_Handle,
                                     uint16_t Attr_Handle,
                                     uint16_t Val_Offset,
                                     uint8_t Attribute_Val_Length,
                                     uint8_t Attribute_Val[])
{
    tBleStatus lReturn;

    lReturn = aci_att_prepare_write_req(Connection_Handle,
            Attr_Handle,
            Val_Offset,
            Attribute_Val_Length,
            Attribute_Val);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_att_execute_write_req(uint16_t Connection_Handle,
                                     uint8_t Execute)
{
    tBleStatus lReturn;

    lReturn = aci_att_execute_write_req(Connection_Handle,
            Execute);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gatt_disc_all_primary_services(uint16_t Connection_Handle)
{
    tBleStatus lReturn;

    lReturn = aci_gatt_disc_all_primary_services(Connection_Handle);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gatt_disc_primary_service_by_uuid(uint16_t Connection_Handle,
                                                 uint8_t UUID_Type,
                                                 UUID_t *UUID)
{
    tBleStatus lReturn;

    lReturn = aci_gatt_disc_primary_service_by_uuid(Connection_Handle,
            UUID_Type,
            UUID);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gatt_find_included_services(uint16_t Connection_Handle,
                                           uint16_t Start_Handle,
                                           uint16_t End_Handle)
{
    tBleStatus lReturn;

    lReturn = aci_gatt_find_included_services(Connection_Handle,
            Start_Handle,
            End_Handle);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gatt_disc_all_char_of_service(uint16_t Connection_Handle,
                                             uint16_t Start_Handle,
                                             uint16_t End_Handle)
{
    tBleStatus lReturn;

    lReturn = aci_gatt_disc_all_char_of_service(Connection_Handle,
            Start_Handle,
            End_Handle);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gatt_disc_char_by_uuid(uint16_t Connection_Handle,
                                      uint16_t Start_Handle,
                                      uint16_t End_Handle,
                                      uint8_t UUID_Type,
                                      UUID_t *UUID)
{
    tBleStatus lReturn;

    lReturn = aci_gatt_disc_char_by_uuid(Connection_Handle,
            Start_Handle,
            End_Handle,
            UUID_Type,
            UUID);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gatt_disc_all_char_desc(uint16_t Connection_Handle,
                                       uint16_t Char_Handle,
                                       uint16_t End_Handle)
{
    tBleStatus lReturn;

    lReturn = aci_gatt_disc_all_char_desc(Connection_Handle,
            Char_Handle,
            End_Handle);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gatt_read_char_value(uint16_t Connection_Handle,
                                    uint16_t Attr_Handle)
{
    tBleStatus lReturn;

    lReturn = aci_gatt_read_char_value(Connection_Handle,
            Attr_Handle);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gatt_read_using_char_uuid(uint16_t Connection_Handle,
                                         uint16_t Start_Handle,
                                         uint16_t End_Handle,
                                         uint8_t UUID_Type,
                                         UUID_t *UUID)
{
    tBleStatus lReturn;

    lReturn = aci_gatt_read_using_char_uuid(Connection_Handle,
            Start_Handle,
            End_Handle,
            UUID_Type,
            UUID);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gatt_read_long_char_value(uint16_t Connection_Handle,
                                         uint16_t Attr_Handle,
                                         uint16_t Val_Offset)
{
    tBleStatus lReturn;

    lReturn = aci_gatt_read_long_char_value(Connection_Handle,
            Attr_Handle,
            Val_Offset);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gatt_read_multiple_char_value(uint16_t Connection_Handle,
                                             uint8_t Number_of_Handles,
                                             Handle_Entry_t Handle_Entry[])
{
    tBleStatus lReturn;

    lReturn = aci_gatt_read_multiple_char_value(Connection_Handle,
            Number_of_Handles,
            Handle_Entry);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gatt_write_char_value(uint16_t Connection_Handle,
                                     uint16_t Attr_Handle,
                                     uint8_t Attribute_Val_Length,
                                     uint8_t Attribute_Val[])
{
    tBleStatus lReturn;

    lReturn = aci_gatt_write_char_value(Connection_Handle,
            Attr_Handle,
            Attribute_Val_Length,
            Attribute_Val);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gatt_write_long_char_value(uint16_t Connection_Handle,
                                          uint16_t Attr_Handle,
                                          uint16_t Val_Offset,
                                          uint8_t Attribute_Val_Length,
                                          uint8_t Attribute_Val[])
{
    tBleStatus lReturn;

    lReturn = aci_gatt_write_long_char_value(Connection_Handle,
            Attr_Handle,
            Val_Offset,
            Attribute_Val_Length,
            Attribute_Val);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gatt_write_char_reliable(uint16_t Connection_Handle,
                                        uint16_t Attr_Handle,
                                        uint16_t Val_Offset,
                                        uint8_t Attribute_Val_Length,
                                        uint8_t Attribute_Val[])
{
    tBleStatus lReturn;

    lReturn = aci_gatt_write_char_reliable(Connection_Handle,
            Attr_Handle,
            Val_Offset,
            Attribute_Val_Length,
            Attribute_Val);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gatt_write_long_char_desc(uint16_t Connection_Handle,
                                         uint16_t Attr_Handle,
                                         uint16_t Val_Offset,
                                         uint8_t Attribute_Val_Length,
                                         uint8_t Attribute_Val[])
{
    tBleStatus lReturn;

    lReturn = aci_gatt_write_long_char_desc(Connection_Handle,
            Attr_Handle,
            Val_Offset,
            Attribute_Val_Length,
            Attribute_Val);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gatt_read_long_char_desc(uint16_t Connection_Handle,
                                        uint16_t Attr_Handle,
                                        uint16_t Val_Offset)
{
    tBleStatus lReturn;

    lReturn = aci_gatt_read_long_char_desc(Connection_Handle,
            Attr_Handle,
            Val_Offset);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gatt_write_char_desc(uint16_t Connection_Handle,
                                    uint16_t Attr_Handle,
                                    uint8_t Attribute_Val_Length,
                                    uint8_t Attribute_Val[])
{
    tBleStatus lReturn;

    lReturn = aci_gatt_write_char_desc(Connection_Handle,
            Attr_Handle,
            Attribute_Val_Length,
            Attribute_Val);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gatt_read_char_desc(uint16_t Connection_Handle,
                                   uint16_t Attr_Handle)
{
    tBleStatus lReturn;

    lReturn = aci_gatt_read_char_desc(Connection_Handle,
            Attr_Handle);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gatt_write_without_resp(uint16_t Connection_Handle,
                                       uint16_t Attr_Handle,
                                       uint8_t Attribute_Val_Length,
                                       uint8_t Attribute_Val[])
{
    tBleStatus lReturn;

    lReturn = aci_gatt_write_without_resp(Connection_Handle,
            Attr_Handle,
            Attribute_Val_Length,
            Attribute_Val);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gatt_signed_write_without_resp(uint16_t Connection_Handle,
                                              uint16_t Attr_Handle,
                                              uint8_t Attribute_Val_Length,
                                              uint8_t Attribute_Val[])
{
    tBleStatus lReturn;

    lReturn = aci_gatt_signed_write_without_resp(Connection_Handle,
            Attr_Handle,
            Attribute_Val_Length,
            Attribute_Val);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gatt_confirm_indication(uint16_t Connection_Handle)
{
    tBleStatus lReturn;

    lReturn = aci_gatt_confirm_indication(Connection_Handle);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gatt_write_resp(uint16_t Connection_Handle,
                               uint16_t Attr_Handle,
                               uint8_t Write_status,
                               uint8_t Error_Code,
                               uint8_t Attribute_Val_Length,
                               uint8_t Attribute_Val[])
{
    tBleStatus lReturn;

    lReturn = aci_gatt_write_resp(Connection_Handle,
            Attr_Handle,
            Write_status,
            Error_Code,
            Attribute_Val_Length,
            Attribute_Val);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gatt_allow_read(uint16_t Connection_Handle)
{
    tBleStatus lReturn;

    lReturn = aci_gatt_allow_read(Connection_Handle);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gatt_set_security_permission(uint16_t Serv_Handle,
                                            uint16_t Attr_Handle,
                                            uint8_t Security_Permissions)
{
    tBleStatus lReturn;

    lReturn = aci_gatt_set_security_permission(Serv_Handle,
            Attr_Handle,
            Security_Permissions);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gatt_set_desc_value(uint16_t Serv_Handle,
                                   uint16_t Char_Handle,
                                   uint16_t Char_Desc_Handle,
                                   uint16_t Val_Offset,
                                   uint8_t Char_Desc_Value_Length,
                                   uint8_t Char_Desc_Value[])
{
    tBleStatus lReturn;

    lReturn = aci_gatt_set_desc_value(Serv_Handle,
            Char_Handle,
            Char_Desc_Handle,
            Val_Offset,
            Char_Desc_Value_Length,
            Char_Desc_Value);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gatt_read_handle_value(uint16_t Attr_Handle,
                                      uint16_t Offset,
                                      uint16_t Value_Length_Requested,
                                      uint16_t *Length,
                                      uint16_t *Value_Length,
                                      uint8_t Value[])
{
    tBleStatus lReturn;

    lReturn = aci_gatt_read_handle_value(Attr_Handle,
            Offset,
            Value_Length_Requested,
            Length,
            Value_Length,
            Value);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gatt_update_char_value_ext(uint16_t Conn_Handle_To_Notify,
                                          uint16_t Service_Handle,
                                          uint16_t Char_Handle,
                                          uint8_t Update_Type,
                                          uint16_t Char_Length,
                                          uint16_t Value_Offset,
                                          uint8_t Value_Length,
                                          uint8_t Value[])
{
    tBleStatus lReturn;

    lReturn = aci_gatt_update_char_value_ext(Conn_Handle_To_Notify,
            Service_Handle,
            Char_Handle,
            Update_Type,
            Char_Length,
            Value_Offset,
            Value_Length,
            Value);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gatt_deny_read(uint16_t Connection_Handle,
                              uint8_t Error_Code)
{
    tBleStatus lReturn;

    lReturn = aci_gatt_deny_read(Connection_Handle,
            Error_Code);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_gatt_set_access_permission(uint16_t Serv_Handle,
                                          uint16_t Attr_Handle,
                                          uint8_t Access_Permissions)
{
    tBleStatus lReturn;

    lReturn = aci_gatt_set_access_permission(Serv_Handle,
            Attr_Handle,
            Access_Permissions);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_l2cap_connection_parameter_update_req(uint16_t Connection_Handle,
                                                     uint16_t Conn_Interval_Min,
                                                     uint16_t Conn_Interval_Max,
                                                     uint16_t Slave_latency,
                                                     uint16_t Timeout_Multiplier)
{
    tBleStatus lReturn;

    lReturn = aci_l2cap_connection_parameter_update_req(Connection_Handle,
            Conn_Interval_Min,
            Conn_Interval_Max,
            Slave_latency,
            Timeout_Multiplier);

    return lReturn;
}

SECTION(".ble_api")tBleStatus BLE_aci_l2cap_connection_parameter_update_resp(uint16_t Connection_Handle,
                                                      uint16_t Conn_Interval_Min,
                                                      uint16_t Conn_Interval_Max,
                                                      uint16_t Slave_latency,
                                                      uint16_t Timeout_Multiplier,
                                                      uint16_t Minimum_CE_Length,
                                                      uint16_t Maximum_CE_Length,
                                                      uint8_t Identifier,
                                                      uint8_t Accept)
{
    tBleStatus lReturn;

    lReturn = aci_l2cap_connection_parameter_update_resp(Connection_Handle,
            Conn_Interval_Min,
            Conn_Interval_Max,
            Slave_latency,
            Timeout_Multiplier,
            Minimum_CE_Length,
            Maximum_CE_Length,
            Identifier,
            Accept);

    return lReturn;
}

 /*********************************** end of ble_api.c *************************************/
