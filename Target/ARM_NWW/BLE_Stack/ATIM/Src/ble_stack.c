/*
    / \     _   |_|
   / _ \  _| |_  _  _____
  / /_\ \(_   _)| ||     |
 / _____ \ | |  | || | | |
/_/     \_\\__) |_||_|_|_|
    (C)2018 Atim

Description: Application

License:

Maintainer: Youen Lebret

 */

/****************************************************************************************
 * Include files
 ****************************************************************************************/
#include "ble.h"
#include "ble_status.h"
#include "bluenrg1_api.h"
#include "bluenrg1_events.h"
#include "bluenrg1_gap.h"
#include "bluenrg1_hal.h"
#include "bluenrg1_gatt_server.h"
#include <string.h>

/****************************************************************************************
 * Macro definitions
 ****************************************************************************************/


/****************************************************************************************
 * Define definitions
 ****************************************************************************************/


/****************************************************************************************
 * Static/global definitions
 ****************************************************************************************/


/****************************************************************************************
 * Static Function prototypes
 ****************************************************************************************/

/****************************************************************************************
 * Static Function Definition
 ****************************************************************************************/

/****************************************************************************************
 * Public Function Definitions
 ****************************************************************************************/
SECTION(".ble_api")
void BLE_StackTick(void)
{
    BTLE_StackTick();
}

SECTION(".ble_api")
tBleStatus BLE_BlueNRG_Stack_Initialization(const BlueNRG_Stack_Initialization_t *BlueNRG_Stack_Init_params_p)
{
    tBleStatus lReturn;

    lReturn = BlueNRG_Stack_Initialization(BlueNRG_Stack_Init_params_p);

    return lReturn;
}


SECTION(".ble_api")
uint8_t BLE_Stack_Perform_Deep_Sleep_Check(void)
{
    uint8_t lReturn;

    lReturn = BlueNRG_Stack_Perform_Deep_Sleep_Check();

    return lReturn;
}

SECTION(".ble_api")
void BLE_RAL_Isr(void)
{
    RAL_Isr();
}

SECTION(".ble_api")
int BLE_HAL_VTimerStart_ms(uint8_t timerNum, int32_t msRelTimeout)
{
    int lReturn;

    lReturn = HAL_VTimerStart_ms(timerNum, msRelTimeout);

    return lReturn;
}

SECTION(".ble_api")
void BLE_HAL_VTimer_Stop(uint8_t timerNum)
{
    HAL_VTimer_Stop(timerNum);
}

SECTION(".ble_api")
uint32_t BLE_HAL_VTimerGetCurrentTime_sysT32(void)
{
    uint32_t lReturn;

    lReturn = HAL_VTimerGetCurrentTime_sysT32();

    return lReturn;
}

SECTION(".ble_api")
uint32_t BLE_HAL_VTimerAcc_sysT32_ms(uint32_t sysTime, int32_t msTime)
{
    uint32_t lReturn;

    lReturn = HAL_VTimerAcc_sysT32_ms(sysTime, msTime);

    return lReturn;
}

SECTION(".ble_api")
int32_t BLE_HAL_VTimerDiff_ms_sysT32(uint32_t sysTime1, uint32_t sysTime2)
{
    int32_t lReturn;

    lReturn = HAL_VTimerDiff_ms_sysT32(sysTime1, sysTime2);

    return lReturn;
}

SECTION(".ble_api")
int BLE_HAL_VTimerStart_sysT32(uint8_t timerNum, uint32_t time)
{
    int lReturn;

    lReturn = HAL_VTimerStart_sysT32(timerNum, time);

    return lReturn;
}

SECTION(".ble_api")
int BLE_HAL_VTimerExpiry_sysT32(uint8_t timerNum, uint32_t *sysTime)
{
    int lReturn;

    lReturn = HAL_VTimerExpiry_sysT32(timerNum, sysTime);

    return lReturn;
}

 /*********************************** end of ble_stacks.c *************************************/
