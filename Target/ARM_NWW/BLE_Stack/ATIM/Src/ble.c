/*
    / \     _   |_|
   / _ \  _| |_  _  _____
  / /_\ \(_   _)| ||     |
 / _____ \ | |  | || | | |
/_/     \_\\__) |_||_|_|_|
    (C)2018 Atim

Description: Application

License:

Maintainer: Youen Lebret

 */

/****************************************************************************************
 * Include files
 ****************************************************************************************/
#include "ble.h"
#include "ble_status.h"
#include "bluenrg1_api.h"
#include "bluenrg1_events.h"
#include "bluenrg1_gap.h"
#include "bluenrg1_hal.h"
#include "bluenrg1_gatt_server.h"
#include <string.h>

/****************************************************************************************
 * Macro definitions
 ****************************************************************************************/


/****************************************************************************************
 * Define definitions
 ****************************************************************************************/
/** Max Length for connection name */
#define BLE_MAX_NAME_LEN    (0x10u)

/****************************************************************************************
 * Static/global definitions
 ****************************************************************************************/
/** Struct where all applicatove callback wanted by user shall be defined .
 * If NULL so callback is not called. */
SECTION(".ble_ram")
REQUIRED(BLE_CallBackInfo_t BLE_CallBackInfo);
/****************************************************************************************
 * Static Function prototypes
 ****************************************************************************************/

/****************************************************************************************
 * Static Function Definition
 ****************************************************************************************/

/****************************************************************************************
 * Public Function Definitions
 ****************************************************************************************/
SECTION(".ble_api")
tBleStatus BLE_SetConnectable(uint8_t * serviceUUID4Scan, uint8_t serviceUUID4ScanLen,
        uint8_t * bleName, uint8_t * bleNameLen, uint16_t Advertising_Interval_Min,
        uint16_t Advertising_Interval_Max, uint16_t Slave_Conn_Interval_Min,
        uint16_t Slave_Conn_Interval_Max)
{
    tBleStatus lReturn;
    uint8_t  local_name[BLE_MAX_NAME_LEN];


    /* First bytes should be AD_TYPE_COMPLETE_LOCAL_NAME */
    local_name[0u] = AD_TYPE_COMPLETE_LOCAL_NAME;

    /* Check size parameter. If bleNameLen is too long, take the max authorized */
    if(*bleNameLen >= BLE_MAX_NAME_LEN)
    {
        *bleNameLen = BLE_MAX_NAME_LEN - 1u;
    }

    /* copy the name */
    memcpy(&local_name[1u], bleName, *bleNameLen);

    /* Add service UUID to scan response */
    lReturn = BLE_hci_le_set_scan_response_data(serviceUUID4ScanLen, serviceUUID4Scan);

    if (BLE_STATUS_SUCCESS == lReturn)
    {

        lReturn = BLE_aci_gap_set_discoverable(ADV_IND, Advertising_Interval_Min, Advertising_Interval_Max, PUBLIC_ADDR, NO_WHITE_LIST_USE,
                (*(bleNameLen) + 1u), local_name, 0, NULL, Slave_Conn_Interval_Min, Slave_Conn_Interval_Max);
    }

    return lReturn;
} /*** end of BLE_SetConnectable ***/

SECTION(".ble_api")
tBleStatus BLE_Init(const BlueNRG_Stack_Initialization_t * stackInitParam,
        uint8_t * addr, uint8_t addr_len, BLE_CallBackInfo_t* callbackInfo)
{
    tBleStatus lReturn;
    uint16_t service_handle;
    uint16_t dev_name_char_handle;
    uint16_t appearance_char_handle;

    /* BlueNRG BLE stack init */
    BLE_BlueNRG_Stack_Initialization(stackInitParam);

    lReturn = BLE_aci_hal_write_config_data(CONFIG_DATA_PUBADDR_OFFSET, CONFIG_DATA_PUBADDR_LEN,
            addr);

    /* Check return */
    if(BLE_STATUS_SUCCESS == lReturn)
    {
        /* Init GATT */
        lReturn = BLE_aci_gatt_init();

        if(BLE_STATUS_SUCCESS == lReturn)
        {
            /* Init GAP. no need to conserve service and characteristics handles */
            lReturn = BLE_aci_gap_init(GAP_PERIPHERAL_ROLE, 0, 0x07, &service_handle, &dev_name_char_handle, &appearance_char_handle);
        }
    }

    /* Save the callback to call in case of event */
    memcpy((uint8_t *)&BLE_CallBackInfo, (uint8_t *)callbackInfo, sizeof(BLE_CallBackInfo_t));

    return lReturn;
} /*** end of BLE_Init ***/

SECTION(".ble_api")
tBleStatus BLE_AddService(BLE_ServiceInfo_t **serviceToAdd, uint8_t numberOfService)
{
    tBleStatus lReturn = BLE_STATUS_SUCCESS;
    Service_UUID_t service_uuid;
    uint8_t index = 0u;
    BLE_ServiceInfo_t * serviceInfo;

    while ((index < numberOfService) &&
            (BLE_STATUS_SUCCESS == lReturn))
    {
        /* check for null param */
        if((serviceToAdd != NULL) &&
                (*(serviceToAdd) != NULL))
        {
            /* get the service */
            serviceInfo = *serviceToAdd;

            /* Add all the service to the GATT */
            memcpy(&service_uuid.Service_UUID_128, &serviceInfo->uuid, sizeof(service_uuid.Service_UUID_128));

            lReturn = BLE_aci_gatt_add_service(serviceInfo->uuidType, &service_uuid, serviceInfo->serviceType,
                    serviceInfo->maxAttrRecord, serviceInfo->serviceHandle);

        }

        /* prepare the next service info */
        serviceToAdd++;
        index++;
    }

    return lReturn;
} /*** end of BLE_AddService ***/

SECTION(".ble_api")
tBleStatus BLE_AddChar(BLE_CharInfo_t **charToAdd, uint8_t numberOfChar)
{
    tBleStatus lReturn = BLE_STATUS_SUCCESS;
    Char_UUID_t char_uuid;
    uint8_t index = 0u;
    BLE_CharInfo_t * charInfo;

    while ((index < numberOfChar) &&
            (BLE_STATUS_SUCCESS == lReturn))
    {
        /* check for null param */
        if((charToAdd != NULL) &&
                (*(charToAdd) != NULL))
        {
            /* get the characteristic */
            charInfo = *charToAdd;

            /* Add all the characteristics to their services  to the GATT */
            memcpy(&char_uuid.Char_UUID_128, &charInfo->uuid, sizeof(char_uuid.Char_UUID_128));

            lReturn = BLE_aci_gatt_add_char(*(charInfo->serviceHandle), charInfo->uuidType, &char_uuid,
                    charInfo->charValueLen, charInfo->charProp, charInfo->secuPerm, charInfo->gattEvent,
                    charInfo->readEncSize, charInfo->charLenType, charInfo->charHandle);
        }

        /* prepare the next characteristic info */
        charToAdd++;
        index++;
    }

    return lReturn;
} /*** end of BLE_AddChar ***/


 /*********************************** end of ble.c *************************************/
