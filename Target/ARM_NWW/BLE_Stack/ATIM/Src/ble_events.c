/*
    / \     _   |_|
   / _ \  _| |_  _  _____
  / /_\ \(_   _)| ||     |
 / _____ \ | |  | || | | |
/_/     \_\\__) |_||_|_|_|
    (C)2018 Atim

Description: Application

License:

Maintainer: Youen Lebret

 */

/****************************************************************************************
 * Include files
 ****************************************************************************************/
#include "ble.h"
#include "ble_status.h"
#include "bluenrg1_api.h"
#include "bluenrg1_events.h"
#include "bluenrg1_gap.h"
#include "bluenrg1_hal.h"
#include "bluenrg1_gatt_server.h"
#include <string.h>

/****************************************************************************************
 * Macro definitions
 ****************************************************************************************/

/****************************************************************************************
 * Define definitions
 ****************************************************************************************/

/****************************************************************************************
 * Static/global definitions
 ****************************************************************************************/

/****************************************************************************************
 * Static Function prototypes
 ****************************************************************************************/

/****************************************************************************************
 * Static Function Definition
 ****************************************************************************************/

/****************************************************************************************
 * Public Function Definitions
 ****************************************************************************************/

/****************************************************************************************
 * Callbacks
 ****************************************************************************************/


void hci_disconnection_complete_event(uint8_t Status,
        uint16_t Connection_Handle,
        uint8_t Reason)
{
    /* Call the callback if one has been registered */
    if(BLE_CallBackInfo.BLE_hci_disconnection_complete_event_cb != NULL)
    {
        BLE_CallBackInfo.BLE_hci_disconnection_complete_event_cb(Status,
                    Connection_Handle,
                    Reason);
    }

}


void hci_encryption_change_event(uint8_t Status,
        uint16_t Connection_Handle,
        uint8_t Encryption_Enabled)
{
    /* Call the callback if one has been registered */
    if(BLE_CallBackInfo.BLE_hci_encryption_change_event_cb != NULL)
    {
        BLE_CallBackInfo.BLE_hci_encryption_change_event_cb(Status,
                Connection_Handle,
                Encryption_Enabled);
    }
}


void hci_read_remote_version_information_complete_event(uint8_t Status,
        uint16_t Connection_Handle,
        uint8_t Version,
        uint16_t Manufacturer_Name,
        uint16_t Subversion)
{
    /* Call the callback if one has been registered */
    if(BLE_CallBackInfo.BLE_hci_read_remote_version_information_complete_event_cb != NULL)
    {
        BLE_CallBackInfo.BLE_hci_read_remote_version_information_complete_event_cb(Status,
            Connection_Handle,
            Version,
            Manufacturer_Name,
            Subversion);
    }
}


void hci_hardware_error_event(uint8_t Hardware_Code)
{
    /* Call the callback if one has been registered */
    if(BLE_CallBackInfo.BLE_hci_hardware_error_event_cb != NULL)
    {
        BLE_CallBackInfo.BLE_hci_hardware_error_event_cb(Hardware_Code);
    }
}


void hci_number_of_completed_packets_event(uint8_t Number_of_Handles,
        Handle_Packets_Pair_Entry_t Handle_Packets_Pair_Entry[])
{
    /* Call the callback if one has been registered */
    if(BLE_CallBackInfo.BLE_hci_number_of_completed_packets_event_cb != NULL)
    {
        BLE_CallBackInfo.BLE_hci_number_of_completed_packets_event_cb(Number_of_Handles,
            Handle_Packets_Pair_Entry);
    }
}


void hci_data_buffer_overflow_event(uint8_t Link_Type)
{
    /* Call the callback if one has been registered */
    if(BLE_CallBackInfo.BLE_hci_data_buffer_overflow_event_cb != NULL)
    {
        BLE_CallBackInfo.BLE_hci_data_buffer_overflow_event_cb(Link_Type);
    }
}


void hci_encryption_key_refresh_complete_event(uint8_t Status,
        uint16_t Connection_Handle)
{
    /* Call the callback if one has been registered */
    if(BLE_CallBackInfo.BLE_hci_encryption_key_refresh_complete_event_cb != NULL)
    {
        BLE_CallBackInfo.BLE_hci_encryption_key_refresh_complete_event_cb(Status, Connection_Handle);
    }
}



void hci_le_connection_complete_event(uint8_t Status,
        uint16_t Connection_Handle,
        uint8_t Role,
        uint8_t Peer_Address_Type,
        uint8_t Peer_Address[6],
        uint16_t Conn_Interval,
        uint16_t Conn_Latency,
        uint16_t Supervision_Timeout,
        uint8_t Master_Clock_Accuracy)
{
    /* Call the callback if one has been registered */
    if(BLE_CallBackInfo.BLE_hci_le_connection_complete_event_cb != NULL)
    {
        BLE_CallBackInfo.BLE_hci_le_connection_complete_event_cb(Status,
            Connection_Handle,
            Role,
            Peer_Address_Type,
            Peer_Address,
            Conn_Interval,
            Conn_Latency,
            Supervision_Timeout,
            Master_Clock_Accuracy);
    }
}


void hci_le_advertising_report_event(uint8_t Num_Reports,
        Advertising_Report_t Advertising_Report[])
{
    /* Call the callback if one has been registered */
    if(BLE_CallBackInfo.BLE_hci_le_advertising_report_event_cb != NULL)
    {
        BLE_CallBackInfo.BLE_hci_le_advertising_report_event_cb(Num_Reports, Advertising_Report);
    }
}


void hci_le_connection_update_complete_event(uint8_t Status,
        uint16_t Connection_Handle,
        uint16_t Conn_Interval,
        uint16_t Conn_Latency,
        uint16_t Supervision_Timeout)
{
    /* Call the callback if one has been registered */
    if(BLE_CallBackInfo.BLE_hci_le_connection_update_complete_event_cb != NULL)
    {
        BLE_CallBackInfo.BLE_hci_le_connection_update_complete_event_cb(Status,
            Connection_Handle,
            Conn_Interval,
            Conn_Latency,
            Supervision_Timeout);
    }
}


void hci_le_read_remote_used_features_complete_event(uint8_t Status,
        uint16_t Connection_Handle,
        uint8_t LE_Features[8])
{
    /* Call the callback if one has been registered */
    if(BLE_CallBackInfo.BLE_hci_le_read_remote_used_features_complete_event_cb != NULL)
    {
        BLE_CallBackInfo.BLE_hci_le_read_remote_used_features_complete_event_cb(Status,
            Connection_Handle,
            LE_Features);
    }
}


void hci_le_long_term_key_request_event(uint16_t Connection_Handle,
        uint8_t Random_Number[8],
        uint16_t Encrypted_Diversifier)
{
    /* Call the callback if one has been registered */
    if(BLE_CallBackInfo.BLE_hci_le_long_term_key_request_event_cb != NULL)
    {
        BLE_CallBackInfo.BLE_hci_le_long_term_key_request_event_cb(Connection_Handle,
            Random_Number,
            Encrypted_Diversifier);
    }
}


void hci_le_data_length_change_event(uint16_t Connection_Handle,
        uint16_t MaxTxOctets,
        uint16_t MaxTxTime,
        uint16_t MaxRxOctets,
        uint16_t MaxRxTime)
{
    /* Call the callback if one has been registered */
    if(BLE_CallBackInfo.BLE_hci_le_data_length_change_event_cb != NULL)
    {
        BLE_CallBackInfo.BLE_hci_le_data_length_change_event_cb(Connection_Handle,
            MaxTxOctets,
            MaxTxTime,
            MaxRxOctets,
            MaxRxTime);
    }
}


void hci_le_read_local_p256_public_key_complete_event(uint8_t Status,
        uint8_t Local_P256_Public_Key[64])
{
    /* Call the callback if one has been registered */
    if(BLE_CallBackInfo.BLE_hci_le_read_local_p256_public_key_complete_event_cb != NULL)
    {
        BLE_CallBackInfo.BLE_hci_le_read_local_p256_public_key_complete_event_cb(Status, Local_P256_Public_Key);
    }
}


void hci_le_generate_dhkey_complete_event(uint8_t Status,
        uint8_t DHKey[32])
{
    /* Call the callback if one has been registered */
    if(BLE_CallBackInfo.BLE_hci_le_generate_dhkey_complete_event_cb != NULL)
    {
        BLE_CallBackInfo.BLE_hci_le_generate_dhkey_complete_event_cb(Status, DHKey);
    }
}


void hci_le_enhanced_connection_complete_event(uint8_t Status,
        uint16_t Connection_Handle,
        uint8_t Role,
        uint8_t Peer_Address_Type,
        uint8_t Peer_Address[6],
        uint8_t Local_Resolvable_Private_Address[6],
        uint8_t Peer_Resolvable_Private_Address[6],
        uint16_t Conn_Interval,
        uint16_t Conn_Latency,
        uint16_t Supervision_Timeout,
        uint8_t Master_Clock_Accuracy)
{
    /* Call the callback if one has been registered */
    if(BLE_CallBackInfo.BLE_hci_le_enhanced_connection_complete_event_cb != NULL)
    {
        BLE_CallBackInfo.BLE_hci_le_enhanced_connection_complete_event_cb(Status,
            Connection_Handle,
            Role,
            Peer_Address_Type,
            Peer_Address,
            Local_Resolvable_Private_Address,
            Peer_Resolvable_Private_Address,
            Conn_Interval,
            Conn_Latency,
            Supervision_Timeout,
            Master_Clock_Accuracy);
    }
}


void hci_le_direct_advertising_report_event(uint8_t Num_Reports,
        Direct_Advertising_Report_t Direct_Advertising_Report[])
{
    /* Call the callback if one has been registered */
    if(BLE_CallBackInfo.BLE_hci_le_direct_advertising_report_event_cb != NULL)
    {
        BLE_CallBackInfo.BLE_hci_le_direct_advertising_report_event_cb(Num_Reports, Direct_Advertising_Report);
    }
}


void aci_gap_limited_discoverable_event(void)
{
    /* Call the callback if one has been registered */
    if(BLE_CallBackInfo.BLE_aci_gap_limited_discoverable_event_cb != NULL)
    {
        BLE_CallBackInfo.BLE_aci_gap_limited_discoverable_event_cb();
    }
}


void aci_gap_pairing_complete_event(uint16_t Connection_Handle,
        uint8_t Status,
        uint8_t Reason)
{
    /* Call the callback if one has been registered */
    if(BLE_CallBackInfo.BLE_aci_gap_pairing_complete_event_cb != NULL)
    {
        BLE_CallBackInfo.BLE_aci_gap_pairing_complete_event_cb(Connection_Handle,
            Status,
            Reason);
    }
}


void aci_gap_pass_key_req_event(uint16_t Connection_Handle)
{
    /* Call the callback if one has been registered */
    if(BLE_CallBackInfo.BLE_aci_gap_pass_key_req_event_cb != NULL)
    {
        BLE_CallBackInfo.BLE_aci_gap_pass_key_req_event_cb(Connection_Handle);
    }
}


void aci_gap_authorization_req_event(uint16_t Connection_Handle)
{
    /* Call the callback if one has been registered */
    if(BLE_CallBackInfo.BLE_aci_gap_authorization_req_event_cb != NULL)
    {
        BLE_CallBackInfo.BLE_aci_gap_authorization_req_event_cb(Connection_Handle);
    }
}


void aci_gap_slave_security_initiated_event(void)
{
    /* Call the callback if one has been registered */
    if(BLE_CallBackInfo.BLE_aci_gap_slave_security_initiated_event_cb != NULL)
    {
        BLE_CallBackInfo.BLE_aci_gap_slave_security_initiated_event_cb();
    }
}


void aci_gap_bond_lost_event(void)
{
    /* Call the callback if one has been registered */
    if(BLE_CallBackInfo.BLE_aci_gap_bond_lost_event_cb != NULL)
    {
        BLE_CallBackInfo.BLE_aci_gap_bond_lost_event_cb();
    }
}


void aci_gap_proc_complete_event(uint8_t Procedure_Code,
        uint8_t Status,
        uint8_t Data_Length,
        uint8_t Data[])
{
    /* Call the callback if one has been registered */
    if(BLE_CallBackInfo.BLE_aci_gap_proc_complete_event_cb != NULL)
    {
        BLE_CallBackInfo.BLE_aci_gap_proc_complete_event_cb(Procedure_Code,
            Status,
            Data_Length,
            Data);
    }
}


void aci_gap_addr_not_resolved_event(uint16_t Connection_Handle)
{
    /* Call the callback if one has been registered */
    if(BLE_CallBackInfo.BLE_aci_gap_addr_not_resolved_event_cb != NULL)
    {
        BLE_CallBackInfo.BLE_aci_gap_addr_not_resolved_event_cb(Connection_Handle);
    }
}


void aci_gap_numeric_comparison_value_event(uint16_t Connection_Handle,
        uint32_t Numeric_Value)
{
    /* Call the callback if one has been registered */
    if(BLE_CallBackInfo.BLE_aci_gap_numeric_comparison_value_event_cb != NULL)
    {
        BLE_CallBackInfo.BLE_aci_gap_numeric_comparison_value_event_cb(Connection_Handle, Numeric_Value);
    }
}


void aci_gap_keypress_notification_event(uint16_t Connection_Handle,
        uint8_t Notification_Type)
{
    /* Call the callback if one has been registered */
    if(BLE_CallBackInfo.BLE_aci_gap_keypress_notification_event_cb != NULL)
    {
        BLE_CallBackInfo.BLE_aci_gap_keypress_notification_event_cb(Connection_Handle, Notification_Type);
    }
}



void aci_gatt_attribute_modified_event(uint16_t Connection_Handle,
        uint16_t Attr_Handle,
        uint16_t Offset,
        uint16_t Attr_Data_Length,
        uint8_t Attr_Data[])
{
    /* Call the callback if one has been registered */
    if(BLE_CallBackInfo.BLE_aci_gatt_attribute_modified_event_cb != NULL)
    {
        BLE_CallBackInfo.BLE_aci_gatt_attribute_modified_event_cb(Connection_Handle,
            Attr_Handle,
            Offset,
            Attr_Data_Length,
            Attr_Data);
    }
}


void aci_gatt_proc_timeout_event(uint16_t Connection_Handle)
{
    /* Call the callback if one has been registered */
    if(BLE_CallBackInfo.BLE_aci_gatt_proc_timeout_event_cb != NULL)
    {
        BLE_CallBackInfo.BLE_aci_gatt_proc_timeout_event_cb(Connection_Handle);
    }
}


void aci_att_exchange_mtu_resp_event(uint16_t Connection_Handle,
        uint16_t Server_RX_MTU)
{
    /* Call the callback if one has been registered */
    if(BLE_CallBackInfo.BLE_aci_att_exchange_mtu_resp_event_cb != NULL)
    {
        BLE_CallBackInfo.BLE_aci_att_exchange_mtu_resp_event_cb(Connection_Handle,
            Server_RX_MTU);
    }
}


void aci_att_find_info_resp_event(uint16_t Connection_Handle,
        uint8_t Format,
        uint8_t Event_Data_Length,
        uint8_t Handle_UUID_Pair[])
{
    /* Call the callback if one has been registered */
    if(BLE_CallBackInfo.BLE_aci_att_find_info_resp_event_cb != NULL)
    {
        BLE_CallBackInfo.BLE_aci_att_find_info_resp_event_cb(Connection_Handle,
            Format,
            Event_Data_Length,
            Handle_UUID_Pair);
    }
}


void aci_att_find_by_type_value_resp_event(uint16_t Connection_Handle,
        uint8_t Num_of_Handle_Pair,
        Attribute_Group_Handle_Pair_t Attribute_Group_Handle_Pair[])
{
    /* Call the callback if one has been registered */
    if(BLE_CallBackInfo.BLE_aci_att_find_by_type_value_resp_event_cb != NULL)
    {
        BLE_CallBackInfo.BLE_aci_att_find_by_type_value_resp_event_cb(Connection_Handle,
            Num_of_Handle_Pair,
            Attribute_Group_Handle_Pair);
    }
}


void aci_att_read_by_type_resp_event(uint16_t Connection_Handle,
        uint8_t Handle_Value_Pair_Length,
        uint8_t Data_Length,
        uint8_t Handle_Value_Pair_Data[])
{
    /* Call the callback if one has been registered */
    if(BLE_CallBackInfo.BLE_aci_att_read_by_type_resp_event_cb != NULL)
    {
        BLE_CallBackInfo.BLE_aci_att_read_by_type_resp_event_cb(Connection_Handle,
            Handle_Value_Pair_Length,
            Data_Length,
            Handle_Value_Pair_Data);
    }
}


void aci_att_read_resp_event(uint16_t Connection_Handle,
        uint8_t Event_Data_Length,
        uint8_t Attribute_Value[])
{
    /* Call the callback if one has been registered */
    if(BLE_CallBackInfo.BLE_aci_att_read_resp_event_cb != NULL)
    {
        BLE_CallBackInfo.BLE_aci_att_read_resp_event_cb(Connection_Handle,
            Event_Data_Length,
            Attribute_Value);
    }
}


void aci_att_read_blob_resp_event(uint16_t Connection_Handle,
        uint8_t Event_Data_Length,
        uint8_t Attribute_Value[])
{
    /* Call the callback if one has been registered */
    if(BLE_CallBackInfo.BLE_aci_att_read_blob_resp_event_cb != NULL)
    {
        BLE_CallBackInfo.BLE_aci_att_read_blob_resp_event_cb(Connection_Handle,
            Event_Data_Length,
            Attribute_Value);
    }
}


void aci_att_read_multiple_resp_event(uint16_t Connection_Handle,
        uint8_t Event_Data_Length,
        uint8_t Set_Of_Values[])
{
    /* Call the callback if one has been registered */
    if(BLE_CallBackInfo.BLE_aci_att_read_multiple_resp_event_cb != NULL)
    {
        BLE_CallBackInfo.BLE_aci_att_read_multiple_resp_event_cb(Connection_Handle,
            Event_Data_Length,
            Set_Of_Values);
    }
}


void aci_att_read_by_group_type_resp_event(uint16_t Connection_Handle,
        uint8_t Attribute_Data_Length,
        uint8_t Data_Length,
        uint8_t Attribute_Data_List[])
{
    /* Call the callback if one has been registered */
    if(BLE_CallBackInfo.BLE_aci_att_read_by_group_type_resp_event_cb != NULL)
    {
        BLE_CallBackInfo.BLE_aci_att_read_by_group_type_resp_event_cb(Connection_Handle,
            Attribute_Data_Length,
            Data_Length,
            Attribute_Data_List);
    }
}


void aci_att_prepare_write_resp_event(uint16_t Connection_Handle,
        uint16_t Attribute_Handle,
        uint16_t Offset,
        uint8_t Part_Attribute_Value_Length,
        uint8_t Part_Attribute_Value[])
{
    /* Call the callback if one has been registered */
    if(BLE_CallBackInfo.BLE_aci_att_prepare_write_resp_event_cb != NULL)
    {
        BLE_CallBackInfo.BLE_aci_att_prepare_write_resp_event_cb(Connection_Handle,
            Attribute_Handle,
            Offset,
            Part_Attribute_Value_Length,
            Part_Attribute_Value);
    }
}


void aci_att_exec_write_resp_event(uint16_t Connection_Handle)
{
    /* Call the callback if one has been registered */
    if(BLE_CallBackInfo.BLE_aci_att_exec_write_resp_event_cb != NULL)
    {
        BLE_CallBackInfo.BLE_aci_att_exec_write_resp_event_cb(Connection_Handle);
    }
}


void aci_gatt_indication_event(uint16_t Connection_Handle,
        uint16_t Attribute_Handle,
        uint8_t Attribute_Value_Length,
        uint8_t Attribute_Value[])
{
    /* Call the callback if one has been registered */
    if(BLE_CallBackInfo.BLE_aci_gatt_indication_event_cb != NULL)
    {
        BLE_CallBackInfo.BLE_aci_gatt_indication_event_cb(Connection_Handle,
            Attribute_Handle,
            Attribute_Value_Length,
            Attribute_Value);
    }
}


void aci_gatt_notification_event(uint16_t Connection_Handle,
        uint16_t Attribute_Handle,
        uint8_t Attribute_Value_Length,
        uint8_t Attribute_Value[])
{
    /* Call the callback if one has been registered */
    if(BLE_CallBackInfo.BLE_aci_gatt_notification_event_cb != NULL)
    {
        BLE_CallBackInfo.BLE_aci_gatt_notification_event_cb(Connection_Handle,
            Attribute_Handle,
            Attribute_Value_Length,
            Attribute_Value);
    }
}


void aci_gatt_proc_complete_event(uint16_t Connection_Handle,
        uint8_t Error_Code)
{
    /* Call the callback if one has been registered */
    if(BLE_CallBackInfo.BLE_aci_gatt_proc_complete_event_cb != NULL)
    {
        BLE_CallBackInfo.BLE_aci_gatt_proc_complete_event_cb(Connection_Handle,
            Error_Code);
    }
}


void aci_gatt_error_resp_event(uint16_t Connection_Handle,
        uint8_t Req_Opcode,
        uint16_t Attribute_Handle,
        uint8_t Error_Code)
{
    /* Call the callback if one has been registered */
    if(BLE_CallBackInfo.BLE_aci_gatt_error_resp_event_cb != NULL)
    {
        BLE_CallBackInfo.BLE_aci_gatt_error_resp_event_cb(Connection_Handle,
            Req_Opcode,
            Attribute_Handle,
            Error_Code);
    }
}


void aci_gatt_disc_read_char_by_uuid_resp_event(uint16_t Connection_Handle,
        uint16_t Attribute_Handle,
        uint8_t Attribute_Value_Length,
        uint8_t Attribute_Value[])
{
    /* Call the callback if one has been registered */
    if(BLE_CallBackInfo.BLE_aci_gatt_disc_read_char_by_uuid_resp_event_cb != NULL)
    {
        BLE_CallBackInfo.BLE_aci_gatt_disc_read_char_by_uuid_resp_event_cb(Connection_Handle,
            Attribute_Handle,
            Attribute_Value_Length,
            Attribute_Value);
    }
}


void aci_gatt_write_permit_req_event(uint16_t Connection_Handle,
        uint16_t Attribute_Handle,
        uint8_t Data_Length,
        uint8_t Data[])
{
    /* Call the callback if one has been registered */
    if(BLE_CallBackInfo.BLE_aci_gatt_write_permit_req_event_cb != NULL)
    {
        BLE_CallBackInfo.BLE_aci_gatt_write_permit_req_event_cb(Connection_Handle,
            Attribute_Handle,
            Data_Length,
            Data);
    }
}


void aci_gatt_read_permit_req_event(uint16_t Connection_Handle,
        uint16_t Attribute_Handle,
        uint16_t Offset)
{
    /* Call the callback if one has been registered */
    if(BLE_CallBackInfo.BLE_aci_gatt_read_permit_req_event_cb != NULL)
    {
        BLE_CallBackInfo.BLE_aci_gatt_read_permit_req_event_cb(Connection_Handle,
            Attribute_Handle,
            Offset);
    }
}


void aci_gatt_read_multi_permit_req_event(uint16_t Connection_Handle,
        uint8_t Number_of_Handles,
        Handle_Item_t Handle_Item[])
{
    /* Call the callback if one has been registered */
    if(BLE_CallBackInfo.BLE_aci_gatt_read_multi_permit_req_event_cb != NULL)
    {
        BLE_CallBackInfo.BLE_aci_gatt_read_multi_permit_req_event_cb(Connection_Handle,
            Number_of_Handles,
            Handle_Item);
    }
}


void aci_gatt_tx_pool_available_event(uint16_t Connection_Handle,
        uint16_t Available_Buffers)
{
    /* Call the callback if one has been registered */
    if(BLE_CallBackInfo.BLE_aci_gatt_tx_pool_available_event_cb != NULL)
    {
        BLE_CallBackInfo.BLE_aci_gatt_tx_pool_available_event_cb(Connection_Handle,
            Available_Buffers);
    }
}


void aci_gatt_server_confirmation_event(uint16_t Connection_Handle)
{
    /* Call the callback if one has been registered */
    if(BLE_CallBackInfo.BLE_aci_gatt_server_confirmation_event_cb != NULL)
    {
        BLE_CallBackInfo.BLE_aci_gatt_server_confirmation_event_cb(Connection_Handle);
    }
}


void aci_gatt_prepare_write_permit_req_event(uint16_t Connection_Handle,
        uint16_t Attribute_Handle,
        uint16_t Offset,
        uint8_t Data_Length,
        uint8_t Data[])
{
    /* Call the callback if one has been registered */
    if(BLE_CallBackInfo.BLE_aci_gatt_prepare_write_permit_req_event_cb != NULL)
    {
        BLE_CallBackInfo.BLE_aci_gatt_prepare_write_permit_req_event_cb(Connection_Handle,
            Attribute_Handle,
            Offset,
            Data_Length,
            Data);
    }
}


void aci_l2cap_connection_update_resp_event(uint16_t Connection_Handle,
        uint16_t Result)
{
    /* Call the callback if one has been registered */
    if(BLE_CallBackInfo.BLE_aci_l2cap_connection_update_resp_event_cb != NULL)
    {
        BLE_CallBackInfo.BLE_aci_l2cap_connection_update_resp_event_cb(Connection_Handle,
            Result);
    }
}


void aci_l2cap_proc_timeout_event(uint16_t Connection_Handle,
        uint8_t Data_Length,
        uint8_t Data[])
{
    /* Call the callback if one has been registered */
    if(BLE_CallBackInfo.BLE_aci_l2cap_proc_timeout_event_cb != NULL)
    {
        BLE_CallBackInfo.BLE_aci_l2cap_proc_timeout_event_cb(Connection_Handle,
            Data_Length,
            Data);
    }
}


void aci_l2cap_connection_update_req_event(uint16_t Connection_Handle,
        uint8_t Identifier,
        uint16_t L2CAP_Length,
        uint16_t Interval_Min,
        uint16_t Interval_Max,
        uint16_t Slave_Latency,
        uint16_t Timeout_Multiplier)
{
    /* Call the callback if one has been registered */
    if(BLE_CallBackInfo.BLE_aci_l2cap_connection_update_req_event_cb != NULL)
    {
        BLE_CallBackInfo.BLE_aci_l2cap_connection_update_req_event_cb(Connection_Handle,
            Identifier,
            L2CAP_Length,
            Interval_Min,
            Interval_Max,
            Slave_Latency,
            Timeout_Multiplier);
    }
}


void aci_l2cap_command_reject_event(uint16_t Connection_Handle,
        uint8_t Identifier,
        uint16_t Reason,
        uint8_t Data_Length,
        uint8_t Data[])
{
    /* Call the callback if one has been registered */
    if(BLE_CallBackInfo.BLE_aci_l2cap_command_reject_event_cb != NULL)
    {
        BLE_CallBackInfo.BLE_aci_l2cap_command_reject_event_cb(Connection_Handle,
            Identifier,
            Reason,
            Data_Length,
            Data);
    }
}


void aci_hal_end_of_radio_activity_event(uint8_t Last_State,
        uint8_t Next_State,
        uint32_t Next_State_SysTime)
{
    /* Call the callback if one has been registered */
    if(BLE_CallBackInfo.BLE_aci_hal_end_of_radio_activity_event_cb != NULL)
    {
        BLE_CallBackInfo.BLE_aci_hal_end_of_radio_activity_event_cb(Last_State,
            Next_State,
            Next_State_SysTime);
    }
}


void aci_hal_scan_req_report_event(uint8_t RSSI,
        uint8_t Peer_Address_Type,
        uint8_t Peer_Address[6])
{
    /* Call the callback if one has been registered */
    if(BLE_CallBackInfo.BLE_aci_hal_scan_req_report_event_cb != NULL)
    {
        BLE_CallBackInfo.BLE_aci_hal_scan_req_report_event_cb(RSSI,
            Peer_Address_Type,
            Peer_Address);
    }
}


void aci_hal_fw_error_event(uint8_t FW_Error_Type,
        uint8_t Data_Length,
        uint8_t Data[])
{
    /* Call the callback if one has been registered */
    if(BLE_CallBackInfo.BLE_aci_hal_fw_error_event_cb != NULL)
    {
        BLE_CallBackInfo.BLE_aci_hal_fw_error_event_cb(FW_Error_Type,
            Data_Length,
            Data);
    }
}

void HAL_VTimerTimeoutCallback(uint8_t timerNum)
{
    /* Call the callback if one has been registered */
    if(BLE_CallBackInfo.BLE_HAL_VTimerTimeoutCallback_cb != NULL)
    {
        BLE_CallBackInfo.BLE_HAL_VTimerTimeoutCallback_cb(timerNum);
    }
}

/*********************************** end of ble_events.c *************************************/
