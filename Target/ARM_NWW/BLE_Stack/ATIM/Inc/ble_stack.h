/*
    / \     _   |_|
   / _ \  _| |_  _  _____
  / /_\ \(_   _)| ||     |
 / _____ \ | |  | || | | |
/_/     \_\\__) |_||_|_|_|
    (C)2018 Atim

Description: Application

License:

Maintainer: Youen Lebret

 */
#ifndef BLE_STACK_H
#define BLE_STACK_H

#include "hal_types.h"
#include "bluenrg1_stack.h"


/****************************************************************************************
 * Define definitions
 ****************************************************************************************/


/****************************************************************************************
 * Type definitions
 ****************************************************************************************/

/****************************************************************************************
 * Function prototypes
 ****************************************************************************************/
/**
 * @brief This function executes the processing of all Host Stack layers.
 *
 * The BTLE Stack Tick function has to be executed regularly to process incoming Link Layer packets and to process Host Layers procedures. All
 * stack callbacks are called by this function.
 *
 * If Low Speed Ring Oscillator is used instead of the LS Crystal oscillator this function performs also the LS RO calibration and hence must
 * be called at least once at every system wake-up in order to keep the 500 ppm accuracy (at least 500 ppm accuracy is mandatory if acting as a master).
 *
 * No BTLE stack function must be called while the BTLE_StackTick is running. For example, if a BTLE stack function may be called inside an
 * interrupt routine, that interrupt must be disabled during the execution of BTLE_StackTick(). Example (if a stack function may be called inside
 * UART ISR):
 * @code
 * NVIC_DisableIRQ(UART_IRQn);
 * BTLE_StackTick();
 * NVIC_EnableIRQ(UART_IRQn);
 * @endcode

 * @note The API name and parameters are subject to change in future releases.
 * @return None
 */
extern void BLE_StackTick(void);

/**
 * @brief The BTLE Stack initialization routine
 *
 * @note The API name and parameters are subject to change in future releases.
 *
 * @param[in]  BlueNRG_Stack_Init_params_p      pointer to the const structure containing memory and low level
 *                                              hardware configuration data for the device
 *
 * @return Value indicating success or error code.
 *
 */
extern tBleStatus BLE_BlueNRG_Stack_Initialization(const BlueNRG_Stack_Initialization_t *BlueNRG_Stack_Init_params_p);


/**
 * @brief Returns the BTLE Stack matching sleep mode
 *
 * @note The API name and parameters are subject to change in future releases.
 *
 * @return
 *  SLEEPMODE_RUNNING       = 0,
 *  SLEEPMODE_CPU_HALT      = 1,
 *  SLEEPMODE_WAKETIMER     = 2,
 *  SLEEPMODE_NOTIMER       = 3,
 */
extern uint8_t BLE_Stack_Perform_Deep_Sleep_Check(void);

/**
 *
 * @brief Radio ISR routine.
 *
 * This is the base function called for any radio ISR.
 *
 * @return None
 */
extern void BLE_RAL_Isr(void);

/**
 * @brief Starts a one-shot virtual timer for the given relative timeout value expressed in ms
 *
 * @note The API name and parameters are subject to change in future releases.
 *
 * @param[in]  timerNum            The virtual timer number [0..3]
 * @param[in]  msRelTimeout        The relative time, from current time, expressed in ms
 *                                 Note: abs(msRelTimeout) <= 5242879
 *
 * @return 0 if the timerNum virtual timer is valid.
 */
extern int BLE_HAL_VTimerStart_ms(uint8_t timerNum, int32_t msRelTimeout);

/**
 * @brief Stops the one-shot virtual timer specified if found
 *
 * @note The API name and parameters are subject to change in future releases.
 *
 * @param[in]  timerNum            The virtual timer number [0..3]
 *
 * @return None
 */
extern void BLE_HAL_VTimer_Stop(uint8_t timerNum);

/**
 * @brief This function return the current reference time expressed in internal time units.
 *
 * The returned value can be used as absolute time parameter where needed in the other
 * HAL_VTimer* APIs
 *
 * @note The API name and parameters are subject to change in future releases.
 *
 * @return  32bit absolute current time expressed in internal time units.
 */
extern uint32_t BLE_HAL_VTimerGetCurrentTime_sysT32(void);

/**
 * @brief This function returns the sum of an absolute time and a signed relative time.
 *
 * @param[in]  sysTime            Absolute time expressed in internal time units.
 * @param[in]  msTime             Signed relative time expressed in ms.
 *                                Note: abs(msTime) <= 5242879
 *
 * @note The API name and parameters are subject to change in future releases.
 *
 * @return  32bit resulting absolute time expressed in internal time units.
 */
extern uint32_t BLE_HAL_VTimerAcc_sysT32_ms(uint32_t sysTime, int32_t msTime);


/**
 * @brief Returns a time difference.
 *
 * This function return the difference between two absolute times: sysTime1-sysTime2.
 * The resulting value is expressed in ms.
 *
 * @param[in]  sysTime1           Absolute time expressed in internal time units.
 * @param[in]  sysTime2           Absolute time expressed in internal time units.
 *
 * @note The API name and parameters are subject to change in future releases.
 * @return  32bit resulting signed relative time expressed in ms.
 */
extern int32_t BLE_HAL_VTimerDiff_ms_sysT32(uint32_t sysTime1, uint32_t sysTime2);

/**
 * @brief Starts a one-shot timer.
 *
 * Starts a one-shot virtual timer for the given absolute timeout value expressed in
 * internal time units.
 *
 * @param[in]  timerNum       The virtual timer number [0..3]
 * @param[in]  time           Absolute time expressed in internal time units.
 *
 * @note The API name and parameters are subject to change in future releases.
 * @return 0 if the timerNum virtual timer is idle.
 */
extern int BLE_HAL_VTimerStart_sysT32(uint8_t timerNum, uint32_t time);


/**
 * @brief Returns the absolute expiry time of a running timer.
 *
 * Returned time is expressed in internal time units.
 *
 * @note The API name and parameters are subject to change in future releases.
 *
 * @param[in]  timerNum       The virtual timer number [0..3]
 * @param[out] sysTime        Absolute time expressed in internal time units.
 *
 * @return 0 if the timerNum virtual timer is running.
 */
extern int BLE_HAL_VTimerExpiry_sysT32(uint8_t timerNum, uint32_t *sysTime);

#endif /* BLE_STACK_H */
/*********************************** end of ble_stack.h *************************************/
