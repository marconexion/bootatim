/*
    / \     _   |_|
   / _ \  _| |_  _  _____
  / /_\ \(_   _)| ||   |
 / _____ \ | |  | || | | |
/_/     \_\\__) |_||_|_|_|
  (C)2018 Atim

Description: Application

License:

Maintainer: Youen Lebret

 */

/****************************************************************************************
 * Include files
 ****************************************************************************************/
#include "blt.h"
#include "com.h"
#include "uart.h"
#include "settings.h"
#include "ble.h"
#include "boot_ble.h"

/****************************************************************************************
 * Macro definitions
 ****************************************************************************************/

/****************************************************************************************
 * Define definitions
 ****************************************************************************************/

/****************************************************************************************
 * Type definitions
 ****************************************************************************************/

/****************************************************************************************
 * Static/global definitions
 ****************************************************************************************/
/**  Holds the communication interface of the currently active interface. */
static COM_InterfaceId_t COM_Interface = COM_IF_OTHER;

/****************************************************************************************
 * Static Function prototypes
 ****************************************************************************************/


/****************************************************************************************
 * Static Function Definition
 ****************************************************************************************/


/****************************************************************************************
 * Public Function Definitions
 ****************************************************************************************/

/************************************************************************************//**
 ** \brief     Initializes the communication module including the hardware needed for
 **            the communication.
 ** \return    none
 **
 ****************************************************************************************/
void COM_Init(void)
{
    UART_Config_t uart_conf;

    /* initialize the XCP communication protocol */
    XcpInit();
    /* Now get the uart parameters loaded in flash */
    uart_conf.baudrate = AT_GetSetting(AT_OFFSET_UART_BAUDRATE);
    uart_conf.datalength = AT_GetSetting(AT_OFFSET_UART_DATALENGTH);
    uart_conf.parity = AT_GetSetting(AT_OFFSET_UART_PARITY);
    uart_conf.stop = AT_GetSetting(AT_OFFSET_UART_STOP);
    uart_conf.flow = AT_GetSetting(AT_OFFSET_UART_FLOW);
    /* Finally Init UART with EEPROM baudrate */
    UART_InitExt(uart_conf);
} /*** end of COM_Init ***/


/************************************************************************************//**
 ** \brief     Updates the communication module by checking if new data was received
 **            and submitting the request to process newly received data.
 ** \return    none
 **
 ****************************************************************************************/
void COM_Task(void)
{
    blt_int8u xcpPacketLen;
    /* make xcpCtoReqPacket static for runtime efficiency */
    static blt_int8u xcpCtoReqPacket[BLT_CONF_C_UART_TX_MAX_DATA] ALIGN(4);

    if (UART_ReceivePacket(&xcpCtoReqPacket[0], &xcpPacketLen) == BLT_TRUE)
    {
        /* make this the active interface */
        COM_Interface = COM_IF_UART;
        /* process packet */
        XcpPacketReceived(&xcpCtoReqPacket[0], xcpPacketLen);
    }
    if (BOOTBLE_ReceivePacket(&xcpCtoReqPacket[0], &xcpPacketLen) == BLT_TRUE)
    {
        COM_Interface = COM_IF_BLE;
        /* process packet */
        XcpPacketReceived(&xcpCtoReqPacket[0], xcpPacketLen);
    }
} /*** end of COM_Task ***/


/************************************************************************************//**
 ** \brief     Transmits the packet using the xcp transport layer.
 ** \param     data Pointer to the byte buffer with packet data.
 ** \param     len  Number of data bytes that need to be transmitted.
 ** \return    none
 **
 ****************************************************************************************/
void COM_TransmitPacket(blt_int8u *data, blt_int16u len)
{
    /* transmit the packet. note that len is limited to 255 in the plausibility check,
     * so cast is okay.
     */
    if (COM_Interface == COM_IF_UART)
    {
        UART_TransmitPacket(data, (blt_int8u)len);
    }
    if (COM_Interface == COM_IF_BLE)
    {
        /* write the packet in the Tx Characteristics */
        BOOTBLE_TransmitPacket(data, (blt_int8u)len);
    }

    /* send signal that the packet was transmitted */
    XcpPacketTransmitted();
} /*** end of COM_TransmitPacket ***/


/************************************************************************************//**
 ** \brief     Obtains the maximum number of bytes that can be received on the specified
 **            communication interface.
 ** \return    Maximum number of bytes that can be received.
 **
 ****************************************************************************************/
blt_int16u COM_GetActiveInterfaceMaxRxLen(void)
{
    blt_int16u result;

    /* filter on communication interface identifier */
    switch (COM_Interface)
    {
    case COM_IF_UART:
        result = UART_RX_MAX_DATA;
        break;
    case COM_IF_BLE:
        result = BOOTBLE_RX_MAX_DATA;
        break;
    default:
        result = BLT_CONF_C_UART_RX_MAX_DATA;
        break;
    }

    return result;
} /*** end of COM_GetActiveInterfaceMaxRxLen ***/


/************************************************************************************//**
 ** \brief     Obtains the maximum number of bytes that can be transmitted on the
 **            specified communication interface.
 ** \return    Maximum number of bytes that can be received.
 **
 ****************************************************************************************/
blt_int16u COM_GetActiveInterfaceMaxTxLen(void)
{
    blt_int16u result;

    /* filter on communication interface identifier */
    switch (COM_Interface)
    {
    case COM_IF_UART:
        result = UART_TX_MAX_DATA;
        break;
    case COM_IF_BLE:
        result = BOOTBLE_TX_MAX_DATA;
        break;
    default:
        result = BLT_CONF_C_UART_TX_MAX_DATA;
        break;
    }

    return result;
} /*** end of COM_GetActiveInterfaceMaxTxLen ***/


/************************************************************************************//**
 ** \brief     This function obtains the XCP connection state.
 ** \return    BLT_TRUE when an XCP connection is established, BLT_FALSE otherwise.
 **
 ****************************************************************************************/
blt_bool COM_IsConnected(void)
{
    return XcpIsConnected();
} /*** end of COM_IsConnected ***/

/*********************************** end of com.c **************************************/
