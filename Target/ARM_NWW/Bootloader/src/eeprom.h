/*
	/ \	    _   |_|
   / _ \  _| |_  _  _____ 
  / /_\ \(_   _)| ||	 |
 / _____ \ | |  | || | | |
/_/	    \_\\__) |_||_|_|_|
	(C)2018 Atim

Description: Application 

License:

Maintainer: Lenoir Jean-Marc

*/

#ifndef _EEPROM_H_
#define _EEPROM_H_

#include <stdint.h>

#define EEPROM_AT_OFFSET 0x100

void EEPROM_Init(void);
void EEPROM_Read(uint16_t nAddress, uint8_t cNbBytes, void* pcBuffer);
void EEPROM_Write(uint16_t nAddress, uint8_t cNbBytes, const void* pcBuffer);

#endif // _EEPROM_H_
