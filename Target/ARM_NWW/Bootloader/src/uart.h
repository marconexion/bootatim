/*
    / \     _   |_|
   / _ \  _| |_  _  _____
  / /_\ \(_   _)| ||   |
 / _____ \ | |  | || | | |
/_/     \_\\__) |_||_|_|_|
  (C)2018 Atim

Description: Application

License:

Maintainer: Youen Lebret

 */
#ifndef UART_H
#define UART_H

/****************************************************************************************
 * Include files
 ****************************************************************************************/
#include "types.h"
#include "settings.h"

/****************************************************************************************
 * Macro definitions
 ****************************************************************************************/

/****************************************************************************************
 * Define definitions
 ****************************************************************************************/
/** Maximum RX DATA */
#define UART_RX_MAX_DATA            (255)

/** Maximum TX DATA */
#define UART_TX_MAX_DATA            (255)

/****************************************************************************************
 * Type definitions
 ****************************************************************************************/
typedef enum UART_Baudrates_e {
    UART_BAUDRATE_1200          = 0x00u,
    UART_BAUDRATE_2400          = 0x01u,
    UART_BAUDRATE_4800          = 0x02u,
    UART_BAUDRATE_9600          = 0x03u,
    UART_BAUDRATE_19200         = 0x04u,
    UART_BAUDRATE_38400         = 0x05u,
    UART_BAUDRATE_57600         = 0x06u,
    UART_BAUDRATE_115200        = 0x07u,
    UART_BAUDRATE_INVALID       = 0x08u
} UART_Baudrate_t;

typedef enum UART_DataLength_e {
    UART_DATALENGTH_7           = 0x07u,
    UART_DATALENGTH_8           = 0x08u
} UART_DataLength_t;

typedef enum UART_Parity_e {
    UART_PARITY_NONE            = 0x00u,
    UART_PARITY_ODD             = 0x01u,
    UART_PARITY_EVEN            = 0x02u
} UART_Parity_t;

typedef enum UART_Stop_e {
    UART_STOP_1                 = 0x01u,
    UART_STOP_2                 = 0x02u
} UART_Stop_t;

typedef enum UART_Flow_e {
    UART_FLOW_NONE              = 0x01u,
    UART_FLOW_CTSRTSBUFFERMODE  = 0x02u
} UART_Flow_t;

typedef struct UART_Config_s {
    UART_Baudrate_t baudrate;
    UART_DataLength_t datalength;
    UART_Parity_t parity;
    UART_Stop_t stop;
    UART_Flow_t flow;
} UART_Config_t;

/****************************************************************************************
 * Function prototypes
 ****************************************************************************************/
void     UART_InitExt(UART_Config_t uart_config);
void     UART_TransmitPacket(blt_int8u *data, blt_int8u len);
blt_bool UART_ReceivePacket(blt_int8u *data, blt_int8u *len);

#endif /* UART_H */
/*********************************** end of uart.h *************************************/
