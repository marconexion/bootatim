/*
	/ \	    _   |_|
   / _ \  _| |_  _  _____ 
  / /_\ \(_   _)| ||	 |
 / _____ \ | |  | || | | |
/_/	    \_\\__) |_||_|_|_|
	(C)2018 Atim

Description: Application 

License:

Maintainer: Lenoir Jean-Marc

*/

#include <stdbool.h>
#include "eeprom.h"

void EepromRead(uint16_t nAddress, uint8_t cNbBytes, uint8_t* pcBuffer) {
    EEPROM_Read(nAddress, cNbBytes, pcBuffer);
}
void EepromWrite(uint16_t nAddress, uint8_t cNbBytes, uint8_t* pcBuffer) {
    EEPROM_Write(nAddress, cNbBytes, pcBuffer);
}

#if 0 // Emulation EEPROM en RAM

#define EEPROM_SIZE 0x700

#include <string.h>
#include "BlueNRG_x_device.h"

static uint8_t eeprom_data[EEPROM_SIZE];

void EEPROM_Init(void) {
    memset(eeprom_data, 0, sizeof(eeprom_data));
}

void EEPROM_Read(uint16_t nAddress, uint8_t cNbBytes, uint8_t* pcBuffer) {
    assert_param(nAddress+cNbBytes <= EEPROM_SIZE);
    if(nAddress+cNbBytes > EEPROM_SIZE)
        return;
    memcpy(pcBuffer, eeprom_data+nAddress, cNbBytes);
}

void EEPROM_Write(uint16_t nAddress, uint8_t cNbBytes, const uint8_t* pcBuffer) {
    assert_param(nAddress+cNbBytes <= EEPROM_SIZE);
    if(nAddress+cNbBytes > EEPROM_SIZE)
        return;
    memcpy(eeprom_data+nAddress, pcBuffer, cNbBytes);
}

#else // Emulation EEPROM en Flash

// L'EEPROM est �mul�e sur la derni�re page de la flash
// Note : les 64 derniers bits de la flash sont utilis�s pour activer la protection readout, et doivent tous rester � 1 pour laisser cette protection d�sactiv�e

#include <string.h>
#include "BlueNRG1_flash.h"

#define EEPROM_USER_SIZE                    ((N_BYTES_PAGE)-(N_BYTES_READOUT_PROTECTION))  // taille d'EEPROM � disposition de l'utilisateur

#define N_BYTES_READOUT_PROTECTION          (8)             // octets utilis�s pour activer la protection en lecture de la flash
#define EEPROM_TOTAL_SIZE                   (N_BYTES_PAGE)  // taille totale de l'EEPROM utilis�e
#define EEPROM_PAGE_NUMBER                  ((N_PAGES)-1)   // utilise la derni�re page de flash
#define EEPROM_FLASH_OFFSET                 ((EEPROM_PAGE_NUMBER)*(N_BYTES_PAGE))
#define EEPROM_START                        ((FLASH_START)+(EEPROM_FLASH_OFFSET))
#define EEPROM_END                          ((EEPROM_START)+(N_BYTES_PAGE)-1)

#define N_WORDS_BURST                       (4)
#define N_BURST                            ((N_BYTES_WORD)*(N_WORDS_BURST))

static uint8_t EEPROM_buffer[EEPROM_TOTAL_SIZE];

void EEPROM_Init(void) {
    // on initialise certains octets pour �viter que la protection en lecture de la flash soit activ�e par erreur par la suite
    memset(EEPROM_buffer+EEPROM_USER_SIZE, 0xFF, N_BYTES_READOUT_PROTECTION);
}

// Copie le contenu de la page de la flash correspondant � l'EEPROM dans la variable EEPROM_buffer
static void EEPROM_GetFlashPage(void) {
#if 0 // utilisation FLASH_ReadByte
    for(uint16_t i = 0 ; i < N_BYTES_PAGE ; i++) {
        EEPROM_buffer[i] = FLASH_ReadByte(EEPROM_START+i);
    }
#else // utilisation FLASH_ReadWord
    uint32_t word;
    
    for(uint16_t i = 0 ; i < N_BYTES_PAGE/N_BYTES_WORD ; i++) {
        word = FLASH_ReadWord(EEPROM_START+i*N_BYTES_WORD);
        ((uint32_t *)EEPROM_buffer)[i] = word;
    }
#endif
}

// R��crit le contenu de la page de la flash correspondant � l'EEPROM par le contenu de la variable EEPROM_buffer
// Si erase_page == true, on �crase le contenu de la page avant de la mettre � jour (n�cessaire si des bits passent de 0 � 1)
static void EEPROM_SetFlashPage(bool erase_page) {
    if(erase_page) {
        // Ecrase le contenu actuel de la flash
        FLASH_ErasePage(EEPROM_PAGE_NUMBER);
        while(FLASH_GetFlagStatus(Flash_CMDDONE) != SET);
    }
    
    // R��crit le contenu de la flash
#if 1 // utilisation FLASH_ProgramWord ; ne r��crit que les mots qui changent (qui n'ont pas tous leurs bits � 1 si la page a �t� �cras�e)
    uint32_t eeprom_word, new_word;
    
    for(uint16_t i = 0 ; i < N_BYTES_PAGE/N_BYTES_WORD ; i++) {
        if(erase_page)
            eeprom_word = 0xFFFFFFFF;
        else
            eeprom_word = FLASH_ReadWord(EEPROM_START+i*N_BYTES_WORD);
        
        new_word = ((uint32_t *)EEPROM_buffer)[i];
        
        if(new_word != eeprom_word) {
            FLASH_ProgramWord(EEPROM_START+i*N_BYTES_WORD, new_word);
            while(FLASH_GetFlagStatus(Flash_CMDDONE) != SET);
        }
    }
#else // utilisation FLASH_ProgramWordBurst ; ne v�rifie pas si les mots changent ou non avant de les modifier
    for(uint16_t i = 0 ; i < N_BYTES_PAGE/N_BURST ; i++) {
        FLASH_ProgramWordBurst(EEPROM_START+i*N_BURST, (uint32_t *)&EEPROM_buffer[i*N_BURST]);
        while(FLASH_GetFlagStatus(Flash_CMDDONE) != SET);
    }
#endif
}

void EEPROM_Read(uint16_t nAddress, uint8_t cNbBytes, void* pcBuffer) {
    uint8_t byte;
    
    // on est en dehors de l'espace d'adressage de l'EEPROM
    assert_param(nAddress+cNbBytes <= EEPROM_USER_SIZE);
    if(nAddress+cNbBytes > EEPROM_USER_SIZE)
        return;
    
    for(uint16_t i = 0 ; i < cNbBytes ; i++) {
        byte = FLASH_ReadByte(EEPROM_START+nAddress+i);
        ((uint8_t *)pcBuffer)[i] = byte;
    }
}

#if 0 // algorithme simpliste (�crase toute la page flash � chaque �criture
void EEPROM_Write(uint16_t nAddress, uint8_t cNbBytes, const void* pcBuffer) {
    // on est en dehors de l'espace d'adressage de l'EEPROM
    assert_param(nAddress+cNbBytes <= EEPROM_USER_SIZE);
    if(nAddress+cNbBytes > EEPROM_USER_SIZE)
        return;
    
    // aucune donn�e � �crire
    assert_param(cNbBytes != 0);
    if(cNbBytes == 0)
        return;
    
    EEPROM_GetFlashPage();
    
    for(uint16_t i = 0 ; i < cNbBytes ; i++) {
        EEPROM_buffer[nAddress+i] = ((uint8_t *)pcBuffer)[i];
    }
    
    EEPROM_SetFlashPage(true);
}
#else // algorithme plus complexe ne mettant � jour la flash que si n�cessaire
void EEPROM_Write(uint16_t nAddress, uint8_t cNbBytes, const void* pcBuffer) {
    bool update_needed; // indique si besoin de modifier la flash (sans forc�ment �craser la page courante)
    bool erase_needed;  // indique si besoin d'�craser la page flash avant de r��crire les donn�es (passage de certains bits de 0 � 1)
    uint8_t eeprom_byte, new_byte;
    
    // on est en dehors de l'espace d'adressage de l'EEPROM
    assert_param(nAddress+cNbBytes <= EEPROM_USER_SIZE);
    if(nAddress+cNbBytes > EEPROM_USER_SIZE)
        return;
    
    // aucune donn�e � �crire
    assert_param(cNbBytes != 0);
    if(cNbBytes == 0)
        return;
    
    EEPROM_GetFlashPage(); // copie du contenu de la flash en RAM
    
    // v�rification des diff�rences entre le contenu en flash actuel et le contenu � mettre � jour
    update_needed = false;
    erase_needed = false;
    for(uint16_t i = 0 ; i < cNbBytes ; i++) {
        eeprom_byte = EEPROM_buffer[nAddress+i];
        new_byte = ((uint8_t *)pcBuffer)[i];
        
        if(new_byte != eeprom_byte) { // diff�rence entre le contenu de l'EEPROM et le contenu � mettre � jour
            update_needed = true;
            
            if((new_byte | eeprom_byte) != eeprom_byte) { // certains bits passent de 0 � 1, donc il faut �craser la page flash pour mettre � jour le contenu
                erase_needed = true;
                break;
            }
        }
    }
    
    if(update_needed) {
        // on modifie la copie de la page flash en RAM par son nouveau contenu
        for(uint16_t i = 0 ; i < cNbBytes ; i++) {
            EEPROM_buffer[nAddress+i] = ((uint8_t *)pcBuffer)[i];
        }
        
        // La flash du BlueNRG ne supporte que 2 mises � jour de donn�es � une adresse (avec uniquement des bits passant de 1 � 0) sans �craser le contenu de la page.
        // Pour ne pas mettre en place un algorithme trop compliqu�, sur cette architecture on force avant chaque �criture l'�crasement de la page, m�me si on ne fait que des passages de bits de 1 � 0.
        erase_needed = true;
        
        // on met � jour le contenu de la page flash avec les nouvelles donn�es
        EEPROM_SetFlashPage(erase_needed);
    }
}
#endif

#endif
