/*
	/ \	    _   |_|
   / _ \  _| |_  _  _____ 
  / /_\ \(_   _)| ||	 |
 / _____ \ | |  | || | | |
/_/	    \_\\__) |_||_|_|_|
	(C)2018 Atim

Description: Application 

License:

Maintainer: Lenoir Jean-Marc

*/

#ifndef _SETTINGS_H_
#define _SETTINGS_H_

#include <stdbool.h>
#include <stdint.h>
#include "uart.h"


#define OFFSET_COMMAND_ATS  0

#define AT_OFFSET_UART_BAUDRATE                     (OFFSET_COMMAND_ATS+12)
#define AT_OFFSET_UART_DATALENGTH                   (OFFSET_COMMAND_ATS+13)
#define AT_OFFSET_UART_PARITY                       (OFFSET_COMMAND_ATS+14)
#define AT_OFFSET_UART_STOP                         (OFFSET_COMMAND_ATS+15)
#define AT_OFFSET_UART_FLOW                         (OFFSET_COMMAND_ATS+16)


typedef uint16_t AT_Reg_t; // type de variable utilis� pour stocker une adresse de registre AT
typedef uint8_t AT_Val_t; // type de variable utilis� pour stocker une valeur de registre AT

// Retourne un r�glage AT
AT_Val_t AT_GetSetting(AT_Reg_t AT_register);


#endif // _SETTINGS_H_
