/*
  / \     _   |_|
   / _ \  _| |_  _  _____
  / /_\ \(_   _)| ||   |
 / _____ \ | |  | || | | |
/_/     \_\\__) |_||_|_|_|
  (C)2018 Atim

Description: Application

License:

Maintainer: Youen Lebret

 */
#ifndef TIMER_H
#define TIMER_H

/****************************************************************************************
 * Function prototypes
 ****************************************************************************************/
void       TIMER_Init(void);
blt_int32u TIMER_Get(void);

#endif /* TIMER_H */
/*********************************** end of timer.h ************************************/
