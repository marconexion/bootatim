/*
    / \     _   |_|
   / _ \  _| |_  _  _____
  / /_\ \(_   _)| ||     |
 / _____ \ | |  | || | | |
/_/     \_\\__) |_||_|_|_|
    (C)2018 Atim

Description: Application

License:

Maintainer: Youen Lebret

 */
#ifndef FLASH_H
#define FLASH_H

/****************************************************************************************
 * Function prototypes
 ****************************************************************************************/
void     FLASH_Init(void);
blt_bool FLASH_Write(blt_addr addr, blt_int32u len, blt_int8u *data);
blt_bool FLASH_Erase(blt_addr addr, blt_int32u len);
blt_bool FLASH_WriteChecksum(void);
blt_bool FLASH_VerfiyChecksum(void);
blt_bool FLASH_Done(void);
blt_addr FLASH_GetUserBaseAddr(void);


#endif /* FLASH_H */
/*********************************** end of flash.h ************************************/
