/*
    / \     _   |_|
   / _ \  _| |_  _  _____
  / /_\ \(_   _)| ||     |
 / _____ \ | |  | || | | |
/_/     \_\\__) |_||_|_|_|
    (C)2018 Atim

Description: Application

License:

Maintainer: Youen Lebret

 */

#ifndef TYPES_H
#define TYPES_H


/****************************************************************************************
 * Macro definitions
 ****************************************************************************************/
/** \brief Boolean true value. */
#define BLT_TRUE       (1)
/** \brief Boolean false value. */
#define BLT_FALSE      (0)
/** \brief NULL pointer value. */
#define BLT_NULL       ((void *)0)


/****************************************************************************************
 * Type definitions
 ****************************************************************************************/
typedef unsigned char   blt_bool;                     /**<  boolean type               */
typedef char            blt_char;                     /**<  character type             */
typedef unsigned long   blt_addr;                     /**<  memory address type        */
typedef unsigned char   blt_int8u;                    /**<  8-bit unsigned integer     */
typedef signed char     blt_int8s;                    /**<  8-bit   signed integer     */
typedef unsigned short  blt_int16u;                   /**< 16-bit unsigned integer     */
typedef signed short    blt_int16s;                   /**< 16-bit   signed integer     */
typedef unsigned int    blt_int32u;                   /**< 32-bit unsigned integer     */
typedef signed int      blt_int32s;                   /**< 32-bit   signed integer     */


#endif /* TYPES_H */
/*********************************** end of types.h ************************************/
