/*
    / \     _   |_|
   / _ \  _| |_  _  _____
  / /_\ \(_   _)| ||     |
 / _____ \ | |  | || | | |
/_/     \_\\__) |_||_|_|_|
    (C)2018 Atim

Description: Application

License:

Maintainer: Youen Lebret

 */

/****************************************************************************************
 * Include files
 ****************************************************************************************/
#include "blt.h"
#include "clock.h"

/****************************************************************************************
 * Local data declarations
 ****************************************************************************************/


/************************************************************************************//**
 ** \brief     Initializes the polling based millisecond timer driver.
 ** \return    none.
 **
 ****************************************************************************************/
void TIMER_Init(void)
{
    Clock_Init();
} /*** end of TIMER_Init ***/



/************************************************************************************//**
 ** \brief     Obtains the counter value of the millisecond timer.
 ** \return    Current value of the millisecond timer.
 **
 ****************************************************************************************/
blt_int32u TIMER_Get(void)
{
    blt_int32u lCurrentTime;

    lCurrentTime = Clock_Time();

    return lCurrentTime;
} /*** end of TIMER_Get ***/


/*********************************** end of timer.c ************************************/
