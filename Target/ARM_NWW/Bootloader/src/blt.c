/*
    / \     _   |_|
   / _ \  _| |_  _  _____
  / /_\ \(_   _)| ||   |
 / _____ \ | |  | || | | |
/_/     \_\\__) |_||_|_|_|
  (C)2018 Atim

Description: Application

License:

Maintainer: Youen Lebret

 */

/****************************************************************************************
 * Include files
 ****************************************************************************************/
#include "blt.h"
#include "flash.h"
#include "BlueNRG1_gpio.h"

/****************************************************************************************
 * Macro definitions
 ****************************************************************************************/

/****************************************************************************************
 * Define definitions
 ****************************************************************************************/
/** Time (in ms) for the bootloader to wait before try to jump to user application
 * if no host connection is set up */
#define BLT_WAIT_TIME           1000u

/** Bypass pin. If this pin is in high Sate, do not wait the BLT_WAIT_TIME, but jump
 * immediatly in applicativ zone */
#define BLT_BYPASS_PIN          GPIO_Pin_20

/****************************************************************************************
 * Type definitions
 ****************************************************************************************/

/****************************************************************************************
 * Static/global definitions
 ****************************************************************************************/
static blt_int32u lStartTime = 0u;
/****************************************************************************************
 * Static Function prototypes
 ****************************************************************************************/
static void BLT_CheckEnd(void);

/****************************************************************************************
 * Static Function Definition
 ****************************************************************************************/

static void BLT_CheckEnd(void)
{
    BitAction lBypassPinState;

    /* If an Host is connected, do not jump to user application. Wait for the end
     * of connection */
    if (COM_IsConnected() != BLT_TRUE)
    {
        /* Read bypass pin */
        lBypassPinState = GPIO_ReadBit(BLT_BYPASS_PIN);

        /* No host connection, check if it is time to go to the user application */
        if ((TIMER_Get() - lStartTime >= (BLT_WAIT_TIME)) ||
            (Bit_SET == lBypassPinState))
        {
            /* Bootloader is waiting for a connection for too long, jump to the user application */
            USER_Start();
        }
    }
} /*** end of BLT_CheckEnd ***/

/****************************************************************************************
 * Public Function Definitions
 ****************************************************************************************/
void BLT_Init(void)
{
    GPIO_InitType lGPIOBypass;

    /* Initialize GPIO CTS (DIO20) which enable bootloader bypass */
    lGPIOBypass.GPIO_Pin = BLT_BYPASS_PIN;
    lGPIOBypass.GPIO_Mode = GPIO_Input;
    /* Pull down */
    lGPIOBypass.GPIO_Pull = ENABLE;
    GPIO_Init(&lGPIOBypass);

    /* initialize the millisecond timer */
    TIMER_Init();
    /* initialize the Flash driver */
    FLASH_Init();
    /* initialize the communication module */
    COM_Init();

    lStartTime = TIMER_Get();

} /*** end of BLT_Init ***/

void BLT_Task(void)
{
    /* process possibly pending communication data */
    COM_Task();
    /* control the end of bootloader (timer elapsed or re-programmation finished or Bypass pin in Low State) */
    BLT_CheckEnd();
} /*** end of BLT_Task ***/


/*********************************** end of blt.c *************************************/
