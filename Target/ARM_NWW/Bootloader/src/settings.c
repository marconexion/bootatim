/*
	/ \	    _   |_|
   / _ \  _| |_  _  _____ 
  / /_\ \(_   _)| ||	 |
 / _____ \ | |  | || | | |
/_/	    \_\\__) |_||_|_|_|
	(C)2018 Atim

Description: Application 

License:

Maintainer: Lenoir Jean-Marc

*/

#include <stdio.h>
#include <string.h>
#include "settings.h"
#include "eeprom.h"


// Retourne un réglage AT
AT_Val_t AT_GetSetting(AT_Reg_t AT_register)
{
    AT_Val_t setting = 0u;

    EEPROM_Read(EEPROM_AT_OFFSET + (1u + AT_register), sizeof(setting), &setting);

    return setting;
}
