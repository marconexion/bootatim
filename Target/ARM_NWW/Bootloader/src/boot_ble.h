/*
    / \     _   |_|
   / _ \  _| |_  _  _____
  / /_\ \(_   _)| ||   |
 / _____ \ | |  | || | | |
/_/     \_\\__) |_||_|_|_|
  (C)2018 Atim

Description: Application

License:

Maintainer: Youen Lebret

 */
#ifndef BOOT_BLE_H
#define BOOT_BLE_H

#include "hal_types.h"
#include "bluenrg1_stack.h"
#include "stack_user_cfg.h"
/****************************************************************************************
 * Type definitions
 ****************************************************************************************/
/****************************************************************************************
 *   BLE  C O N F I G U R A T I O N
 ****************************************************************************************/
/* This file contains all the information needed to init the BlueNRG-1 stack.
 * These constants and variables are used from the BlueNRG-1 stack to reserve RAM and FLASH
 * according the application requests.
 */

/* Default number of link */
#define BOOTBLE_MIN_NUM_LINK                1
/* Default number of GAP and GATT services */
#define BOOTBLE_DEFAULT_NUM_GATT_SERVICES   2
/* Default number of GAP and GATT attributes */
#define BOOTBLE_DEFAULT_NUM_GATT_ATTRIBUTES 9

/* Number of services requests from the OTA Service manager application */
#define BOOTBLE_NUM_APP_GATT_SERVICES 1

/* Number of attributes requests from the OTA Service manager application */
#define BOOTBLE_NUM_APP_GATT_ATTRIBUTES 9

/* Number of links needed for the demo: 1
 * Only 1 the default
 */
#define BOOTBLE_NUM_LINKS               (BOOTBLE_MIN_NUM_LINK)

/* Number of GATT services needed for the OTA Service Manager application. */
#define BOOTBLE_NUM_GATT_SERVICES       (BOOTBLE_DEFAULT_NUM_GATT_SERVICES + BOOTBLE_NUM_APP_GATT_SERVICES)

/* Number of GATT attributes needed for the OTA Service Manager application. */
#define BOOTBLE_NUM_GATT_ATTRIBUTES     (BOOTBLE_DEFAULT_NUM_GATT_ATTRIBUTES + BOOTBLE_NUM_APP_GATT_ATTRIBUTES)

/* Enable/disable Data length extension Max supported ATT_MTU size based on OTA client & server Max ATT_MTU sizes capabilities */
#define BOOTBLE_MAX_ATT_MTU_SIZE    (255)    /* DEFAULT_ATT_MTU size = 23 bytes */
/*#define MAX_ATT_MTU_SIZE    (23)*/    /* DEFAULT_ATT_MTU size = 23 bytes */

/**
 * Set the number of 16-bytes units used on an OTA FW data packet for matching OTA client MAX ATT_MTU
 */
#define BOOTBLE_16_BYTES_BLOCKS_NUMBER ((BOOTBLE_MAX_ATT_MTU_SIZE-4)/16)   /* 4 bytes for OTA sequence numbers + needs ack + checksum bytes */

/* Array size for the attribute value for OTA service */
/* (GATT + GAP) = 44 bytes; OTA service: 4 characteristics (1 notify property): 99 bytes +
   Image Content characteristic length = 4  + (BOOTBLE_16_BYTES_BLOCKS_NUMBER * 16); 4 for sequence number, checksum and needs acks bytes */
#define BOOTBLE_ATT_VALUE_ARRAY_SIZE    (44 + 99 + (4 + (BOOTBLE_16_BYTES_BLOCKS_NUMBER * 16)))

/* Flash security database size */
#define BOOTBLE_FLASH_SEC_DB_SIZE       (0x400)

/* Flash server database size */
#define BOOTBLE_FLASH_SERVER_DB_SIZE    (0x400)

/* Set supported max value for attribute size: it is the biggest attribute size enabled by the application */
#define BOOTBLE_MAX_ATT_SIZE            (4 + (BOOTBLE_16_BYTES_BLOCKS_NUMBER * 16))  /* max value which can be supported on DTM client side to fit a single ATT packet */

/* Set supported max value for ATT_MTU enabled by the application */
#define BOOTBLE_MAX_ATT_MTU             (BOOTBLE_MAX_ATT_MTU_SIZE)


/* Set the minumum number of prepare write requests needed for a long write procedure for a characteristic with len > 20bytes:
 *
 * It returns 0 for characteristics with len <= 20bytes
 *
 * NOTE: If prepare write requests are used for a characteristic (reliable write on multiple characteristics), then
 * this value should be set to the number of prepare write needed by the application.
 *
 *  [New parameter added on BLE stack v2.x]
 */
#define BOOTBLE_PREPARE_WRITE_LIST_SIZE PREP_WRITE_X_ATT(BOOTBLE_MAX_ATT_SIZE)

/* Additional number of memory blocks  to be added to the minimum  */
#define BOOTBLE_OPT_MBLOCKS   (6) /* 6:  for reaching the max throughput: ~220kbps (same as BLE stack 1.x) */

/* Set the number of memory block for packet allocation */
#define BOOTBLE_MBLOCKS_COUNT           (MBLOCKS_CALC(BOOTBLE_PREPARE_WRITE_LIST_SIZE, BOOTBLE_MAX_ATT_MTU, BOOTBLE_NUM_LINKS) + BOOTBLE_OPT_MBLOCKS)

/* Maximum duration of the connection event */
#define BOOTBLE_MAX_CONN_EVENT_LENGTH 0xFFFFFFFF

/* Sleep clock accuracy */
#if (LS_SOURCE == LS_SOURCE_INTERNAL_RO)

/* Sleep clock accuracy in Slave mode */
#define BOOTBLE_SLAVE_SLEEP_CLOCK_ACCURACY 500

/* Sleep clock accuracy in Master mode */
#define BOOTBLE_MASTER_SLEEP_CLOCK_ACCURACY MASTER_SCA_500ppm

#else

/* Sleep clock accuracy in Slave mode */
#define BOOTBLE_SLAVE_SLEEP_CLOCK_ACCURACY 100

/* Sleep clock accuracy in Master mode */
#define BOOTBLE_MASTER_SLEEP_CLOCK_ACCURACY MASTER_SCA_100ppm

#endif

/* Low Speed Oscillator source */
#if (LS_SOURCE == LS_SOURCE_INTERNAL_RO)
#define BOOTBLE_LOW_SPEED_SOURCE  1 // Internal RO
#else
#define BOOTBLE_LOW_SPEED_SOURCE  0 // External 32 KHz
#endif

/* High Speed start up time */
#define BOOTBLE_HS_STARTUP_TIME 328 // 800 us

/* Radio Config Hot Table */
extern uint8_t hot_table_radio_config[];

/* Low level hardware configuration data for the device */
#define BOOTBLE_CONFIG_TABLE                            \
{                                                      \
    (uint32_t*)hot_table_radio_config,                 \
    BOOTBLE_MAX_CONN_EVENT_LENGTH,                      \
    BOOTBLE_SLAVE_SLEEP_CLOCK_ACCURACY,                 \
    BOOTBLE_MASTER_SLEEP_CLOCK_ACCURACY,                \
    BOOTBLE_LOW_SPEED_SOURCE,                           \
    BOOTBLE_HS_STARTUP_TIME                             \
}


/** \brief Configure number of bytes in the target->host data packet.
 * (MAX_ATT_MTU_SIZE -  4 bytes for OTA sequence numbers) */
#define BOOTBLE_TX_MAX_DATA       (BOOTBLE_MAX_ATT_MTU_SIZE - 4u)
/** \brief Configure number of bytes in the host->target data packet.
 * (MAX_ATT_MTU_SIZE -  4 bytes for OTA sequence numbers) */
#define BOOTBLE_RX_MAX_DATA       (BOOTBLE_MAX_ATT_MTU_SIZE - 4u)

/****************************************************************************************
 * Function prototypes
 ****************************************************************************************/
tBleStatus BOOTBLE_Init(void);

/************************************************************************************//**
 ** \brief     Receives a communication interface packet if one is present.
 ** \param     data Pointer to byte array where the data is to be stored.
 ** \param     len Pointer where the length of the packet is to be stored.
 ** \return    TRUE if a packet was received, FALSE otherwise.
 **
 ****************************************************************************************/
BOOL BOOTBLE_ReceivePacket(uint8_t *data, uint8_t *len);

/************************************************************************************//**
 ** \brief     Transmits a packet formatted for the communication interface.
 ** \param     data Pointer to byte array with data that it to be transmitted.
 ** \param     len  Number of bytes that are to be transmitted.
 ** \return    none.
 **
 ****************************************************************************************/
void BOOTBLE_TransmitPacket(uint8_t *data, uint8_t len);


/************************************************************************************//**
 ** \brief     Change the connection interval to the minimum (7.5ms) to increase
 ** data transfert
 ** \param     none.
 ** \return    none.
 **
 ****************************************************************************************/
void BOOTBLE_SetMinConnectionIntervall(void);

/************************************************************************************//**
 ** \brief     Get the connection Handle
 ** \param     none.
 ** \return    none.
 **
 ****************************************************************************************/
uint16_t BOOTBLE_GetConnectionHandle(void);

#endif /* BOOT_BLE_H */
/*********************************** end of boot_ble.h *************************************/
