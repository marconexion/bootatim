/*
	/ \	    _   |_|
   / _ \  _| |_  _  _____
  / /_\ \(_   _)| ||	 |
 / _____ \ | |  | || | | |
/_/	    \_\\__) |_||_|_|_|
	(C)2018 Atim

Description: Application

License:

Maintainer: Youen Lebret

 */

/****************************************************************************************
 * Include files
 ****************************************************************************************/
#include "blt_conf.h"
#include "types.h"
#include "xcp.h"
#include "ble.h"
#include "boot_ble.h"
#include "ble_status.h"
#include "bluenrg1_api.h"
#include "bluenrg1_events.h"
#include "bluenrg1_gap.h"
#include "bluenrg1_hal.h"
#include "bluenrg1_gatt_server.h"
#include <string.h>

#include "sleep.h"


/****************************************************************************************
 * Macro definitions
 ****************************************************************************************/



/****************************************************************************************
 * Define definitions
 ****************************************************************************************/
/** Minimum interval for connection */
#define BOOTBLE_MIN_CONN_INTERVAL    (0x6u)

/** MAximum interval for connection */
#define BOOTBLE_MAX_ADV_INTERVAL    (0x4000)

/** Address where the unique device ID is stored */
#define BOOTBLE_DEVICE_ADDR     (0x100007F4u)
/****************************************************************************************
 * Static Function prototypes
 ****************************************************************************************/

/* callback definition */
static void BOOTBLE_ConnectCompleteEvent(uint8_t Status,
        uint16_t Connection_Handle,
        uint8_t Role,
        uint8_t Peer_Address_Type,
        uint8_t Peer_Address[6],
        uint16_t Conn_Interval,
        uint16_t Conn_Latency,
        uint16_t Supervision_Timeout,
        uint8_t Master_Clock_Accuracy);

static void BOOTBLE_AttributeModifiedEvent(uint16_t Connection_Handle,
        uint16_t Attr_Handle,
        uint16_t Offset,
        uint16_t Attr_Data_Length,
        uint8_t Attr_Data[]);

static void BOOTBLE_DisconnectEvent(uint8_t Status,
        uint16_t Connection_Handle,
        uint8_t Reason);

/****************************************************************************************
 * Static/global definitions
 ****************************************************************************************/

/* RAM reserved to manage all the data stack according the number of links,
 * number of services, number of attributes and attribute value length
 */
NO_INIT(uint32_t BOOTBLE_DynAllocA[TOTAL_BUFFER_SIZE(BOOTBLE_NUM_LINKS,BOOTBLE_NUM_GATT_ATTRIBUTES,BOOTBLE_NUM_GATT_SERVICES,BOOTBLE_ATT_VALUE_ARRAY_SIZE,
        BOOTBLE_MBLOCKS_COUNT,CONTROLLER_DATA_LENGTH_EXTENSION_ENABLED)>>2]);

/* FLASH reserved to store all the security database information and
 * and the server database information
 */
NO_INIT_SECTION(uint32_t BOOTBLE_StacklibFlashData[TOTAL_FLASH_BUFFER_SIZE(BOOTBLE_FLASH_SEC_DB_SIZE, BOOTBLE_FLASH_SERVER_DB_SIZE)>>2], ".noinit.stacklib_flash_data");

/* FLASH reserved to store: security root keys, static random address, public address */
NO_INIT_SECTION(uint8_t BOOTBLE_StacklibStoredDeviceIdData[56], ".noinit.stacklib_stored_device_id_data");

/* This structure contains memory and low level hardware configuration data for the device */
const BlueNRG_Stack_Initialization_t BOOTBLE_BlueNRGStackInitParams = {
        (uint8_t*)BOOTBLE_StacklibFlashData,
        BOOTBLE_FLASH_SEC_DB_SIZE,
        BOOTBLE_FLASH_SERVER_DB_SIZE,
        (uint8_t*)BOOTBLE_StacklibStoredDeviceIdData,
        (uint8_t*)BOOTBLE_DynAllocA,
        TOTAL_BUFFER_SIZE(BOOTBLE_NUM_LINKS,BOOTBLE_NUM_GATT_ATTRIBUTES,BOOTBLE_NUM_GATT_SERVICES,BOOTBLE_ATT_VALUE_ARRAY_SIZE,BOOTBLE_MBLOCKS_COUNT,CONTROLLER_DATA_LENGTH_EXTENSION_ENABLED),
        BOOTBLE_NUM_GATT_ATTRIBUTES,
        BOOTBLE_NUM_GATT_SERVICES,
        BOOTBLE_ATT_VALUE_ARRAY_SIZE,
        BOOTBLE_NUM_LINKS,
        0, /* reserved for future use */
        BOOTBLE_PREPARE_WRITE_LIST_SIZE,
        BOOTBLE_MBLOCKS_COUNT,
        BOOTBLE_MAX_ATT_MTU,
        BOOTBLE_CONFIG_TABLE,
};
/** Device service name in scan process */
static uint8_t BOOTBLE_serviceUUID4Scan[18]= {0x11,0x06,0x36,0x3d,0x74,0xd0,0x24,0x42,0x4e,0xdc,0x9a,0x0f,0x00,0x3e,0xb4,0xf4,0xa9,0xe6};

/** \brief XCP service Handle number (updated after adding xcp service to GATT). */
static uint16_t BOOTBLE_ServHandle = 0u;

/** \brief Tx characteristic Handle number (updated after adding tx char to xcp service). */
static uint16_t BOOTBLE_TxCharHandle = 0u;

/** \brief Rx characteristic Handle number (updated after adding tx char to xcp service). */
static uint16_t BOOTBLE_RxCharHandle = 0u;

/** device name during discover process */
static uint8_t BOOTBLE_DeviceName[10u] = {'X','C','P','S','e','r','v','i','c','e'};

/** \brief List of Service to Add to the GATT Service */
static BLE_ServiceInfo_t BLE_V_ServiceInfo = {
        {0xe6,0xa9,0xf4,0xb4,0x3e,0x00,0x0f,0x9a,0xdc,0x4e,0x42,0x24,0xd0,0x74,0x3d,0x36},
        BLE_UUID_TYPE_128,
        BLE_PRIMARY_SERVICE,
        10u,
        &BOOTBLE_ServHandle
};

/** \brief Characteristics Rx to Add to the xcp Service */
static BLE_CharInfo_t BLE_V_CharRxInfo = {
        &BOOTBLE_ServHandle,
        {0xe6,0xa9,0xf4,0xb4,0x3e,0x00,0x0f,0x9a,0xdc,0x4e,0x42,0x24,0xd1,0x74,0x3d,0x36},
        BLE_UUID_TYPE_128,
        BOOTBLE_MAX_ATT_MTU_SIZE,
        (BLE_CHAR_PROP_READ | BLE_CHAR_PROP_WRITE_WITHOUT_RESP),
        BLE_ATTR_PERMISSION_NONE,
        BLE_GATT_NOTIFY_ATTRIBUTE_WRITE,
        BLE_ENC_SIZE_10,
        BLE_FIXED_LEN,
        &BOOTBLE_RxCharHandle
};

/** \brief Characteristics Tx to Add to the xcp Service */
static BLE_CharInfo_t BLE_V_CharTxInfo = {
        &BOOTBLE_ServHandle,
        {0xe6,0xa9,0xf4,0xb4,0x3e,0x00,0x0f,0x9a,0xdc,0x4e,0x42,0x24,0xd2,0x74,0x3d,0x36},
        BLE_UUID_TYPE_128,
        DEFAULT_ATT_MTU,
        (BLE_CHAR_PROP_READ | BLE_CHAR_PROP_NOTIFY),
        BLE_ATTR_PERMISSION_NONE,
        BLE_GATT_DONT_NOTIFY_EVENTS,
        BLE_ENC_SIZE_10,
        BLE_FIXED_LEN,
        &BOOTBLE_TxCharHandle
};

/** Connection supervision Timeout */
static uint16_t BOOTBLE_ConnSupervisionTimeout;

/** Connection handle */
static uint16_t BOOTBLE_ConnHandle = 0u;

/** Reception Buffer (for Command_data Characteristics) */
static uint8_t BOOTBLE_ReceiveBuffer[BOOTBLE_MAX_ATT_MTU_SIZE];

/** Flag for Reception notification */
static BOOL BOOTBLE_bIsNewReception = FALSE;

/** List of Callback to registered */
static BLE_CallBackInfo_t BOOTBLE_CallBackInfo =
{
        .BLE_hci_disconnection_complete_event_cb = &BOOTBLE_DisconnectEvent,
        .BLE_hci_encryption_change_event_cb = NULL,
        .BLE_hci_read_remote_version_information_complete_event_cb  = NULL,
        .BLE_hci_hardware_error_event_cb = NULL,
        .BLE_hci_number_of_completed_packets_event_cb = NULL,
        .BLE_hci_data_buffer_overflow_event_cb = NULL,
        .BLE_hci_encryption_key_refresh_complete_event_cb = NULL,
        .BLE_hci_le_connection_complete_event_cb = &BOOTBLE_ConnectCompleteEvent,
        .BLE_hci_le_advertising_report_event_cb = NULL,
        .BLE_hci_le_connection_update_complete_event_cb = NULL,
        .BLE_hci_le_read_remote_used_features_complete_event_cb = NULL,
        .BLE_hci_le_long_term_key_request_event_cb = NULL,
        .BLE_hci_le_data_length_change_event_cb = NULL,
        .BLE_hci_le_read_local_p256_public_key_complete_event_cb = NULL,
        .BLE_hci_le_generate_dhkey_complete_event_cb = NULL,
        .BLE_hci_le_enhanced_connection_complete_event_cb = NULL,
        .BLE_hci_le_direct_advertising_report_event_cb = NULL,
        .BLE_aci_gap_limited_discoverable_event_cb = NULL,
        .BLE_aci_gap_pairing_complete_event_cb = NULL,
        .BLE_aci_gap_pass_key_req_event_cb = NULL,
        .BLE_aci_gap_authorization_req_event_cb = NULL,
        .BLE_aci_gap_slave_security_initiated_event_cb = NULL,
        .BLE_aci_gap_bond_lost_event_cb = NULL,
        .BLE_aci_gap_proc_complete_event_cb = NULL,
        .BLE_aci_gap_addr_not_resolved_event_cb = NULL,
        .BLE_aci_gap_numeric_comparison_value_event_cb = NULL,
        .BLE_aci_gap_keypress_notification_event_cb = NULL,
        .BLE_aci_gatt_attribute_modified_event_cb = &BOOTBLE_AttributeModifiedEvent,
        .BLE_aci_gatt_proc_timeout_event_cb = NULL,
        .BLE_aci_att_exchange_mtu_resp_event_cb = NULL,
        .BLE_aci_att_find_info_resp_event_cb = NULL,
        .BLE_aci_att_find_by_type_value_resp_event_cb = NULL,
        .BLE_aci_att_read_by_type_resp_event_cb = NULL,
        .BLE_aci_att_read_resp_event_cb = NULL,
        .BLE_aci_att_read_blob_resp_event_cb = NULL,
        .BLE_aci_att_read_multiple_resp_event_cb = NULL,
        .BLE_aci_att_read_by_group_type_resp_event_cb = NULL,
        .BLE_aci_att_prepare_write_resp_event_cb = NULL,
        .BLE_aci_att_exec_write_resp_event_cb = NULL,
        .BLE_aci_gatt_indication_event_cb = NULL,
        .BLE_aci_gatt_notification_event_cb = NULL,
        .BLE_aci_gatt_proc_complete_event_cb = NULL,
        .BLE_aci_gatt_error_resp_event_cb = NULL,
        .BLE_aci_gatt_disc_read_char_by_uuid_resp_event_cb = NULL,
        .BLE_aci_gatt_write_permit_req_event_cb = NULL,
        .BLE_aci_gatt_read_permit_req_event_cb = NULL,
        .BLE_aci_gatt_read_multi_permit_req_event_cb = NULL,
        .BLE_aci_gatt_tx_pool_available_event_cb = NULL,
        .BLE_aci_gatt_server_confirmation_event_cb = NULL,
        .BLE_aci_gatt_prepare_write_permit_req_event_cb = NULL,
        .BLE_aci_l2cap_connection_update_resp_event_cb = NULL,
        .BLE_aci_l2cap_proc_timeout_event_cb = NULL,
        .BLE_aci_l2cap_connection_update_req_event_cb = NULL,
        .BLE_aci_l2cap_command_reject_event_cb = NULL,
        .BLE_aci_hal_end_of_radio_activity_event_cb = NULL,
        .BLE_aci_hal_scan_req_report_event_cb = NULL,
        .BLE_aci_hal_fw_error_event_cb = NULL,
        .BLE_HAL_VTimerTimeoutCallback_cb = NULL
};

/****************************************************************************************
 * Static Function Definition
 ****************************************************************************************/
static void BOOTBLE_ConnectCompleteEvent(uint8_t Status,
        uint16_t Connection_Handle,
        uint8_t Role,
        uint8_t Peer_Address_Type,
        uint8_t Peer_Address[6],
        uint16_t Conn_Interval,
        uint16_t Conn_Latency,
        uint16_t Supervision_Timeout,
        uint8_t Master_Clock_Accuracy)
{
    BOOTBLE_ConnSupervisionTimeout = Supervision_Timeout;

    /* connection event. Keep the module in the bootloader state */
    XcpConnect(TRUE);

    /* keep the connection handle, we will need it for the answer */
    BOOTBLE_ConnHandle = Connection_Handle;

    /* exchange the MTU (not the defaut MTU set to 23Bytes) */
    BLE_aci_gatt_exchange_config(BOOTBLE_ConnHandle);
}

static void BOOTBLE_AttributeModifiedEvent(uint16_t Connection_Handle,
        uint16_t Attr_Handle,
        uint16_t Offset,
        uint16_t Attr_Data_Length,
        uint8_t Attr_Data[])
{
    /* Modification of characteristics value. Fist we check if this is a change in the command_data characteristics */
    if((BOOTBLE_RxCharHandle + 1u) == Attr_Handle)
    {
        /* Check the size */
        if(sizeof(BOOTBLE_ReceiveBuffer) == Attr_Data_Length)
        {
            /* Copy the Received buffer  and notify a new reception */
            memcpy(BOOTBLE_ReceiveBuffer, Attr_Data, Attr_Data_Length);

            /* Notifiy that there is e new buffer available */
            BOOTBLE_bIsNewReception = TRUE;
        }
    }
}

static void BOOTBLE_DisconnectEvent(uint8_t Status,
        uint16_t Connection_Handle,
        uint8_t Reason)
{
    uint8_t bleNameLen;
    /* disconnect event. Notify that the device shall be discoverable again */
    bleNameLen = sizeof(BOOTBLE_DeviceName);
    (void)BLE_SetConnectable(BOOTBLE_serviceUUID4Scan, sizeof(BOOTBLE_serviceUUID4Scan), BOOTBLE_DeviceName, &bleNameLen, 0u, 0u, 0u, 0u);
}

/****************************************************************************************
 * Public Function Definitions
 ****************************************************************************************/
tBleStatus BOOTBLE_Init(void)
{
    tBleStatus lReturn = BLE_STATUS_SUCCESS;
    uint8_t BleAddr[6u];
    BLE_ServiceInfo_t * serviceInfo = &BLE_V_ServiceInfo;
    BLE_CharInfo_t * charInfo[2] = {&BLE_V_CharRxInfo, &BLE_V_CharTxInfo};
    uint8_t bleNameLen;

    /* Read unique device serial number. It will be used for bleName */
    memcpy((void *)BleAddr, (void *)BOOTBLE_DEVICE_ADDR, sizeof(BleAddr));

    /* initialize the BLE Stack interface */
    lReturn = BLE_Init(&BOOTBLE_BlueNRGStackInitParams, BleAddr, sizeof(BleAddr), &BOOTBLE_CallBackInfo);

    if(BLE_STATUS_SUCCESS == lReturn)
    {
        /* Add the App service to the GATT */
        lReturn = BLE_AddService(&serviceInfo, sizeof(serviceInfo)/sizeof(BLE_ServiceInfo_t *));

        if(BLE_STATUS_SUCCESS == lReturn)
        {
            /* Add the App Characteristic the App service */
            lReturn = BLE_AddChar(charInfo, sizeof(charInfo)/sizeof(BLE_CharInfo_t *));
            if(BLE_STATUS_SUCCESS == lReturn)
            {
                /* Make the device scannable and connectable */
                bleNameLen = sizeof(BOOTBLE_DeviceName);
                lReturn = BLE_SetConnectable(BOOTBLE_serviceUUID4Scan, sizeof(BOOTBLE_serviceUUID4Scan), BOOTBLE_DeviceName, &bleNameLen,
                        0x0u, 0x0u, 0x0u, 0x0u);
            }
        }
    }

    return lReturn;
} /*** end of BOOTBLE_Init ***/

BOOL BOOTBLE_ReceivePacket(uint8_t *data, uint8_t *len)
{
    BOOL lbIsNewPacket = FALSE;

    /* First Check if characteristics value of xcpCommandDataCharHandle has changed */
    if(TRUE == BOOTBLE_bIsNewReception)
    {
        /* Once the packet is given, reset the flag */
        BOOTBLE_bIsNewReception = FALSE;

        /* The size is always store in the fist byte */
        *len = BOOTBLE_ReceiveBuffer[0u];

        /* Get the data. Start at the second byte (first one is the size) */
        memcpy(data, &BOOTBLE_ReceiveBuffer[1u], *len);

        /* New Packet Available */
        lbIsNewPacket = TRUE;
    }

    return lbIsNewPacket;
} /*** end of BLE_ReceivePacket ***/

void BOOTBLE_TransmitPacket(uint8_t *data, uint8_t len)
{
    uint8_t txbuffer[DEFAULT_ATT_MTU];
    /* Check if the size is not greater than the authorized one */
    if(len <= DEFAULT_ATT_MTU - 1)
    {
        /* Size OK, add the byte for len before the payload */
        txbuffer[0u] = len;

        /* Copy the rest of data after */
        memcpy(&txbuffer[1u], data, len);
        /* Size OK, copy data to the characteristics value, and notify client */
        (void)BLE_aci_gatt_update_char_value_ext(BOOTBLE_ConnHandle, BOOTBLE_ServHandle, BOOTBLE_TxCharHandle, 1u /* NOTIFICATION */,
                DEFAULT_ATT_MTU, 0u, sizeof(txbuffer), txbuffer);
    }
} /*** end of BOOTBLE_TransmitPacket ***/

void BOOTBLE_SetMinConnectionIntervall(void)
{
    static BOOL bIsChangedNeeded = TRUE;

    /* Only changed once */
    if(TRUE == bIsChangedNeeded)
    {
        BLE_aci_l2cap_connection_parameter_update_req(BOOTBLE_ConnHandle, BOOTBLE_MIN_CONN_INTERVAL,
                BOOTBLE_MIN_CONN_INTERVAL, 0, BOOTBLE_ConnSupervisionTimeout);

        bIsChangedNeeded = FALSE;
    }
} /*** end of BLE_SetMinConnectionIntervall ***/


uint16_t BOOTBLE_GetConnectionHandle(void)
{
    return BOOTBLE_ConnHandle;
} /*** end of BOOTBLE_GetConnectionHandle ***/

/*********************************** end of boot_ble.c *************************************/
