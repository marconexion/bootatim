/*
    / \     _   |_|
   / _ \  _| |_  _  _____
  / /_\ \(_   _)| ||     |
 / _____ \ | |  | || | | |
/_/     \_\\__) |_||_|_|_|
    (C)2018 Atim

Description: Application

License:

Maintainer: Youen Lebret

 */

/****************************************************************************************
 * Include files
 ****************************************************************************************/
#include "blt.h"
#include "flash.h"
#include "BlueNRG1_flash.h"
#include "aes256.h"
#include "xcp.h"
#include <string.h>


/****************************************************************************************
 * Macro definitions
 ****************************************************************************************/
/** \brief Value for an invalid flash sector. */
#define FLASH_C_INVALID_SECTOR            (0xff)
/** \brief Value for an invalid flash address. */
#define FLASH_C_INVALID_ADDRESS           (0xffffffff)
/** \brief Standard size of a flash block for writing. */
#define FLASH_C_WRITE_BLOCK_SIZE          (0x800)
/** \brief Total numbers of sectors in array FLASH_V_Layout[]. */
#define FLASH_C_TOTAL_SECTORS             (sizeof(FLASH_V_Layout)/sizeof(FLASH_V_Layout[0]))
/** \brief End address of the bootloader programmable flash. */
#define FLASH_C_END_ADDRESS               (FLASH_V_Layout[FLASH_C_TOTAL_SECTORS-1].sector_start + \
        FLASH_V_Layout[FLASH_C_TOTAL_SECTORS-1].sector_size - 1)

/** \brief Number of bytes to erase per erase operation. */
#define FLASH_C_ERASE_BLOCK_SIZE          (0x800)

/** \brief Offset into the user program's vector table where the checksum is located.
 *         For this target it is set to the end of the vector table. Note that the
 *         value can be overriden in blt_conf.h, because the size of the vector table
 *         could vary. When changing this value, don't forget to update the location
 *         of the checksum in the user program accordingly. Otherwise the checksum
 *         verification will always fail.
 */
#ifndef FLASH_C_VECTOR_TABLE_CS_OFFSET
#define FLASH_C_VECTOR_TABLE_CS_OFFSET    (0xC0)
#endif


/****************************************************************************************
 * Plausibility checks
 ****************************************************************************************/
#if (FLASH_C_VECTOR_TABLE_CS_OFFSET >= FLASH_C_WRITE_BLOCK_SIZE)
#error "FLASH_C_VECTOR_TABLE_CS_OFFSET is set too high. It must be located in the first writable block."
#endif



/****************************************************************************************
 * Type definitions
 ****************************************************************************************/
/** \brief Flash sector descriptor type. */
typedef struct
{
    blt_addr   sector_start;                       /**< sector start address             */
    blt_int32u sector_size;                        /**< sector size in bytes             */
    blt_int8u  sector_num;                         /**< sector number                    */
} FLASH_T_FlashSector;

/** \brief    Structure type for grouping flash block information.
 *  \details  Programming is done per block of max FLASH_C_WRITE_BLOCK_SIZE. for this a
 *            flash block manager is implemented in this driver. this flash block manager
 *            depends on this flash block info structure. It holds the base address of
 *            the flash block and the data that should be programmed into the flash
 *            block. The .base_addr must be a multiple of FLASH_C_WRITE_BLOCK_SIZE.
 */
typedef struct
{
    blt_addr  base_addr;
    blt_int8u data[FLASH_C_WRITE_BLOCK_SIZE];
} FLASH_T_BlockInfo;


/****************************************************************************************
 * Function prototypes
 ****************************************************************************************/

static blt_bool  FLASH_F_InitBlock(FLASH_T_BlockInfo *block, blt_addr address);
static FLASH_T_BlockInfo *FLASH_F_SwitchBlock(FLASH_T_BlockInfo *block, blt_addr base_addr);
static blt_bool  FLASH_F_AddToBlock(FLASH_T_BlockInfo *block, blt_addr address,
        blt_int8u *data, blt_int32u len);
static blt_bool  FLASH_F_WriteBlock(FLASH_T_BlockInfo *block);
static blt_bool  FLASH_F_EraseSectors(blt_int8u first_sector, blt_int8u last_sector);
static blt_int8u FLASH_F_GetSector(blt_addr address);
static blt_addr  FLASH_F_GetSectorBaseAddr(blt_int8u sector);
static blt_addr  FLASH_F_GetSectorSize(blt_int8u sector);
static void      FLASH_F_DecryptBlock(FLASH_T_BlockInfo *block);


/****************************************************************************************
 * Local constant declarations
 ****************************************************************************************/
static blt_int8u const FLASH_V_key[32] = {0xE1u, 0x2Au, 0x35u, 0xCAu, 0xDAu, 0xE5u, 0xBAu, 0xEAu,
        0x2Cu, 0x38u, 0x74u, 0x9Bu, 0x58u, 0x12u, 0xE9u, 0xF8u,
        0xCCu, 0xB5u, 0x16u, 0x1Au, 0x20u, 0x0Fu, 0x6Eu, 0x4Cu,
        0x90u, 0x57u, 0x24u, 0xB2u, 0xC7u, 0xF0u, 0xD7u, 0xAEu};

/** \brief   Defnition of Flash Mapping between bootloader and user Application */
static const FLASH_T_FlashSector FLASH_V_Layout[] =
{
        /* In Case of bootloader size change, this array must be adapted */

        /* { 0x10040000, 0x800,  0},           flash sector  0 - reserved for bootloader   */
        /* { 0x10040800, 0x800,  1},           flash sector  1 - reserved for bootloader   */
        /* { 0x10041000, 0x800,  2},           flash sector  2 - reserved for bootloader   */
        /* { 0x10041800, 0x800,  3},           flash sector  3 - reserved for bootloader   */
        /* { 0x10042000, 0x800,  4},           flash sector  4 - reserved for bootloader   */
        /* { 0x10042800, 0x800,  5},           flash sector  5 - reserved for bootloader   */
        /* { 0x10043000, 0x800,  6},           flash sector  6 - reserved for bootloader   */
        /* { 0x10043800, 0x800,  7},           flash sector  7 - reserved for bootloader   */
        /* { 0x10044000, 0x800,  8},           flash sector  8 - reserved for bootloader   */
        /* { 0x10044800, 0x800,  9},           flash sector  9 - reserved for bootloader   */
        /* { 0x10045000, 0x800,  10},          flash sector  10 - reserved for bootloader   */
        /* { 0x10045800, 0x800,  11},          flash sector  11 - reserved for bootloader   */
        /* { 0x10046000, 0x800,  12},          flash sector  12 - reserved for bootloader   */
        /* { 0x10046800, 0x800,  13},          flash sector  13 - reserved for bootloader   */
        /* { 0x10047000, 0x800,  14},          flash sector  14 - reserved for bootloader   */
        /* { 0x10047800, 0x800,  15},          flash sector  15 - reserved for bootloader   */
        /* { 0x10048000, 0x800,  16},          flash sector  16 - reserved for bootloader   */
        /* { 0x10048800, 0x800,  17},          flash sector  17 - reserved for bootloader   */
        /* { 0x10049000, 0x800,  18},          flash sector  18 - reserved for bootloader   */
        /* { 0x10049800, 0x800,  19},          flash sector  19 - reserved for bootloader   */
        /* { 0x1004a000, 0x800,  20},          flash sector  20 - reserved for bootloader   */
        /* { 0x1004a800, 0x800,  21},          flash sector  21 - reserved for bootloader   */
        /* { 0x1004b000, 0x800,  22},          flash sector  22 - reserved for bootloader   */
        /* { 0x1004b800, 0x800,  23},          flash sector  23 - reserved for bootloader   */
        /* { 0x1004c000, 0x800,  24},          flash sector  24 - reserved for bootloader   */
        /* { 0x1004c800, 0x800,  25},          flash sector  25 - reserved for bootloader   */
        /* { 0x1004d000, 0x800,  26},          flash sector  26 - reserved for bootloader   */
        /* { 0x1004d800, 0x800,  27},          flash sector  27 - reserved for bootloader   */
        /* { 0x1004e000, 0x800,  28},          flash sector  28 - reserved for bootloader   */
        /* { 0x1004e800, 0x800,  29},          flash sector  29 - reserved for bootloader   */
        /* { 0x1004f000, 0x800,  30},          flash sector  30 - reserved for bootloader   */
        /* { 0x1004f800, 0x800,  31},          flash sector  31 - reserved for bootloader   */
        /* { 0x10050000, 0x800,  32},          flash sector  32 - reserved for bootloader   */
        /* { 0x10050800, 0x800,  33},          flash sector  33 - reserved for bootloader   */
        /* { 0x10051000, 0x800,  34},          flash sector  34 - reserved for bootloader   */
        /* { 0x10051800, 0x800,  35},          flash sector  35 - reserved for bootloader   */
        /* { 0x10052000, 0x800,  36},          flash sector  36 - reserved for bootloader   */
        /* { 0x10052800, 0x800,  37},          flash sector  37 - reserved for bootloader   */
        /* { 0x10053000, 0x800,  38},          flash sector  38 - reserved for bootloader   */
        /* { 0x10053800, 0x800,  39},          flash sector  39 - reserved for bootloader   */
        /* { 0x10054000, 0x800,  40},          flash sector  40 - reserved for bootloader   */
        /* { 0x10054800, 0x800,  41},          flash sector  41 - reserved for bootloader   */
        /* { 0x10055000, 0x800,  42},          flash sector  42 - reserved for bootloader   */
        /* { 0x10055800, 0x800,  43},          flash sector  43 - reserved for bootloader   */
        /* { 0x10056000, 0x800,  44},          flash sector  44 - reserved for bootloader   */
        /* { 0x10056800, 0x800,  45},          flash sector  45 - reserved for bootloader   */
        /* { 0x10057000, 0x800,  46},          flash sector  46 - reserved for bootloader   */
        /* { 0x10057800, 0x800,  47},          flash sector  47 - reserved for bootloader   */
        /* { 0x10058000, 0x800,  48},          flash sector  48 - reserved for bootloader   */
        /* { 0x10058800, 0x800,  49},          flash sector  49 - reserved for bootloader   */
        { 0x10059000, 0x800,  50},           /* flash sector  50 - 2048b                       */
        { 0x10059800, 0x800,  51},           /* flash sector  51 - 2048b                       */
        { 0x1005a000, 0x800,  52},           /* flash sector  52 - 2048b                       */
        { 0x1005a800, 0x800,  53},           /* flash sector  53 - 2048b                       */
        { 0x1005b000, 0x800,  54},           /* flash sector  54 - 2048b                       */
        { 0x1005b800, 0x800,  55},           /* flash sector  55 - 2048b                       */
        { 0x1005c000, 0x800,  56},           /* flash sector  56 - 2048b                       */
        { 0x1005c800, 0x800,  57},           /* flash sector  57 - 2048b                       */
        { 0x1005d000, 0x800,  58},           /* flash sector  58 - 2048b                       */
        { 0x1005d800, 0x800,  59},           /* flash sector  59 - 2048b                       */
        { 0x1005e000, 0x800,  60},           /* flash sector  60 - 2048b                       */
        { 0x1005e800, 0x800,  61},           /* flash sector  61 - 2048b                       */
        { 0x1005f000, 0x800,  62},           /* flash sector  62 - 2048b                       */
        { 0x1005f800, 0x800,  63},           /* flash sector  63 - 2048b                       */
        { 0x10060000, 0x800,  64},           /* flash sector  64 - 2048b                       */
        { 0x10060800, 0x800,  65},           /* flash sector  65 - 2048b                       */
        { 0x10061000, 0x800,  66},           /* flash sector  66 - 2048b                       */
        { 0x10061800, 0x800,  67},           /* flash sector  67 - 2048b                       */
        { 0x10062000, 0x800,  68},           /* flash sector  68 - 2048b                       */
        { 0x10062800, 0x800,  69},           /* flash sector  69 - 2048b                       */
        { 0x10063000, 0x800,  70},           /* flash sector  70 - 2048b                       */
        { 0x10063800, 0x800,  71},           /* flash sector  71 - 2048b                       */
        { 0x10064000, 0x800,  72},           /* flash sector  72 - 2048b                       */
        { 0x10064800, 0x800,  73},           /* flash sector  73 - 2048b                       */
        { 0x10065000, 0x800,  74},           /* flash sector  74 - 2048b                       */
        { 0x10065800, 0x800,  75},           /* flash sector  75 - 2048b                       */
        { 0x10066000, 0x800,  76},           /* flash sector  76 - 2048b                       */
        { 0x10066800, 0x800,  77},           /* flash sector  77 - 2048b                       */
        { 0x10067000, 0x800,  78},           /* flash sector  78 - 2048b                       */
        { 0x10067800, 0x800,  79},           /* flash sector  79 - 2048b                       */
        { 0x10068000, 0x800,  80},           /* flash sector  80 - 2048b                       */
        { 0x10068800, 0x800,  81},           /* flash sector  81 - 2048b                       */
        { 0x10069000, 0x800,  82},           /* flash sector  82 - 2048b                       */
        { 0x10069800, 0x800,  83},           /* flash sector  83 - 2048b                       */
        { 0x1006a000, 0x800,  84},           /* flash sector  84 - 2048b                       */
        { 0x1006a800, 0x800,  85},           /* flash sector  85 - 2048b                       */
        { 0x1006b000, 0x800,  86},           /* flash sector  86 - 2048b                       */
        { 0x1006b800, 0x800,  87},           /* flash sector  87 - 2048b                       */
        { 0x1006c000, 0x800,  88},           /* flash sector  88 - 2048b                       */
        { 0x1006c800, 0x800,  89},           /* flash sector  89 - 2048b                       */
        { 0x1006d000, 0x800,  90},           /* flash sector  90 - 2048b                       */
        { 0x1006d800, 0x800,  91},           /* flash sector  91 - 2048b                       */
        { 0x1006e000, 0x800,  92},           /* flash sector  92 - 2048b                       */
        { 0x1006e800, 0x800,  93},           /* flash sector  93 - 2048b                       */
        { 0x1006f000, 0x800,  94},           /* flash sector  94 - 2048b                       */
        { 0x1006f800, 0x800,  95},           /* flash sector  95 - 2048b                       */
        { 0x10070000, 0x800,  96},           /* flash sector  96 - 2048b                       */
        { 0x10070800, 0x800,  97},           /* flash sector  97 - 2048b                       */
        { 0x10071000, 0x800,  98},           /* flash sector  98 - 2048b                       */
        { 0x10071800, 0x800,  99},           /* flash sector  99 - 2048b                       */
        { 0x10072000, 0x800,  100},          /* flash sector  100 - 2048b                      */
        { 0x10072800, 0x800,  101},          /* flash sector  101 - 2048b                      */
        { 0x10073000, 0x800,  102},          /* flash sector  102 - 2048b                      */
        { 0x10073800, 0x800,  103},          /* flash sector  103 - 2048b                      */
        { 0x10074000, 0x800,  104},          /* flash sector  104 - 2048b                      */
        { 0x10074800, 0x800,  105},          /* flash sector  105 - 2048b                      */
        { 0x10075000, 0x800,  106},          /* flash sector  106 - 2048b                      */
        { 0x10075800, 0x800,  107},          /* flash sector  107 - 2048b                      */
        { 0x10076000, 0x800,  108},          /* flash sector  108 - 2048b                      */
        { 0x10076800, 0x800,  109},          /* flash sector  109 - 2048b                      */
        { 0x10077000, 0x800,  110},          /* flash sector  110 - 2048b                      */
        { 0x10077800, 0x800,  111},          /* flash sector  111 - 2048b                      */
        { 0x10078000, 0x800,  112},          /* flash sector  112 - 2048b                      */
        { 0x10078800, 0x800,  113},          /* flash sector  113 - 2048b                      */
        { 0x10079000, 0x800,  114},          /* flash sector  114 - 2048b                      */
        { 0x10079800, 0x800,  115},          /* flash sector  115 - 2048b                      */
        { 0x1007a000, 0x800,  116},          /* flash sector  116 - 2048b                      */
        { 0x1007a800, 0x800,  117},          /* flash sector  117 - 2048b                      */
        { 0x1007b000, 0x800,  118},          /* flash sector  118 - 2048b                      */
        { 0x1007b800, 0x800,  119},          /* flash sector  119 - 2048b                      */
        { 0x1007c000, 0x800,  120},          /* flash sector  120 - 2048b                      */
        { 0x1007c800, 0x800,  121},          /* flash sector  121 - 2048b                      */
        { 0x1007d000, 0x800,  122},          /* flash sector  122 - 2048b                      */
        { 0x1007d800, 0x800,  123},          /* flash sector  123 - 2048b                      */
        { 0x1007e000, 0x800,  124},          /* flash sector  124 - 2048b                      */
        { 0x1007e800, 0x800,  125},          /* flash sector  125 - 2048b                      */
        { 0x1007f000, 0x800,  126},          /* flash sector  126 - 2048b                      */
        { 0x1007f800, 0x800,  127},          /* flash sector  127 - 2048b                      */
};

/****************************************************************************************
 * Local data declarations
 ****************************************************************************************/
/** \brief   Local variable with information about the flash block that is currently
 *           being operated on.
 *  \details The smallest amount of flash that can be programmed is
 *           FLASH_C_WRITE_BLOCK_SIZE. A flash block manager is implemented in this driver
 *           and stores info in this variable. Whenever new data should be flashed, it
 *           is first added to a RAM buffer, which is part of this variable. Whenever
 *           the RAM buffer, which has the size of a flash block, is full or  data needs
 *           to be written to a different block, the contents of the RAM buffer are
 *           programmed to flash. The flash block manager requires some software
 *           overhead, yet results is faster flash programming because data is first
 *           harvested, ideally until there is enough to program an entire flash block,
 *           before the flash device is actually operated on.
 */
static FLASH_T_BlockInfo blockInfo;

/** \brief   Local variable with information about the flash boot block.
 *  \details The first block of the user program holds the vector table, which on the
 *           STM32 is also the where the checksum is written to. Is it likely that
 *           the vector table is first flashed and then, at the end of the programming
 *           sequence, the checksum. This means that this flash block need to be written
 *           to twice. Normally this is not a problem with flash memory, as long as you
 *           write the same values to those bytes that are not supposed to be changed
 *           and the locations where you do write to are still in the erased 0xFF state.
 *           Unfortunately, writing twice to flash this way, does not work reliably on
 *           all micros. This is why we need to have an extra block, the bootblock,
 *           placed under the management of the block manager. This way is it possible
 *           to implement functionality so that the bootblock is only written to once
 *           at the end of the programming sequence.
 */
static FLASH_T_BlockInfo bootBlockInfo;


/************************************************************************************//**
 ** \brief     Initializes the flash driver.
 ** \return    none.
 **
 ****************************************************************************************/
void FLASH_Init(void)
{
    /* init the flash block info structs by setting the address to an invalid address */
    blockInfo.base_addr = FLASH_C_INVALID_ADDRESS;
    bootBlockInfo.base_addr = FLASH_C_INVALID_ADDRESS;
} /*** end of FLASH_Init ***/


/************************************************************************************//**
 ** \brief     Writes the data to flash through a flash block manager. Note that this
 **            function also checks that no data is programmed outside the flash
 **            memory region, so the bootloader can never be overwritten.
 ** \param     addr Start address.
 ** \param     len  Length in bytes.
 ** \param     data Pointer to the data buffer.
 ** \return    BLT_TRUE if successful, BLT_FALSE otherwise.
 **
 ****************************************************************************************/
blt_bool FLASH_Write(blt_addr addr, blt_int32u len, blt_int8u *data)
{
    blt_addr base_addr;

    /* validate the len parameter */
    if ((len - 1) > (FLASH_C_END_ADDRESS - addr))
    {
        return BLT_FALSE;
    }

    /* make sure the addresses are within the flash device */
    if ((FLASH_F_GetSector(addr) == FLASH_C_INVALID_SECTOR) || \
            (FLASH_F_GetSector(addr+len-1) == FLASH_C_INVALID_SECTOR))
    {
        return BLT_FALSE;
    }

    /* if this is the bootblock, then let the boot block manager handle it */
    base_addr = (addr/FLASH_C_WRITE_BLOCK_SIZE)*FLASH_C_WRITE_BLOCK_SIZE;
    if (base_addr == FLASH_V_Layout[0].sector_start)
    {
        /* let the boot block manager handle it */
        return FLASH_F_AddToBlock(&bootBlockInfo, addr, data, len);
    }
    /* let the block manager handle it */
    return FLASH_F_AddToBlock(&blockInfo, addr, data, len);
} /*** end of FLASH_Write ***/


/************************************************************************************//**
 ** \brief     Erases the flash memory. Note that this function also checks that no
 **            data is erased outside the flash memory region, so the bootloader can
 **            never be erased.
 ** \param     addr Start address.
 ** \param     len  Length in bytes.
 ** \return    BLT_TRUE if successful, BLT_FALSE otherwise.
 **
 ****************************************************************************************/
blt_bool FLASH_Erase(blt_addr addr, blt_int32u len)
{
    blt_int8u first_sector;
    blt_int8u last_sector;

    /* validate the len parameter */
    if ((len - 1) > (FLASH_C_END_ADDRESS - addr))
    {
        return BLT_FALSE;
    }

    /* obtain the first and last sector number */
    first_sector = FLASH_F_GetSector(addr);
    last_sector  = FLASH_F_GetSector(addr+len-1);
    /* check them */
    if ((first_sector == FLASH_C_INVALID_SECTOR) || (last_sector == FLASH_C_INVALID_SECTOR))
    {
        return BLT_FALSE;
    }
    /* erase the sectors */
    return FLASH_F_EraseSectors(first_sector, last_sector);
} /*** end of FLASH_Erase ***/


/************************************************************************************//**
 ** \brief     Writes a checksum of the user program to non-volatile memory. This is
 **            performed once the entire user program has been programmed. Through
 **            the checksum, the bootloader can check if the programming session
 **            was completed, which indicates that a valid user programming is
 **            present and can be started.
 ** \return    BLT_TRUE if successful, BLT_FALSE otherwise.
 **
 ****************************************************************************************/
blt_bool FLASH_WriteChecksum(void)
{
    blt_int32u signature_checksum = 0;
    blt_int8u index = 0u;

    /* for the BlueNrg target we defined the checksum as the
     * sum of the intvec address
     *
     *    the bootloader writes this 32-bit checksum value right after the vector table
     *    of the user program. note that this means one extra dummy entry must be added
     *    at the end of the user program's vector table to reserve storage space for the
     *    checksum.
     */

    /* first check that the bootblock contains valid data. if not, this means the
     * bootblock is not part of the reprogramming this time and therefore no
     * new checksum needs to be written
     */
    if (bootBlockInfo.base_addr == FLASH_C_INVALID_ADDRESS)
    {
        return BLT_TRUE;
    }

    /* compute the checksum. note that the user program's vectors are not yet written
     * to flash but are present in the bootblock data structure at this point.
     */
    for(index = 0u; index < FLASH_C_VECTOR_TABLE_CS_OFFSET; index+=4)
    {
        signature_checksum += *((blt_int32u *)(&bootBlockInfo.data[index]));
    }

    /* write the checksum */
    return FLASH_Write(FLASH_V_Layout[0].sector_start+FLASH_C_VECTOR_TABLE_CS_OFFSET,
            sizeof(blt_addr), (blt_int8u *)&signature_checksum);
} /*** end of FLASH_WriteChecksum ***/


/************************************************************************************//**
 ** \brief     Verifies the checksum, which indicates that a valid user program is
 **            present and can be started.
 ** \return    BLT_TRUE if successful, BLT_FALSE otherwise.
 **
 ****************************************************************************************/
blt_bool FLASH_VerfiyChecksum(void)
{
    blt_int32u signature_checksum = 0;
    blt_int32u checksum = 0;
    blt_int8u index = 0u;

    for(index = 0u; index < FLASH_C_VECTOR_TABLE_CS_OFFSET; index+=4)
    {
        signature_checksum += *((blt_int32u *)(FLASH_V_Layout[0].sector_start + index));
    }

    checksum = *((blt_int32u *)(FLASH_V_Layout[0].sector_start + FLASH_C_VECTOR_TABLE_CS_OFFSET));

    /* sum should add up to an unsigned 32-bit value of 0 */
    if (signature_checksum == checksum)
    {
        /* checksum okay */
        return BLT_TRUE;
    }
    /* checksum incorrect */
    return BLT_FALSE;
} /*** end of FLASH_VerfiyChecksum ***/


/************************************************************************************//**
 ** \brief     Finalizes the flash driver operations. There could still be data in
 **            the currently active block that needs to be flashed.
 ** \return    BLT_TRUE if successful, BLT_FALSE otherwise.
 **
 ****************************************************************************************/
blt_bool FLASH_Done(void)
{
    /* check if there is still data waiting to be programmed in the boot block */
    if (bootBlockInfo.base_addr != FLASH_C_INVALID_ADDRESS)
    {
        /* Decrypt block if needed */
        FLASH_F_DecryptBlock(&bootBlockInfo);
        /* compute and write checksum, which is programmed by the internal driver. */
        if (FLASH_WriteChecksum() == BLT_FALSE)
        {
            return BLT_FALSE;
        }
        /* Write it to the Flash */
        if (FLASH_F_WriteBlock(&bootBlockInfo) == BLT_FALSE)
        {
            return BLT_FALSE;
        }
    }

    /* check if there is still data waiting to be programmed */
    if (blockInfo.base_addr != FLASH_C_INVALID_ADDRESS)
    {
        /* Decrypt block if needed */
        FLASH_F_DecryptBlock(&blockInfo);
        /* Write it to the Flash */
        if (FLASH_F_WriteBlock(&blockInfo) == BLT_FALSE)
        {
            return BLT_FALSE;
        }
    }
    /* still here so all is okay */
    return BLT_TRUE;
} /*** end of FLASH_Done ***/


/************************************************************************************//**
 ** \brief     Obtains the base address of the flash memory available to the user program.
 **            This is basically the first address in the FLASH_V_Layout table.
 ** \return    Base address.
 **
 ****************************************************************************************/
blt_addr FLASH_GetUserBaseAddr(void)
{
    return FLASH_V_Layout[0].sector_start;
} /*** end of FLASH_GetUserBaseAddr ***/


/************************************************************************************//**
 ** \brief     Copies data currently in flash to the block->data and sets the
 **            base address.
 ** \param     block   Pointer to flash block info structure to operate on.
 ** \param     address Base address of the block data.
 ** \return    BLT_TRUE if successful, BLT_FALSE otherwise.
 **
 ****************************************************************************************/
static blt_bool FLASH_F_InitBlock(FLASH_T_BlockInfo *block, blt_addr address)
{
    /* check address alignment */
    if ((address % FLASH_C_WRITE_BLOCK_SIZE) != 0)
    {
        return BLT_FALSE;
    }
    /* make sure that we are initializing a new block and not the same one */
    if (block->base_addr == address)
    {
        /* block already initialized, so nothing to do */
        return BLT_TRUE;
    }
    /* set the base address and copies the current data from flash */
    block->base_addr = address;
    memcpy((void *)block->data, (void *)address, FLASH_C_WRITE_BLOCK_SIZE);
    return BLT_TRUE;
} /*** end of FLASH_F_InitBlock ***/


/************************************************************************************//**
 ** \brief     Switches blocks by programming the current one and initializing the
 **            next.
 ** \param     block   Pointer to flash block info structure to operate on.
 ** \param     base_addr Base address of the next block.
 ** \return    The pointer of the block info struct that is no being used, or a NULL
 **            pointer in case of error.
 **
 ****************************************************************************************/
static FLASH_T_BlockInfo *FLASH_F_SwitchBlock(FLASH_T_BlockInfo *block, blt_addr base_addr)
{
    /* check if a switch needs to be made away from the boot block. in this case the boot
     * block shouldn't be written yet, because this is done at the end of the programming
     * session by FLASH_Done(), this is right after the checksum was written.
     */
    if (block == &bootBlockInfo)
    {
        /* switch from the boot block to the generic block info structure */
        block = &blockInfo;
    }
    /* check if a switch back into the bootblock is needed. in this case the generic block
     * doesn't need to be written here yet.
     */
    else if (base_addr == FLASH_V_Layout[0].sector_start)
    {
        /* switch from the generic block to the boot block info structure */
        block = &bootBlockInfo;
        base_addr = FLASH_V_Layout[0].sector_start;
    }
    else
    {
        /* Decrypt block if needed */
        FLASH_F_DecryptBlock(block);
        /* need to switch to a new block, so program the current one and init the next */
        if (FLASH_F_WriteBlock(block) == BLT_FALSE)
        {
            return BLT_NULL;
        }
    }

    /* initialize tne new block when necessary */
    if (FLASH_F_InitBlock(block, base_addr) == BLT_FALSE)
    {
        return BLT_NULL;
    }

    /* still here to all is okay  */
    return block;
} /*** end of FLASH_F_SwitchBlock ***/


/************************************************************************************//**
 ** \brief     Programming is done per block. This function adds data to the block
 **            that is currently collecting data to be written to flash. If the
 **            address is outside of the current block, the current block is written
 **            to flash an a new block is initialized.
 ** \param     block   Pointer to flash block info structure to operate on.
 ** \param     address Flash destination address.
 ** \param     data    Pointer to the byte array with data.
 ** \param     len     Number of bytes to add to the block.
 ** \return    BLT_TRUE if successful, BLT_FALSE otherwise.
 **
 ****************************************************************************************/
static blt_bool FLASH_F_AddToBlock(FLASH_T_BlockInfo *block, blt_addr address,
        blt_int8u *data, blt_int32u len)
{
    blt_addr   current_base_addr;
    blt_int8u  *dst;
    blt_int8u  *src;

    /* determine the current base address */
    current_base_addr = (address/FLASH_C_WRITE_BLOCK_SIZE)*FLASH_C_WRITE_BLOCK_SIZE;

    /* make sure the blockInfo is not uninitialized */
    if (block->base_addr == FLASH_C_INVALID_ADDRESS)
    {
        /* initialize the blockInfo struct for the current block */
        if (FLASH_F_InitBlock(block, current_base_addr) == BLT_FALSE)
        {
            return BLT_FALSE;
        }
    }

    /* check if the new data fits in the current block */
    if (block->base_addr != current_base_addr)
    {
        /* need to switch to a new block, so program the current one and init the next */
        block = FLASH_F_SwitchBlock(block, current_base_addr);
        if (block == BLT_NULL)
        {
            return BLT_FALSE;
        }
    }

    /* add the data to the current block, but check for block overflow */
    dst = &(block->data[address - block->base_addr]);
    src = data;
    do
    {
        /* buffer overflow? */
        if ((blt_addr)(dst-&(block->data[0])) >= FLASH_C_WRITE_BLOCK_SIZE)
        {
            /* need to switch to a new block, so program the current one and init the next */
            block = FLASH_F_SwitchBlock(block, current_base_addr+FLASH_C_WRITE_BLOCK_SIZE);
            if (block == BLT_NULL)
            {
                return BLT_FALSE;
            }
            /* reset destination pointer */
            dst = &(block->data[0]);
        }
        /* write the data to the buffer */
        *dst = *src;
        /* update pointers */
        dst++;
        src++;
        /* decrement byte counter */
        len--;
    }
    while (len > 0);
    /* still here so all is good */
    return BLT_TRUE;
} /*** end of FLASH_F_AddToBlock ***/


/************************************************************************************//**
 ** \brief     Programs FLASH_C_WRITE_BLOCK_SIZE bytes to flash from the block->data
 **            array.
 ** \param     block   Pointer to flash block info structure to operate on.
 ** \return    BLT_TRUE if successful, BLT_FALSE otherwise.
 **
 ****************************************************************************************/
static blt_bool FLASH_F_WriteBlock(FLASH_T_BlockInfo *block)
{
    blt_bool   result = BLT_TRUE;
    blt_int8u  sector_num;
    blt_addr   prog_addr;
    blt_int32u prog_data;
    blt_int32u word_cnt;

    /* check that address is actually within flash */
    sector_num = FLASH_F_GetSector(block->base_addr);
    if (sector_num == FLASH_C_INVALID_SECTOR)
    {
        return BLT_FALSE;
    }

    /* unlock the flash peripheral to enable the flash control register access. */
    FLASH_Unlock();
    /* program all words in the block one by one */
    for (word_cnt=0; word_cnt<(FLASH_C_WRITE_BLOCK_SIZE/sizeof(blt_int32u)); word_cnt++)
    {
        prog_addr = block->base_addr + (word_cnt * sizeof(blt_int32u));
        prog_data = *(volatile blt_int32u *)(&block->data[word_cnt * sizeof(blt_int32u)]);
        /* program the word */
        FLASH_ProgramWord(prog_addr, prog_data);

        /* verify that the written data is actually there */
        if (*(volatile blt_int32u *)prog_addr != prog_data)
        {
            result = BLT_FALSE;
            break;
        }
    }
    /* lock the flash peripheral to disable the flash control register access. */
    FLASH_Lock();
    /* give the result back to the caller */
    return result;
} /*** end of FLASH_F_WriteBlock ***/


/************************************************************************************//**
 ** \brief     Erases the flash sectors from first_sector up until last_sector.
 ** \param     first_sector First flash sector number.
 ** \param     last_sector  Last flash sector number.
 ** \return    BLT_TRUE if successful, BLT_FALSE otherwise.
 **
 ****************************************************************************************/
static blt_bool FLASH_F_EraseSectors(blt_int8u first_sector, blt_int8u last_sector)
{
    blt_bool   result = BLT_TRUE;
    blt_int16u nr_of_blocks;
    blt_int16u block_cnt;
    blt_addr   start_addr;
    blt_addr   end_addr;

    /* validate the sector numbers */
    if (first_sector > last_sector)
    {
        result = BLT_FALSE;
    }
    if ((first_sector < FLASH_V_Layout[0].sector_num) || \
            (last_sector > FLASH_V_Layout[FLASH_C_TOTAL_SECTORS-1].sector_num))
    {
        result = BLT_FALSE;
    }
    /* only continue if everything is okay so far */
    if (result == BLT_TRUE)
    {
        /* determine how many blocks need to be erased */
        start_addr = FLASH_F_GetSectorBaseAddr(first_sector);
        end_addr = FLASH_F_GetSectorBaseAddr(last_sector) + FLASH_F_GetSectorSize(last_sector) - 1;
        nr_of_blocks = (end_addr - start_addr + 1) / FLASH_C_ERASE_BLOCK_SIZE;

        /* unlock the flash array */
        FLASH_Unlock();
        /* erase all blocks one by one */
        for (block_cnt=0; block_cnt<nr_of_blocks; block_cnt++)
        {
            /* erase block */
            FLASH_ErasePage(first_sector + block_cnt);
        }
        /* lock the flash array again */
        FLASH_Lock();
    }
    /* give the result back to the caller */
    return result;
} /*** end of FLASH_F_EraseSectors ***/


/************************************************************************************//**
 ** \brief     Determines the flash sector the address is in.
 ** \param     address Address in the flash sector.
 ** \return    Flash sector number or FLASH_C_INVALID_SECTOR.
 **
 ****************************************************************************************/
static blt_int8u FLASH_F_GetSector(blt_addr address)
{
    blt_int8u sectorIdx;

    /* search through the sectors to find the right one */
    for (sectorIdx = 0; sectorIdx < FLASH_C_TOTAL_SECTORS; sectorIdx++)
    {
        /* is the address in this sector? */
        if ((address >= FLASH_V_Layout[sectorIdx].sector_start) && \
                (address < (FLASH_V_Layout[sectorIdx].sector_start + \
                        FLASH_V_Layout[sectorIdx].sector_size)))
        {
            /* return the sector number */
            return FLASH_V_Layout[sectorIdx].sector_num;
        }
    }
    /* still here so no valid sector found */
    return FLASH_C_INVALID_SECTOR;
} /*** end of FLASH_F_GetSector ***/


/************************************************************************************//**
 ** \brief     Determines the flash sector base address.
 ** \param     sector Sector to get the base address of.
 ** \return    Flash sector base address or FLASH_C_INVALID_ADDRESS.
 **
 ****************************************************************************************/
static blt_addr FLASH_F_GetSectorBaseAddr(blt_int8u sector)
{
    blt_int8u sectorIdx;

    /* search through the sectors to find the right one */
    for (sectorIdx = 0; sectorIdx < FLASH_C_TOTAL_SECTORS; sectorIdx++)
    {
        if (FLASH_V_Layout[sectorIdx].sector_num == sector)
        {
            return FLASH_V_Layout[sectorIdx].sector_start;
        }
    }
    /* still here so no valid sector found */
    return FLASH_C_INVALID_ADDRESS;
} /*** end of FLASH_F_GetSectorBaseAddr ***/


/************************************************************************************//**
 ** \brief     Determines the flash sector size.
 ** \param     sector Sector to get the size of.
 ** \return    Flash sector size or 0.
 **
 ****************************************************************************************/
static blt_addr FLASH_F_GetSectorSize(blt_int8u sector)
{
    blt_int8u sectorIdx;

    /* search through the sectors to find the right one */
    for (sectorIdx = 0; sectorIdx < FLASH_C_TOTAL_SECTORS; sectorIdx++)
    {
        if (FLASH_V_Layout[sectorIdx].sector_num == sector)
        {
            return FLASH_V_Layout[sectorIdx].sector_size;
        }
    }
    /* still here so no valid sector found */
    return 0;
} /*** end of FLASH_F_GetSectorSize ***/

/************************************************************************************//**
 ** \brief     Decrypt the encrypted block received
 ** \param     block Block to decrypt
 **
 ****************************************************************************************/
static void FLASH_F_DecryptBlock(FLASH_T_BlockInfo *block)
{
    blt_int32u index = 0;
    aes256_context ctx;

    if(TRUE == XcpIsEncrypted())
    {
        /* Init context */
        aes256_init(&ctx, FLASH_V_key);

        /* Decrypt in blocks of 16 bytes. */
        for (index = 0; index < (FLASH_C_WRITE_BLOCK_SIZE / 16u); index++)
        {
            aes256_decrypt_ecb(&ctx, &block->data[index * 16u]);
        }

        /* Cleanup context */
        aes256_done(&ctx);
    }
}/*** end of FLASH_F_DecryptBlock ***/
/*********************************** end of flash.c ************************************/
