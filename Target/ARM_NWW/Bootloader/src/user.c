/*
  / \     _   |_|
   / _ \  _| |_  _  _____
  / /_\ \(_   _)| ||   |
 / _____ \ | |  | || | | |
/_/     \_\\__) |_||_|_|_|
  (C)2018 Atim

Description: Application

License:

Maintainer: Youen Lebret

 */

/****************************************************************************************
 * Include files
 ****************************************************************************************/
#include "user.h"
#include "flash.h"
#include "blt.h"
#include "BlueNRG1_conf.h"
#include "ble.h"
#include "boot_ble.h"
#include <string.h>

/****************************************************************************************
 * Macro definitions
 ****************************************************************************************/

/****************************************************************************************
 * Define definitions
 ****************************************************************************************/
/** Pointer to the user application reset handler. */
#define USER_C_PROGRAM_STARTADDR_PTR    ((blt_addr)(FLASH_GetUserBaseAddr() + 0x00000004))
/** Pointer to the user application vector table. */
#define USER_C_PROGRAM_VECTABLE_OFFSET  ((blt_int32u)FLASH_GetUserBaseAddr())
/** Size in bytes of the user application vector table */
#define USER_C_PROGRAM_VECTABLE_SIZE    (0xC0u)
/** Start address of the user application RAM where its vector table will be copied to */
#define USER_C_PROGRAM_RAM_BASEADDR     ((blt_addr)(0x20000000))

/****************************************************************************************
 * Type definitions
 ****************************************************************************************/

/****************************************************************************************
 * Static/global definitions
 ****************************************************************************************/

/****************************************************************************************
 * Static Function prototypes
 ****************************************************************************************/

/****************************************************************************************
 * Static Function Definition
 ****************************************************************************************/

/****************************************************************************************
 * Public Function Definitions
 ****************************************************************************************/

/************************************************************************************//**
 ** \brief     Starts the user application, if one is present. In this case this function
 **            does not return.
 ** \return    none.
 **
 ****************************************************************************************/
void USER_Start(void)
{
    uint16_t lconnHandle = 0u;

    void (*pUserResetHandler)(void);

    /* Check if a valid user application if loaded in flash. If not, do not
     * try to go to, just stay in bootloader. */
#ifdef BLT_CONF_CHECKSUM_VERIFY_ENABLE
    if (FLASH_VerfiyChecksum() == BLT_FALSE)
    {
        /* Not a valid user application, stay in bootloader */
        return;
    }
#endif

    /* copy user application vectir table to RAM */
    memcpy((void *)USER_C_PROGRAM_RAM_BASEADDR, (void *)USER_C_PROGRAM_VECTABLE_OFFSET,
            USER_C_PROGRAM_VECTABLE_SIZE);

    /* reset the periph */
    SysCtrl_DeInit();
    UART_DeInit();
    GPIO_DeInit();
    /* the GPIO_DeInit fonction set the pull option on each GPIO.
     * Prefer High Z */
    GPIO->PE = 0x0;

    /* Stop Connnection */
    lconnHandle = BOOTBLE_GetConnectionHandle();
    BLE_aci_gap_terminate(lconnHandle, 0x93u);

    /* Stop advertising */
    BLE_aci_gap_set_non_discoverable();

    /* set the address where the bootloader needs to jump to. this is the address of
     * the 2nd entry in the user program's vector table. this address points to the
     * user program's reset handler.
     */
    pUserResetHandler = (void(*)(void))(*((blt_addr *)USER_C_PROGRAM_STARTADDR_PTR));

    /* start the user program by activating its reset interrupt service routine */
    pUserResetHandler();
} /*** end of USER_Start ***/


/*********************************** end of cpu.c **************************************/
