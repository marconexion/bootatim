/*
	  / \	    _   |_|
   / _ \  _| |_  _  _____
  / /_\ \(_   _)| ||	 |
 / _____ \ | |  | || | | |
/_/	    \_\\__) |_||_|_|_|
	(C)2018 Atim

Description: Application

License:

Maintainer: Youen Lebret

 */

/****************************************************************************************
 * Include files
 ****************************************************************************************/
#include "blt.h"
#include "BlueNRG1_uart.h"
#include "BlueNRG1_sysCtrl.h"
#include "BlueNRG1_gpio.h"
#include "uart.h"
#include <string.h>

/****************************************************************************************
 * Macro definitions
 ****************************************************************************************/
/** Timeout for one byte TX transmission. Based on baudrate min (1200) and
 * 10bit for 1byte transfert (8bits + 1stop bit + 1start bit). it shall never
 * be more than 20ms */
#define UART_BYTE_TX_TIMEOUT    (10u)

/** Nomber of Bytes min between two frame (needed to know the end of frame*/
#define UART_INTER_FRAME_BYTE   (3u)
/****************************************************************************************
 * Static/global definitions
 ****************************************************************************************/
static const blt_int32u UART_Baudrate_values[] = {
    1200,
    2400,
    4800,
    9600,
    19200,
    38400,
    57600,
    115200
};

static const blt_int32u UART_DataLength_Values[] = {
        UART_WordLength_7b,
        UART_WordLength_8b
};

static const blt_int32u UART_FlowControl_Values[] = {
        UART_HardwareFlowControl_None,
        UART_HardwareFlowControl_RTS_CTS
};

static const blt_int32u UART_Parity_Values[] = {
        UART_Parity_No,
        UART_Parity_Odd,
        UART_Parity_Even
};

static const blt_int32u UART_Stop_Values[] = {
        UART_StopBits_1,
        UART_StopBits_2
};
/****************************************************************************************
 * Static/global definitions
 ****************************************************************************************/
/** theoretical time needed for the reception of 3 bytes
 * (depending on baudrate); Used to know if end of frame is reach or not */
static blt_int32u UART_RXTimeout = 0u;
/****************************************************************************************
 * Function prototypes
 ****************************************************************************************/
static blt_bool UART_F_ReceiveByte(blt_int8u *data);
static void     UART_F_TransmitByte(blt_int8u data);

/************************************************************************************//**
 ** \brief     Initializes the UART communication interface.
 ** \return    none.
 **
 ****************************************************************************************/
void     UART_InitExt(UART_Config_t uart_config)
{
    UART_InitType lUart_InitType;
    GPIO_InitType GPIO_InitStructure;
    blt_int32u lTimeoutus;
    blt_int8u lFrameLength;

    /* Set default value */
    lUart_InitType.UART_BaudRate = 19200;
    lUart_InitType.UART_WordLengthReceive = UART_WordLength_8b;
    lUart_InitType.UART_WordLengthTransmit = UART_WordLength_8b;
    lUart_InitType.UART_Parity = UART_Parity_No;
    lUart_InitType.UART_StopBits = UART_StopBits_1;
    lUart_InitType.UART_HardwareFlowControl = UART_HardwareFlowControl_None;

    /* GPIO Periph clock enable */
    SysCtrl_PeripheralClockCmd(CLOCK_PERIPH_UART | CLOCK_PERIPH_GPIO, ENABLE);

    /* Configure GPIO_Pin_8 and GPIO_Pin_11 as UART_TXD and UART_RXD*/
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8;
    GPIO_InitStructure.GPIO_Mode = Serial1_Mode;
    GPIO_InitStructure.GPIO_Pull = DISABLE;
    GPIO_InitStructure.GPIO_HighPwr = DISABLE;
    GPIO_Init(&GPIO_InitStructure);

    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;
    GPIO_InitStructure.GPIO_Mode = Serial1_Mode;
    GPIO_InitStructure.GPIO_Pull = DISABLE;
    GPIO_InitStructure.GPIO_HighPwr = DISABLE;
    GPIO_Init(&GPIO_InitStructure);

    /* Configure uart settings */
    if(uart_config.baudrate < UART_BAUDRATE_INVALID)
    {
        /* Valid baudrate */
        lUart_InitType.UART_BaudRate = UART_Baudrate_values[uart_config.baudrate];
    }


    if((uart_config.datalength == UART_DATALENGTH_7) ||
       (uart_config.datalength == UART_DATALENGTH_8))
    {
        lUart_InitType.UART_WordLengthReceive = UART_DataLength_Values[uart_config.datalength - UART_DATALENGTH_7];
        lUart_InitType.UART_WordLengthTransmit = UART_DataLength_Values[uart_config.datalength - UART_DATALENGTH_7];
    }

    if((uart_config.flow == UART_FLOW_NONE) ||
       (uart_config.flow == UART_FLOW_CTSRTSBUFFERMODE))
    {
        lUart_InitType.UART_HardwareFlowControl = UART_FlowControl_Values[uart_config.flow - UART_FLOW_NONE];
    }

    if((uart_config.parity == UART_PARITY_NONE) ||
       (uart_config.parity == UART_PARITY_ODD)  ||
       (uart_config.parity == UART_PARITY_EVEN))
    {
        lUart_InitType.UART_Parity = UART_Parity_Values[uart_config.parity];
    }

    if((uart_config.stop == UART_STOP_1) ||
       (uart_config.stop == UART_STOP_2))
    {
        lUart_InitType.UART_StopBits = UART_Stop_Values[uart_config.stop - UART_STOP_1];
    }

    lUart_InitType.UART_Mode = UART_Mode_Rx | UART_Mode_Tx;
    lUart_InitType.UART_Mode = UART_Mode_Rx | UART_Mode_Tx;
    lUart_InitType.UART_FifoEnable = ENABLE;
    UART_Init(&lUart_InitType);

    /* Calculation of the frame length in bits */
    lFrameLength = lUart_InitType.UART_WordLengthReceive + (5u-UART_WordLength_5b);
    lFrameLength++; // bit de start
    lFrameLength += lUart_InitType.UART_StopBits + (1u-UART_StopBits_1);
    if(lUart_InitType.UART_Parity != UART_Parity_No)
        lFrameLength++;

    /* Calculation of RX and TX Timeout (based on 1 bytes = 8data bits + 1stop bit
     * + 1start bit */
    lTimeoutus = (UART_INTER_FRAME_BYTE * lFrameLength * 1000000u)/lUart_InitType.UART_BaudRate;
    /* Add one ms if needed */
    UART_RXTimeout = (lTimeoutus/1000u) + (lTimeoutus%1000 == 0 ? 0 : 1);

    /* Interrupt as soon as data is received. */
    UART_RxFifoIrqLevelConfig(FIFO_LEV_1_64);

    /* Enable UART */
    UART_Cmd(ENABLE);

} /*** end of UART_InitExt ***/

/************************************************************************************//**
 ** \brief     Transmits a packet formatted for the communication interface.
 ** \param     data Pointer to byte array with data that it to be transmitted.
 ** \param     len  Number of bytes that are to be transmitted.
 ** \return    none.
 **
 ****************************************************************************************/
void UART_TransmitPacket(blt_int8u *data, blt_int8u len)
{
    blt_int16u data_index;

    /* verify validity of the len-paramenter */
    if(len <= UART_TX_MAX_DATA)
    {
        /* first transmit the length of the packet */
        UART_F_TransmitByte(len);

        /* transmit all the packet bytes one-by-one */
        for (data_index = 0; data_index < len; data_index++)
        {
            /* write byte */
            UART_F_TransmitByte(data[data_index]);
        }
    }
} /*** end of UART_TransmitPacket ***/

/************************************************************************************//**
 ** \brief     Receives a communication interface packet if one is present.
 ** \param     data Pointer to byte array where the data is to be stored.
 ** \param     len Pointer where the length of the packet is to be stored.
 ** \return    BLT_TRUE if a packet was received, BLT_FALSE otherwise.
 **
 ****************************************************************************************/
blt_bool UART_ReceivePacket(blt_int8u *data, blt_int8u *len)
{
    static blt_int8u xcpCtoReqPacket[BLT_CONF_C_UART_RX_MAX_DATA+1];  /* one extra for length */
    static blt_int8u xcpCtoRxLength;
    static blt_bool  xcpCtoRxInProgress = BLT_FALSE;
    static blt_int32u xcpCtoLastRxTime = 0;

    /* start of cto packet received? */
    if (xcpCtoRxInProgress == BLT_FALSE)
    {
        /* store the message length when received */
        if (UART_F_ReceiveByte(&xcpCtoReqPacket[0]) == BLT_TRUE)
        {
            if ( (xcpCtoReqPacket[0] > 0) &&
                    (xcpCtoReqPacket[0] <= BLT_CONF_C_UART_RX_MAX_DATA) )
            {
                /* store the reception time */
                xcpCtoLastRxTime = TIMER_Get();
                /* reset packet data count */
                xcpCtoRxLength = 0;
                /* indicate that a cto packet is being received */
                xcpCtoRxInProgress = BLT_TRUE;
            }
        }
    }
    else
    {
        /* store the next packet byte */
        if (UART_F_ReceiveByte(&xcpCtoReqPacket[xcpCtoRxLength+1]) == BLT_TRUE)
        {
            /* store the reception time */
            xcpCtoLastRxTime = TIMER_Get();
            /* increment the packet data count */
            xcpCtoRxLength++;

            /* check to see if the entire packet was received */
            if(xcpCtoRxLength == xcpCtoReqPacket[0])
            {
                /* copy the packet data */
                memcpy((void *)data, (void *)&xcpCtoReqPacket[1], xcpCtoRxLength);
                /* done with cto packet reception */
                xcpCtoRxInProgress = BLT_FALSE;
                /* set the packet length */
                *len = xcpCtoRxLength;
                /* packet reception complete */
                return BLT_TRUE;
            }
        }
        else
        {
            /* check packet reception timeout */
            if (TIMER_Get() > (xcpCtoLastRxTime + UART_RXTimeout))
            {
                /* cancel cto packet reception due to timeout. note that that automaticaly
                 * discards the already received packet bytes, allowing the host to retry.
                 */
                xcpCtoRxInProgress = BLT_FALSE;
            }
        }
    }
    /* packet reception not yet complete */
    return BLT_FALSE;
} /*** end of UART_ReceivePacket ***/

/************************************************************************************//**
 ** \brief     Receives a communication interface byte if one is present.
 ** \param     data Pointer to byte where the data is to be stored.
 ** \return    BLT_TRUE if a byte was received, BLT_FALSE otherwise.
 **
 ****************************************************************************************/
static blt_bool UART_F_ReceiveByte(blt_int8u *data)
{
    blt_bool result = BLT_FALSE;

    /* check flag to see if a byte was received */
    if(RESET == UART_GetFlagStatus(UART_FLAG_RXFE))
    {
        /* retrieve and store the newly received byte */
        *data = (blt_int8u)UART_ReceiveData();
        /* update the result for success */
        result = BLT_TRUE;
    }
    /* give the result back to the caller */
    return result;
} /*** end of UART_F_ReceiveByte ***/

/************************************************************************************//**
 ** \brief     Transmits a communication interface byte.
 ** \param     data Value of byte that is to be transmitted.
 ** \return    none.
 **
 ****************************************************************************************/
static void UART_F_TransmitByte(blt_int8u data)
{
    blt_int32u timeout;

    /* write byte to transmit holding register */
    UART_SendData(data);

    /* set timeout time to wait for transmit completion. */
    timeout = TIMER_Get() + UART_BYTE_TX_TIMEOUT;
    /* wait for tx holding register to be empty */
    while (RESET == UART_GetFlagStatus(UART_FLAG_TXFE))
    {
        /* break loop upon timeout. this would indicate a hardware failure. */
        if (TIMER_Get() > timeout)
        {
            break;
        }
    }
} /*** end of UART_F_TransmitByte ***/


/*********************************** end of uart.c *************************************/
