/*
    / \     _   |_|
   / _ \  _| |_  _  _____
  / /_\ \(_   _)| ||   |
 / _____ \ | |  | || | | |
/_/     \_\\__) |_||_|_|_|
  (C)2018 Atim

Description: Application

License:

Maintainer: Youen Lebret

 */
#ifndef COM_H
#define COM_H

/****************************************************************************************
 * Include files
 ****************************************************************************************/
#include "xcp.h"

/****************************************************************************************
 * Macro definitions
 ****************************************************************************************/

/****************************************************************************************
 * Define definitions
 ****************************************************************************************/

/****************************************************************************************
 * Type definitions
 ****************************************************************************************/
/** \brief Enumeration for the different communication interfaces. */
typedef enum
{
    COM_IF_UART   = 0x00u,                                   /**< UART interface                   */
    COM_IF_BLE    = 0x10u,                                   /**< BLE interface                    */
    COM_IF_OTHER  = 0xFFu                                    /**< Other interface                  */
} COM_InterfaceId_t;


/****************************************************************************************
 * Function prototypes
 ****************************************************************************************/
void            COM_Init(void);
void            COM_Task(void);
blt_int16u      COM_GetActiveInterfaceMaxRxLen(void);
blt_int16u      COM_GetActiveInterfaceMaxTxLen(void);
void            COM_TransmitPacket(blt_int8u *data, blt_int16u len);
blt_bool        COM_IsConnected(void);


#endif /* COM_H */
/*********************************** end of com.h **************************************/
