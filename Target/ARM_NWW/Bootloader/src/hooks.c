/*
    / \     _   |_|
   / _ \  _| |_  _  _____
  / /_\ \(_   _)| ||     |
 / _____ \ | |  | || | | |
/_/     \_\\__) |_||_|_|_|
    (C)2018 Atim

Description: Application

License:

Maintainer: Youen Lebret

*/

/****************************************************************************************
* Include files
****************************************************************************************/
#include "blt.h"


/****************************************************************************************
*   S E C U R I T Y   H O O K   F U N C T I O N S
****************************************************************************************/

#ifdef BLT_CONF_AUTHENTIFICATION_ENABLE

#define XCP_SEED_LEN        16u

static const blt_int8u XCP_Seed[XCP_SEED_LEN] = {0x7D, 0x5E, 0x1A, 0xC6, 0x35,
        0xF3, 0x09, 0x3E, 0x56, 0xEC, 0x97, 0x1D, 0x8C, 0x90, 0x32, 0x55};

/************************************************************************************//**
** \brief     Provides a seed to the XCP master that will be used for the key
**            generation when the master attempts to unlock the specified resource.
**            Called by the GET_SEED command.
** \param     resource  Resource that the seed if requested for (XCP_RES_XXX).
** \param     seed      Pointer to byte buffer wher the seed will be stored.
** \return    Length of the seed in bytes.
**
****************************************************************************************/
blt_int8u XcpGetSeedHook(blt_int8u resource, blt_int8u *seed)
{
  /* request seed for unlocking ProGraMming resource */
  if ((resource & XCP_RES_PGM) != 0)
  {
    memcpy(seed, &XCP_Seed, XCP_SEED_LEN);
  }

  /* return seed length */
  return XCP_SEED_LEN;
} /*** end of XcpGetSeedHook ***/


/************************************************************************************//**
** \brief     Called by the UNLOCK command and checks if the key to unlock the
**            specified resource was correct. If so, then the resource protection
**            will be removed.
** \param     resource  resource to unlock (XCP_RES_XXX).
** \param     key       pointer to the byte buffer holding the key.
** \param     len       length of the key in bytes.
** \return    1 if the key was correct, 0 otherwise.
**
****************************************************************************************/
blt_int8u XcpVerifyKeyHook(blt_int8u resource, blt_int8u *key, blt_int8u len)
{
  blt_int8u idx;
  blt_int8u cpt = 0;
  blt_int8u seedcheck[XCP_SEED_LEN];

  /* check key for unlocking ProGraMming resource */
  if ((resource == XCP_RES_PGM))
  {
    /* Check the provided key and len */
    if(XCP_SEED_LEN != len )
    {
        /* incorrect len */
        return 0;
    }

    for (idx = 0; idx < XCP_SEED_LEN; idx++)
    {
        cpt += XCP_Seed[(len -1 ) - idx];
        seedcheck[idx] = XCP_Seed[idx] ^ cpt ^ XCP_SEED_LEN;

        if(seedcheck[idx] != key[idx])
        {
          /* incorrect key */
          return 0;
        }
    }
    /* correct key received for unlocking PGM resource */
    return 1;
  }

  /* still here so key incorrect */
  return 0;
} /*** end of XcpVerifyKeyHook ***/
#endif /* BLT_CONF_AUTHENTIFICATION_ENABLE */


/*********************************** end of hooks.c ************************************/
