/*
	/ \	    _   |_|
   / _ \  _| |_  _  _____
  / /_\ \(_   _)| ||	 |
 / _____ \ | |  | || | | |
/_/	    \_\\__) |_||_|_|_|
	(C)2018 Atim

Description: Application

License:

Maintainer: Youen Lebret

 */

/****************************************************************************************
 * Include files
 ****************************************************************************************/
#include "hal_types.h"
#include "system_BlueNRG.h"
#include "clock.h"
#include "blt.h"                                /* bootloader generic header          */
#include "blt_conf.h"
#include "ble_status.h"
#include "bluenrg1_api.h"
#include "bluenrg1_gap.h"
#include "uart.h"
#include "sleep.h"
#include "ble.h"
#include "boot_ble.h"

/****************************************************************************************
 * Global - Static Variables
 ****************************************************************************************/

/****************************************************************************************
 * Function prototypes
 ****************************************************************************************/


/************************************************************************************//**
 ** \brief     This is the entry point for the bootloader application and is called
 **            by the reset interrupt vector after the C-startup routines executed.
 ** \return    Program return code.
 **
 ****************************************************************************************/
int main(void)
{
    /* System Init */
    SystemInit();

    /* Initialize the bootloader. */
    BLT_Init();

    /* BLE Init */
    BOOTBLE_Init();

    /* Start the infinite program loop. */
    while (1)
    {
        /* BlueNRG-1 stack tick */
        BLE_StackTick();

        /* Run the bootloader task. */
        BLT_Task();
    }
} /*** end of main ***/



/*********************************** end of main.c *************************************/
