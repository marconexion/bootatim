/*
    / \     _   |_|
   / _ \  _| |_  _  _____
  / /_\ \(_   _)| ||   |
 / _____ \ | |  | || | | |
/_/     \_\\__) |_||_|_|_|
  (C)2018 Atim

Description: Application

License:

Maintainer: Youen Lebret

*/

#ifndef BLT_CONF_H
#define BLT_CONF_H

#include "bluenrg1_stack.h"
#include "stack_user_cfg.h"
#include "compiler.h"
#include "uart.h"
#include <stdint.h>
#include <string.h>


/****************************************************************************************
*   U A R T  C O M M U N I C A T I O N   I N T E R F A C E   C O N F I G U R A T I O N
****************************************************************************************/
/* The maximum amount of data bytes in a message for data transmission
 * and reception is set through BLT_CONF_C_UART_TX_MAX_DATA and BLT_CONF_C_UART_RX_MAX_DATA,
 * respectively.
 */
/** \brief Configure number of bytes in the target->host data packet. */
#define BLT_CONF_C_UART_TX_MAX_DATA       (255)
/** \brief Configure number of bytes in the host->target data packet. */
#define BLT_CONF_C_UART_RX_MAX_DATA       (255)

#if(BLT_CONF_C_UART_TX_MAX_DATA > UART_TX_MAX_DATA)
#error "BLT_CONF_C_UART_TX_MAX_DATA must not be less or equal than 255"
#endif

#if(BLT_CONF_C_UART_RX_MAX_DATA > UART_RX_MAX_DATA)
#error "BLT_CONF_C_UART_RX_MAX_DATA must not be less or equal than 255"
#endif


/****************************************************************************************
*   S E C U R I T Y   C O N F I G U R A T I O N
****************************************************************************************/
/* By activating this security configuration, the bootloader and host program will both
 * perform an authentification check based on a common algorithm. the seed needed by the algorithm
 * is provided by the host. Once the result is calculated by the bootloader, this one is returned
 * to the host and compared to the result calculated by the host (libseednkey.dll). If mismatch,
 * no reprog actions is allowed. */
#ifdef RELEASE_MODE
#define BLT_CONF_AUTHENTIFICATION_ENABLE

/* By activating this security configuration, the bootloader calculate a checksum during a reprog
 * action and store it in FLASH. during normal boot (no reprog, this checksum is recalculate
 * and compared against the one stored. If mismatch, the user application is not started */
#define BLT_CONF_CHECKSUM_VERIFY_ENABLE

/* By activating this security configuration, the unwanted access
 * while in debug mode (halt mode) is not possible. As soon as there is a access to Flash Memory
 * by the flash debugger, 0x0 will be return and a AHB error is generated. It prevent any flash dump
 * through external probe */
#define BLT_CONF_C_FLASH_READOUT_PROTECTION
#endif
/* In debug mode, no protection is enabled. It is easier for debugging purpose */

#endif /* BLT_CONF_H */
/*********************************** end of blt_conf.h *********************************/
