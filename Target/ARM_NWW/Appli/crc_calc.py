#!/usr/bin/env python

# usage: $ ./srec-checksum.py <s-record without checksum> <checksum_file to save>

import sys
import struct


file = sys.argv[1]
checksum_file = sys.argv[2]
checksum = 0
payload  = ""
payload_size = 0
parser = {}
i = 0
chunk = 0

def swap32(i):
    return struct.unpack("<I", struct.pack(">I", i))[0]
	
with open(file) as srec:
    for index, line in enumerate(srec):
        parser["line"] = index
        parser["type"] = line[0:2]
        parser["size"] = int(line[2:4], 16)
        parser["crc"] = line[-4:]
        #les champs donnees et adresses dependent du type
        if parser["type"] == "S1":
            parser["addr"] = line[4:8]
            #-4, car les deux dernier characteres sont CRLF
            parser["data"] = line[8:-4]
            parser["data_size"] = len(parser["data"])
        elif parser["type"] == "S2":
            parser["addr"] = line[4:10]
            #-4, car les deux dernier characteres sont CRLF
            parser["data"] = line[10:-4]
            parser["data_size"] = len(parser["data"])
        elif parser["type"] == "S3":
            parser["addr"] = line[4:12]
            #-4, car les deux dernier characteres sont CRLF
            parser["data"] = line[12:-4]
            parser["data_size"] = len(parser["data"])
        else:
            parser["addr"] = "NULL"
            parser["data"] = "NULL"
            parser["data_size"] = 0
        if parser["data"] != "NULL":
            payload += parser["data"]
            payload_size += parser["data_size"]

        #print "line : %d, type : %s, data size %d, addr : %s, data : %s, crc : %s "%(parser["line"], parser["type"], parser["data_size"], parser["addr"], parser["data"], parser["crc"])

#print "payload : %s \ntotal size : %d"%(payload, payload_size)

#Calcul du checksum
while i < payload_size - 8:
    chunk = int(payload[i:i+8], 16)
    chunk = swap32(chunk)
    #print "add 0x%08x"%(chunk)
    checksum += chunk
    #print "checksum 0x%08x"%(checksum)
    i += 8
	
checksum = checksum & 0xFFFFFFFF
checksum = swap32(checksum)
checksum = "%08x" % checksum
checksum = checksum.decode("hex")

check_file = open(checksum_file, "w");
check_file.write(checksum)
check_file.close()
