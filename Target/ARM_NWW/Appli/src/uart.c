/*
	/ \	    _   |_|
   / _ \  _| |_  _  _____
  / /_\ \(_   _)| ||	 |
 / _____ \ | |  | || | | |
/_/	    \_\\__) |_||_|_|_|
	(C)2018 Atim

Description: Application

License:

Maintainer: Youen Lebret

 */

/****************************************************************************************
 * Include files
 ****************************************************************************************/
#include "uart.h"
#include "BlueNRG1_uart.h"
#include "BlueNRG1_sysCtrl.h"
#include "BlueNRG1_gpio.h"
#include "clock.h"

#include <string.h>

/****************************************************************************************
 * Macro definitions
 ****************************************************************************************/
/** \brief Timeout time for the reception of a CTO packet. The timer is started upon
 *         reception of the first packet byte.
 */
#define UART_C_CTO_RX_PACKET_TIMEOUT_MS (100u)
/** \brief Timeout for transmitting a byte in milliseconds. */
#define UART_C_BYTE_TX_TIMEOUT_MS       (10u)

#define UART_RX_MAX_DATA              (64u)
#define UART_TX_MAX_DATA              (64u)
#define UART_BAUDRATE                 (115200u)

/****************************************************************************************
 * Function prototypes
 ****************************************************************************************/
static BOOL     UART_F_ReceiveByte(uint8_t *data);
static void     UART_F_TransmitByte(uint8_t data);

/************************************************************************************//**
 ** \brief     Initializes the UART communication interface.
 ** \return    none.
 **
 ****************************************************************************************/
void UART_InitExt(void)
{
    UART_InitType lUart_InitType;
    GPIO_InitType GPIO_InitStructure;

    /* GPIO Periph clock enable */
    SysCtrl_PeripheralClockCmd(CLOCK_PERIPH_UART | CLOCK_PERIPH_GPIO, ENABLE);

    /* Configure GPIO_Pin_8 and GPIO_Pin_11 as UART_TXD and UART_RXD*/
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8;
    GPIO_InitStructure.GPIO_Mode = Serial1_Mode;
    GPIO_InitStructure.GPIO_Pull = DISABLE;
    GPIO_InitStructure.GPIO_HighPwr = DISABLE;
    GPIO_Init(&GPIO_InitStructure);

    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;
    GPIO_InitStructure.GPIO_Mode = Serial1_Mode;
    GPIO_Init(&GPIO_InitStructure);


    /* configure UART peripheral */
    lUart_InitType.UART_BaudRate = UART_BAUDRATE;
    lUart_InitType.UART_WordLengthReceive = UART_WordLength_8b;
    lUart_InitType.UART_WordLengthTransmit = UART_WordLength_8b;
    lUart_InitType.UART_StopBits = UART_StopBits_1;
    lUart_InitType.UART_Parity = UART_Parity_No;
    lUart_InitType.UART_Mode = UART_Mode_Rx | UART_Mode_Tx;
    lUart_InitType.UART_HardwareFlowControl = UART_HardwareFlowControl_None;
    lUart_InitType.UART_Mode = UART_Mode_Rx | UART_Mode_Tx;
    lUart_InitType.UART_FifoEnable = ENABLE;
    UART_Init(&lUart_InitType);

    /* Interrupt as soon as data is received. */
    UART_RxFifoIrqLevelConfig(FIFO_LEV_1_64);

    /* Enable UART */
    UART_Cmd(ENABLE);

} /*** end of UART_InitExt ***/

/************************************************************************************//**
 ** \brief     Transmits a packet formatted for the communication interface.
 ** \param     data Pointer to byte array with data that it to be transmitted.
 ** \param     len  Number of bytes that are to be transmitted.
 ** \return    none.
 **
 ****************************************************************************************/
void UART_TransmitPacket(uint8_t *data, uint8_t len)
{
    uint16_t data_index;

    /* first transmit the length of the packet */
    UART_F_TransmitByte(len);

    /* transmit all the packet bytes one-by-one */
    for (data_index = 0; data_index < len; data_index++)
    {
        /* write byte */
        UART_F_TransmitByte(data[data_index]);
    }
} /*** end of UART_TransmitPacket ***/

/************************************************************************************//**
 ** \brief     Receives a communication interface packet if one is present.
 ** \param     data Pointer to byte array where the data is to be stored.
 ** \param     len Pointer where the length of the packet is to be stored.
 ** \return    TRUE if a packet was received, FALSE otherwise.
 **
 ****************************************************************************************/
BOOL UART_ReceivePacket(uint8_t *data, uint8_t *len)
{
    static uint8_t xcpCtoReqPacket[UART_RX_MAX_DATA+1];  /* one extra for length */
    static uint8_t xcpCtoRxLength;
    static BOOL  xcpCtoRxInProgress = FALSE;
    static tClockTime xcpCtoRxStartTime = 0;

    /* start of cto packet received? */
    if (xcpCtoRxInProgress == FALSE)
    {
        /* Reset  the packet data count */
        xcpCtoRxLength = 0;

        /* store the message length when received */
        if (UART_F_ReceiveByte(&xcpCtoReqPacket[xcpCtoRxLength]) == TRUE)
        {
            /* check if first char is also the last char */
            if ( (xcpCtoReqPacket[xcpCtoRxLength] == '\n') ||
                    (xcpCtoReqPacket[xcpCtoRxLength] == '\r') )
            {
                /* Increment the packet data count */
                xcpCtoRxLength++;
                /* copy the packet data (without the last char '\n' or '\r' */
                memcpy((void *)data, (void * )xcpCtoReqPacket, xcpCtoRxLength);
                /* done with cto packet reception */
                xcpCtoRxInProgress = FALSE;
                /* set the packet length */
                *len = xcpCtoRxLength;
                /* packet reception complete */
                return TRUE;
            }
            else
            {
                /* indicate that a cto packet is being received */
                xcpCtoRxInProgress = TRUE;
                /* store the start time */
                xcpCtoRxStartTime = Clock_Time();
                /* Increment the packet data count */
                xcpCtoRxLength++;
            }
        }
    }
    else
    {
        /* store the next packet byte */
        if (UART_F_ReceiveByte(&xcpCtoReqPacket[xcpCtoRxLength]) == TRUE)
        {
            /* check to see if the entire packet was received */
            if ((xcpCtoReqPacket[xcpCtoRxLength] == '\n') ||
                    (xcpCtoReqPacket[xcpCtoRxLength] == '\r'))
            {
                /* copy the packet data (without the last char '\n' or '\r' */
                memcpy((void *)data, (void * )xcpCtoReqPacket, xcpCtoRxLength);
                /* done with cto packet reception */
                xcpCtoRxInProgress = FALSE;
                /* set the packet length */
                *len = xcpCtoRxLength;
                /* packet reception complete */
                return TRUE;
            }
            else
            {
                /* increment the packet data count, and continue reception  */
                xcpCtoRxLength++;
            }
        }
        else
        {
            /* check packet reception timeout */
            if (Clock_Time() > (xcpCtoRxStartTime + UART_C_CTO_RX_PACKET_TIMEOUT_MS))
            {
                /* cancel cto packet reception due to timeout. note that that automaticaly
                 * discards the already received packet bytes, allowing the host to retry.
                 */
                xcpCtoRxInProgress = FALSE;
            }
        }
    }
    /* packet reception not yet complete */
    return FALSE;
} /*** end of UART_ReceivePacket ***/

/************************************************************************************//**
 ** \brief     Receives a communication interface byte if one is present.
 ** \param     data Pointer to byte where the data is to be stored.
 ** \return    TRUE if a byte was received, FALSE otherwise.
 **
 ****************************************************************************************/
static BOOL UART_F_ReceiveByte(uint8_t *data)
{
    BOOL result = FALSE;

    /* check flag to see if a byte was received */
    if(RESET == UART_GetFlagStatus(UART_FLAG_RXFE))
    {
        /* retrieve and store the newly received byte */
        *data = (uint8_t)UART_ReceiveData();
        /* update the result for success */
        result = TRUE;
    }
    /* give the result back to the caller */
    return result;
} /*** end of UART_F_ReceiveByte ***/

/************************************************************************************//**
 ** \brief     Transmits a communication interface byte.
 ** \param     data Value of byte that is to be transmitted.
 ** \return    none.
 **
 ****************************************************************************************/
static void UART_F_TransmitByte(uint8_t data)
{
    tClockTime timeout;

    /* write byte to transmit holding register */
    UART_SendData(data);

    /* set timeout time to wait for transmit completion. */
    timeout = Clock_Time() + UART_C_BYTE_TX_TIMEOUT_MS;
    /* wait for tx holding register to be empty */
    while (RESET == UART_GetFlagStatus(UART_FLAG_TXFE))
    {
        /* break loop upon timeout. this would indicate a hardware failure. */
        if (Clock_Time() > timeout)
        {
            break;
        }
    }
} /*** end of UART_F_TransmitByte ***/


/*********************************** end of uart.c *************************************/
