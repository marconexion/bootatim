/*
    / \     _   |_|
   / _ \  _| |_  _  _____
  / /_\ \(_   _)| ||     |
 / _____ \ | |  | || | | |
/_/     \_\\__) |_||_|_|_|
    (C)2018 Atim

Description: Application

License:

Maintainer: Youen Lebret

 */
#ifndef APP_BLE_H
#define APP_BLE_H

#include "hal_types.h"
#include "stack_user_cfg.h"
#include "bluenrg1_stack.h"
#include "BlueNRG_x_device.h"
/****************************************************************************************
 * Type definitions
 ****************************************************************************************/
/****************************************************************************************
 *   BLE  C O N F I G U R A T I O N
 ****************************************************************************************/
/* This file contains all the information needed to init the BlueNRG-1 stack.
 * These constants and variables are used from the BlueNRG-1 stack to reserve RAM and FLASH
 * according the application requests.
 */

/* Default number of link */
#define APPBLE_MIN_NUM_LINK                1
/* Default number of GAP and GATT services */
#define APPBLE_DEFAULT_NUM_GATT_SERVICES   2
/* Default number of GAP and GATT attributes */
#define APPBLE_DEFAULT_NUM_GATT_ATTRIBUTES 9

/* Number of services for BLE module  */
#define APPBLE_NUM_APP_GATT_SERVICES 1

/* Number of attributes for BLE module */
#define APPBLE_NUM_APP_GATT_ATTRIBUTES 9

/* Number of links needed for the demo: 1
 * Only 1 the default
 */
#define APPBLE_NUM_LINKS               (APPBLE_MIN_NUM_LINK)

/* Number of GATT services needed for the OTA Service Manager application. */
#define APPBLE_NUM_GATT_SERVICES       (APPBLE_DEFAULT_NUM_GATT_SERVICES + APPBLE_NUM_APP_GATT_SERVICES)

/* Number of GATT attributes needed for the OTA Service Manager application. */
#define APPBLE_NUM_GATT_ATTRIBUTES     (APPBLE_DEFAULT_NUM_GATT_ATTRIBUTES + APPBLE_NUM_APP_GATT_ATTRIBUTES)

/* Enable/disable Data length extension Max supported ATT_MTU size based on OTA client & server Max ATT_MTU sizes capabilities */
#define APPBLE_MAX_ATT_MTU_SIZE        (251u)    /* DEFAULT_ATT_MTU size = 23 bytes */

/* Array size for the attribute value for OTA service */
#define APPBLE_ATT_VALUE_ARRAY_SIZE    (19 * APPBLE_NUM_GATT_ATTRIBUTES)

/* Flash security database size */
#define APPBLE_FLASH_SEC_DB_SIZE       (0x400)

/* Flash server database size */
#define APPBLE_FLASH_SERVER_DB_SIZE    (0x400)


/* Set the minumum number of prepare write requests needed for a long write procedure for a characteristic with len > 20bytes:
 *
 * It returns 0 for characteristics with len <= 20bytes
 *
 * NOTE: If prepare write requests are used for a characteristic (reliable write on multiple characteristics), then
 * this value should be set to the number of prepare write needed by the application.
 *
 *  [New parameter added on BLE stack v2.x]
 */
#define APPBLE_PREPARE_WRITE_LIST_SIZE (DIV_CEIL(DEFAULT_MAX_ATT_SIZE, APPBLE_MAX_ATT_MTU_SIZE - 5U) * 2)

/* Set the number of memory block for packet allocation */
#define APPBLE_MBLOCKS_COUNT           MBLOCKS_CALC(APPBLE_PREPARE_WRITE_LIST_SIZE, APPBLE_MAX_ATT_MTU_SIZE, APPBLE_NUM_LINKS)

/* Maximum duration of the connection event */
#define APPBLE_MAX_CONN_EVENT_LENGTH 0xFFFFFFFF

/* Sleep clock accuracy */
#if (LS_SOURCE == LS_SOURCE_INTERNAL_RO)

/* Sleep clock accuracy in Slave mode */
#define APPBLE_SLAVE_SLEEP_CLOCK_ACCURACY 500

/* Sleep clock accuracy in Master mode */
#define APPBLE_MASTER_SLEEP_CLOCK_ACCURACY MASTER_SCA_500ppm

#else

/* Sleep clock accuracy in Slave mode */
#define APPBLE_SLAVE_SLEEP_CLOCK_ACCURACY 100

/* Sleep clock accuracy in Master mode */
#define APPBLE_MASTER_SLEEP_CLOCK_ACCURACY MASTER_SCA_100ppm

#endif

/* Low Speed Oscillator source */
#if (LS_SOURCE == LS_SOURCE_INTERNAL_RO)
#define APPBLE_LOW_SPEED_SOURCE  1 // Internal RO
#else
#define APPBLE_LOW_SPEED_SOURCE  0 // External 32 KHz
#endif

/* High Speed start up time */
#define APPBLE_HS_STARTUP_TIME 328 // 800 us

/* Radio Config Hot Table */
extern uint8_t hot_table_radio_config[];

/* Low level hardware configuration data for the device */
#define APPBLE_CONFIG_TABLE                            \
{                                                      \
    (uint32_t*)hot_table_radio_config,                 \
    APPBLE_MAX_CONN_EVENT_LENGTH,                      \
    APPBLE_SLAVE_SLEEP_CLOCK_ACCURACY,                 \
    APPBLE_MASTER_SLEEP_CLOCK_ACCURACY,                \
    APPBLE_LOW_SPEED_SOURCE,                           \
    APPBLE_HS_STARTUP_TIME                             \
}

/****************************************************************************************
 * Function prototypes
 ****************************************************************************************/
tBleStatus APPBLE_Init(void);
void APPBLE_Task(void);

#endif /* APP_BLE_H */
/*********************************** end of uart.h *************************************/
