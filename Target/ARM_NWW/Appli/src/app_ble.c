/*
    / \     _   |_|
   / _ \  _| |_  _  _____
  / /_\ \(_   _)| ||     |
 / _____ \ | |  | || | | |
/_/     \_\\__) |_||_|_|_|
    (C)2018 Atim

Description: Application

License:

Maintainer: Youen Lebret

 */

/****************************************************************************************
 * Include files
 ****************************************************************************************/
#include "ble.h"
#include "app_ble.h"
#include "bluenrg1_events.h"
#include "bluenrg1_gap.h"
#include "bluenrg1_hal.h"
#include "clock.h"
#include "BlueNRG1_flash.h"
#include <string.h>
#include <stdbool.h>

/****************************************************************************************
 * Macro definitions
 ****************************************************************************************/

/****************************************************************************************
 * Define definitions
 ****************************************************************************************/
/** Minimum interval for connection */
#define APPBLE_MIN_CONN_INTERVAL    (0x0006u)

/** Max interval for connection */
#define APPBLE_MAX_CONN_INTERVAL    (0x0C80u)

/** Minimum interval for connection */
#define APPBLE_MIN_ADV_INTERVAL     (0x0020u)

/** Max interval for connection */
#define APPBLE_MAX_ADV_INTERVAL     (0x4000u)

/** Address where the unique device ID is stored */
#define APPBLE_DEVICE_ADDR     (0x100007F4u)

/** Address offset to apply to vector table to go to the fist address after
 * vector table */
#define APPBLE_VECT_TABLE_OFFSET    (0xC0u)

/****************************************************************************************
 * Type definitions
 ****************************************************************************************/
typedef enum APPBLE_State_e {
    APPBLE_STATE_IDLE,
    APPBLE_STATE_BRIDGE_SGFX,
    APPBLE_STATE_RECEIVED
} APPBLE_State_t;

/****************************************************************************************
 * Static Function prototypes
 ****************************************************************************************/
/** callback definition */
static void APPBLE_ConnectCompleteEvent(uint8_t Status,
        uint16_t Connection_Handle,
        uint8_t Role,
        uint8_t Peer_Address_Type,
        uint8_t Peer_Address[6],
        uint16_t Conn_Interval,
        uint16_t Conn_Latency,
        uint16_t Supervision_Timeout,
        uint8_t Master_Clock_Accuracy);

static void APPBLE_AttributeModifiedEvent(uint16_t Connection_Handle,
        uint16_t Attr_Handle,
        uint16_t Offset,
        uint16_t Attr_Data_Length,
        uint8_t Attr_Data[]);

static void APPBLE_DisconnectEvent(uint8_t Status,
        uint16_t Connection_Handle,
        uint8_t Reason);

static BOOL APPBLE_ReceivePacket(uint8_t *data, uint8_t *len);
static void APPBLE_GetInterval(uint16_t *pIntervalAdvMin, uint16_t *pIntervalAdvMax,
        uint16_t *pIntervalConnMin, uint16_t *pIntervalConnMax);

/****************************************************************************************
 * Static/global definitions
 ****************************************************************************************/
/** RAM reserved to manage all the data stack according the number of links,
 * number of services, number of attributes and attribute value length
 */
NO_INIT(uint32_t APPBLE_dynAllocA[TOTAL_BUFFER_SIZE(APPBLE_NUM_LINKS, APPBLE_NUM_GATT_ATTRIBUTES, APPBLE_NUM_GATT_SERVICES,
        APPBLE_ATT_VALUE_ARRAY_SIZE, APPBLE_MBLOCKS_COUNT, CONTROLLER_DATA_LENGTH_EXTENSION_ENABLED)>>2]);
/** FLASH reserved to store all the security database information and
 * and the server database information
 */
NO_INIT_SECTION(uint32_t APPBLE_StacklibFlashData[TOTAL_FLASH_BUFFER_SIZE(APPBLE_FLASH_SEC_DB_SIZE, APPBLE_FLASH_SERVER_DB_SIZE)>>2], ".noinit.stacklib_flash_data");

/** FLASH reserved to store: security root keys, static random address, public address */
NO_INIT_SECTION(uint8_t APPBLE_StacklibStoredDeviceIdData[56], ".noinit.stacklib_stored_device_id_data");

/** This structure contains memory and low level hardware configuration data for the device */
const BlueNRG_Stack_Initialization_t APPBLE_BlueNRGStackInitParams = {
        (uint8_t*)APPBLE_StacklibFlashData,
        APPBLE_FLASH_SEC_DB_SIZE,
        APPBLE_FLASH_SERVER_DB_SIZE,
        (uint8_t*)APPBLE_StacklibStoredDeviceIdData,
        (uint8_t*)APPBLE_dynAllocA,
        TOTAL_BUFFER_SIZE(APPBLE_NUM_LINKS,APPBLE_NUM_GATT_ATTRIBUTES,APPBLE_NUM_GATT_SERVICES,APPBLE_ATT_VALUE_ARRAY_SIZE,APPBLE_MBLOCKS_COUNT,CONTROLLER_DATA_LENGTH_EXTENSION_ENABLED),
        APPBLE_NUM_GATT_ATTRIBUTES,
        APPBLE_NUM_GATT_SERVICES,
        APPBLE_ATT_VALUE_ARRAY_SIZE,
        APPBLE_NUM_LINKS,
        0, /* reserved for future use */
        APPBLE_PREPARE_WRITE_LIST_SIZE,
        APPBLE_MBLOCKS_COUNT,
        APPBLE_MAX_ATT_MTU_SIZE,
        APPBLE_CONFIG_TABLE,
};

/** Device service name in scan process */
static uint8_t APPBLE_serviceUUID4Scan[18]= {0x11,0x07,0x36,0x3d,0x74,0xd0,0x24,0x42,0x4e,0xdc,0x9a,0x0f,0x00,0x3e,0xb4,0xf4,0xa9,0xe6};

/** App service Handle number (updated after adding xcp service to GATT). */
static uint16_t APPBLE_ServHandle = 0u;

/** Rx characteristic Handle number (updated after adding tx char to xcp service). */
static uint16_t APPBLE_CharHandle = 0u;

/** device name during discover process */
static uint8_t APPBLE_DeviceName[6u] = {'A','P','P','B','l','e'};

/** List of Service to Add to the GATT Service */
static BLE_ServiceInfo_t APPBLE_ServiceInfo = {
        {0xe6,0xa9,0xf4,0xb4,0x3e,0x00,0x0f,0x9a,0xdc,0x4e,0x42,0x24,0xd3,0x74,0x3d,0x36},
        BLE_UUID_TYPE_128,
        BLE_PRIMARY_SERVICE,
        4u,
        &APPBLE_ServHandle
};

/** Characteristics Rx to Add to the xcp Service */
static BLE_CharInfo_t APPBLE_CharInfo = {
        &APPBLE_ServHandle,
        {0xe6,0xa9,0xf4,0xb4,0x3e,0x00,0x0f,0x9a,0xdc,0x4e,0x42,0x24,0xd4,0x74,0x3d,0x36},
        BLE_UUID_TYPE_128,
        APPBLE_MAX_ATT_MTU_SIZE,
        (BLE_CHAR_PROP_READ | BLE_CHAR_PROP_WRITE_WITHOUT_RESP),
        BLE_ATTR_PERMISSION_NONE,
        BLE_GATT_NOTIFY_ATTRIBUTE_WRITE,
        BLE_ENC_SIZE_10,
        BLE_FIXED_LEN,
        &APPBLE_CharHandle
};

/** Connection supervision Timeout */
static uint16_t APPBLE_ConnSupervisionTimeout;

/** Connection handle */
static uint16_t APPBLE_ConnHandle;

/** Reception Buffer (for Command_data Characteristics) */
static uint8_t APPBLE_ReceiveBuffer[APPBLE_MAX_ATT_MTU_SIZE];

/** Taille utile du buffer reçu */
static uint8_t APPBLE_ReceivedSize = 0u;

/** Flag for Reception notification */
static BOOL APPBLE_bIsNewReception = FALSE;

/** Etat courrant de la tache APPBLE */
static APPBLE_State_t APPBLE_State = APPBLE_STATE_IDLE;

/** Intervalle de temps Min pour le mode advertising */
static uint16_t APPBLE_IntervalAdvMin = APPBLE_MIN_ADV_INTERVAL;

/** Intervalle de temps Max pour le mode advertising */
static uint16_t APPBLE_IntervalAdvMax = APPBLE_MAX_ADV_INTERVAL;

/** Intervalle de temps Min pour le transfert de donné une fois connecté */
static uint16_t APPBLE_IntervalConnMin = APPBLE_MAX_CONN_INTERVAL;

/** Intervalle de temps Max pour le transfert de donné une fois connecté */
static uint16_t APPBLE_IntervalConnMax = APPBLE_MAX_CONN_INTERVAL;

/** List of Callback to registered */
static BLE_CallBackInfo_t APPBLE_CallBackInfo =
{
        .BLE_hci_disconnection_complete_event_cb = &APPBLE_DisconnectEvent,
        .BLE_hci_encryption_change_event_cb = NULL,
        .BLE_hci_read_remote_version_information_complete_event_cb  = NULL,
        .BLE_hci_hardware_error_event_cb = NULL,
        .BLE_hci_number_of_completed_packets_event_cb = NULL,
        .BLE_hci_data_buffer_overflow_event_cb = NULL,
        .BLE_hci_encryption_key_refresh_complete_event_cb = NULL,
        .BLE_hci_le_connection_complete_event_cb = &APPBLE_ConnectCompleteEvent,
        .BLE_hci_le_advertising_report_event_cb = NULL,
        .BLE_hci_le_connection_update_complete_event_cb = NULL,
        .BLE_hci_le_read_remote_used_features_complete_event_cb = NULL,
        .BLE_hci_le_long_term_key_request_event_cb = NULL,
        .BLE_hci_le_data_length_change_event_cb = NULL,
        .BLE_hci_le_read_local_p256_public_key_complete_event_cb = NULL,
        .BLE_hci_le_generate_dhkey_complete_event_cb = NULL,
        .BLE_hci_le_enhanced_connection_complete_event_cb = NULL,
        .BLE_hci_le_direct_advertising_report_event_cb = NULL,
        .BLE_aci_gap_limited_discoverable_event_cb = NULL,
        .BLE_aci_gap_pairing_complete_event_cb = NULL,
        .BLE_aci_gap_pass_key_req_event_cb = NULL,
        .BLE_aci_gap_authorization_req_event_cb = NULL,
        .BLE_aci_gap_slave_security_initiated_event_cb = NULL,
        .BLE_aci_gap_bond_lost_event_cb = NULL,
        .BLE_aci_gap_proc_complete_event_cb = NULL,
        .BLE_aci_gap_addr_not_resolved_event_cb = NULL,
        .BLE_aci_gap_numeric_comparison_value_event_cb = NULL,
        .BLE_aci_gap_keypress_notification_event_cb = NULL,
        .BLE_aci_gatt_attribute_modified_event_cb = &APPBLE_AttributeModifiedEvent,
        .BLE_aci_gatt_proc_timeout_event_cb = NULL,
        .BLE_aci_att_exchange_mtu_resp_event_cb = NULL,
        .BLE_aci_att_find_info_resp_event_cb = NULL,
        .BLE_aci_att_find_by_type_value_resp_event_cb = NULL,
        .BLE_aci_att_read_by_type_resp_event_cb = NULL,
        .BLE_aci_att_read_resp_event_cb = NULL,
        .BLE_aci_att_read_blob_resp_event_cb = NULL,
        .BLE_aci_att_read_multiple_resp_event_cb = NULL,
        .BLE_aci_att_read_by_group_type_resp_event_cb = NULL,
        .BLE_aci_att_prepare_write_resp_event_cb = NULL,
        .BLE_aci_att_exec_write_resp_event_cb = NULL,
        .BLE_aci_gatt_indication_event_cb = NULL,
        .BLE_aci_gatt_notification_event_cb = NULL,
        .BLE_aci_gatt_proc_complete_event_cb = NULL,
        .BLE_aci_gatt_error_resp_event_cb = NULL,
        .BLE_aci_gatt_disc_read_char_by_uuid_resp_event_cb = NULL,
        .BLE_aci_gatt_write_permit_req_event_cb = NULL,
        .BLE_aci_gatt_read_permit_req_event_cb = NULL,
        .BLE_aci_gatt_read_multi_permit_req_event_cb = NULL,
        .BLE_aci_gatt_tx_pool_available_event_cb = NULL,
        .BLE_aci_gatt_server_confirmation_event_cb = NULL,
        .BLE_aci_gatt_prepare_write_permit_req_event_cb = NULL,
        .BLE_aci_l2cap_connection_update_resp_event_cb = NULL,
        .BLE_aci_l2cap_proc_timeout_event_cb = NULL,
        .BLE_aci_l2cap_connection_update_req_event_cb = NULL,
        .BLE_aci_l2cap_command_reject_event_cb = NULL,
        .BLE_aci_hal_end_of_radio_activity_event_cb = NULL,
        .BLE_aci_hal_scan_req_report_event_cb = NULL,
        .BLE_aci_hal_fw_error_event_cb = NULL,
        .BLE_HAL_VTimerTimeoutCallback_cb = NULL
};

/****************************************************************************************
 * Static Function Definition
 ****************************************************************************************/
static void APPBLE_ConnectCompleteEvent(uint8_t Status,
        uint16_t Connection_Handle,
        uint8_t Role,
        uint8_t Peer_Address_Type,
        uint8_t Peer_Address[6],
        uint16_t Conn_Interval,
        uint16_t Conn_Latency,
        uint16_t Supervision_Timeout,
        uint8_t Master_Clock_Accuracy)
{
    APPBLE_ConnSupervisionTimeout = Supervision_Timeout;

    /* keep the connection handle, we will need it for the answer */
    APPBLE_ConnHandle = Connection_Handle;

    /* Update connection timing with the one in ATS registre */
    BLE_aci_l2cap_connection_parameter_update_req(Connection_Handle, APPBLE_IntervalConnMin, APPBLE_IntervalConnMin, 0, 0xC80);

    /* exchange the ATT_MTU Size */
    BLE_aci_gatt_exchange_config(APPBLE_ConnHandle);
}

static void APPBLE_AttributeModifiedEvent(uint16_t Connection_Handle,
        uint16_t Attr_Handle,
        uint16_t Offset,
        uint16_t Attr_Data_Length,
        uint8_t Attr_Data[])
{
    /* Modification of characteristics value. Fist we check if this is a change in the command_data characteristics */
    if(((APPBLE_CharHandle + 1u) == Attr_Handle) &&
        (Connection_Handle == APPBLE_ConnHandle))
    {
        /* Check the size */
        if(sizeof(APPBLE_ReceiveBuffer) >= Attr_Data_Length)
        {
            /* Copy the Received buffer  and notify a new reception */
            memcpy(APPBLE_ReceiveBuffer, Attr_Data, Attr_Data_Length);

            /* Copy the receive size */
            APPBLE_ReceivedSize = Attr_Data_Length;

            /* Notifiy that there is a new buffer available */
            APPBLE_bIsNewReception = TRUE;
        }
    }
}


static void APPBLE_DisconnectEvent(uint8_t Status,
        uint16_t Connection_Handle,
        uint8_t Reason)
{
    uint8_t bleNameLen;
    /* disconnect event. Notify that the device shall be discoverable again */
    bleNameLen = sizeof(APPBLE_DeviceName);
    (void)BLE_SetConnectable(APPBLE_serviceUUID4Scan, sizeof(APPBLE_serviceUUID4Scan), APPBLE_DeviceName, &bleNameLen,
            APPBLE_IntervalAdvMin, APPBLE_IntervalAdvMax, APPBLE_IntervalConnMin, APPBLE_IntervalConnMax);
}

/************************************************************************************//**
 ** \brief     Receives a communication interface packet if one is present.
 ** \param     data Pointer to byte array where the data is to be stored.
 ** \param     len Pointer where the length of the packet is to be stored.
 ** \return    TRUE if a packet was received, FALSE otherwise.
 **
 ****************************************************************************************/
static BOOL APPBLE_ReceivePacket(uint8_t *data, uint8_t *len)
{
    BOOL lbIsNewPacket = FALSE;

    /* First Check if characteristics value of xcpCommandDataCharHandle has changed */
    if(TRUE == APPBLE_bIsNewReception)
    {
        /* Once the packet is given, reset the flag */
        APPBLE_bIsNewReception = FALSE;

        /* Retrieve the receive size */
        *len = APPBLE_ReceivedSize;

        /* Get the data. Start at the second byte (first one is the size) */
        memcpy(data, APPBLE_ReceiveBuffer, *len);

        /* New Packet Available */
        lbIsNewPacket = TRUE;
    }

    return lbIsNewPacket;
} /*** end of APPBLE_ReceivePacket ***/


static void APPBLE_GetInterval(uint16_t *pIntervalAdvMin, uint16_t *pIntervalAdvMax,
        uint16_t *pIntervalConnMin, uint16_t *pIntervalConnMax)
{
    /* On récupère le sinterval définit dans les regitsres, et on vérifie la validité */
    *pIntervalAdvMin = 0x2580;
    *pIntervalAdvMax = 0x25F0;
    *pIntervalConnMin = 0x190u;
    *pIntervalConnMax = 0x1F0u;

    /* Vérification du paramètre pIntervalAdv */
    if(*pIntervalAdvMin < APPBLE_MIN_ADV_INTERVAL)
    {
        *pIntervalAdvMin = APPBLE_MIN_ADV_INTERVAL;
    }

    if(*pIntervalAdvMax > APPBLE_MAX_ADV_INTERVAL)
    {
        *pIntervalAdvMax = APPBLE_MAX_ADV_INTERVAL;
    }


    /* Vérification du paramètre pIntervalConn */
    if(*pIntervalConnMin < APPBLE_MIN_CONN_INTERVAL)
    {
        *pIntervalConnMin = APPBLE_MIN_CONN_INTERVAL;
    }

    if(*pIntervalConnMax > APPBLE_MAX_CONN_INTERVAL)
    {
        *pIntervalConnMax = APPBLE_MAX_CONN_INTERVAL;
    }
}

/****************************************************************************************
 * Public Function Definitions
 ****************************************************************************************/
void APPBLE_Task(void)
{
    static uint8_t lReceivedBuffer[APPBLE_MAX_ATT_MTU_SIZE];
    static uint8_t lReceivedLen = 0;
    uint32_t signature_checksum_addr = 0;

    switch(APPBLE_State)
    {
        case APPBLE_STATE_IDLE:
            /* attente de reception d'une requte BLE */
            if(true == APPBLE_ReceivePacket(lReceivedBuffer, &lReceivedLen))
            {
                /* demande de traitement de la trame reçue */
                APPBLE_State = APPBLE_STATE_RECEIVED;
            }
            break;
        case APPBLE_STATE_RECEIVED:
            /* pour le moment, le BLE sert uniquement pour les MAJ software. On regarde si c'est
             * une demande de Reset "ATR". Si c'est le cas, on reset la carte, mais avant, on casse
             * le pattern de checksum.
             *
             * C'est nécessaire pour le moment, car le logiciel de mise a jour débarqué
             * ne voit pas le service BLE du bootloader nécessaire à la maj, car lors du scan, il voit un device
             * BLE avec une adresse qui correspond au device ayant le service APPBLE (celui-ci), et donc ne rescanne pas les services, car pense que
             * les services n'ont pas changeé et donc recharge le service APPBLE.
             *
             * Pourtant c'est bien un service différent d'APPBLE qui fait la MAJ dans le bootloader. Le logiciel de MAJ recharge donc le service
             * APPBLE lors du SCAN BLE et non le service de MAJ, tout cela parce que dans le bootloader est dans l'applicatif, le device qui contient
             * des services différents a la meme adresse.
             *
             * C'est un bug connu dans la lib QT BLE sur windows (et MAC aussi il me semble). Pour reforcer le scan, il faut eteindre et rallumer
             * le bluetooth sur la tablette/smartphone...
             *
             * C'est pour cela qu'on casse le pattern de checksum, pour que le bootmloader ne "jump" pas dans une zone applicative non intègre et
             * donc reste dans le bootloader. ça nous permet d'eteindre/rallumer le bluetooth de la tablette/smartphone afin de refaire un scan
             * et de finir la MAJ.
             */
            if(0 == strncmp((char *)lReceivedBuffer, "ATR\n", lReceivedLen))
            {
                /* On casse le pattern de checksum qui se trouve juste apres la table de vecteur */
                signature_checksum_addr = (uint32_t)((uint32_t *)__vector_table) + APPBLE_VECT_TABLE_OFFSET;

                FLASH_Unlock();
                /* On écrit 0 dans l'emplacement du checksum */
                FLASH_ProgramWord(signature_checksum_addr, 0x00);

                /* Soft Reset pour acceder au bootloader. */
                NVIC_SystemReset();
            }

            /* dans tous les cas, on a plus rien a faire, on repasse en IDLE */
            APPBLE_State = APPBLE_STATE_IDLE;
            break;
        default:
            break;
    }
}
/************************************************************************************//**
 ** \brief     Initializes the Ble service and characteristics
 ** \return    Value indicating success or error code.
 **
 ****************************************************************************************/
tBleStatus APPBLE_Init(void)
{
    tBleStatus lReturn = BLE_STATUS_SUCCESS;
    uint8_t bdaddr[6u];
    BLE_ServiceInfo_t * serviceInfo = &APPBLE_ServiceInfo;
    BLE_CharInfo_t * charInfo = &APPBLE_CharInfo;
    uint8_t bleNameLen;

    lReturn = BLE_Init(&APPBLE_BlueNRGStackInitParams, bdaddr, sizeof(bdaddr), &APPBLE_CallBackInfo);

    /* Read unique device serial number. It will be used for APPBLE_DeviceName */
    memcpy((void *)bdaddr, (void *)APPBLE_DEVICE_ADDR, sizeof(bdaddr));

    if(BLE_STATUS_SUCCESS == lReturn)
    {
        /* Add the App service to the GATT */
        lReturn = BLE_AddService(&serviceInfo, sizeof(serviceInfo)/sizeof(BLE_ServiceInfo_t *));

        if(lReturn == BLE_STATUS_SUCCESS)
        {
            /* Retrieve the ATT_MTU Size in ATS register */
            charInfo->charValueLen = 0x17;

            /* Add the App Characteristic the App service */
            lReturn = BLE_AddChar(&charInfo, sizeof(charInfo)/sizeof(BLE_CharInfo_t *));

            /* Make the device scannable and connectable */
            if(lReturn == BLE_STATUS_SUCCESS)
            {
                bleNameLen = sizeof(APPBLE_DeviceName);

                /* On recupère les intervalles de temps définis dans les registres */
                APPBLE_GetInterval(&APPBLE_IntervalAdvMin, &APPBLE_IntervalAdvMax,
                        &APPBLE_IntervalConnMin, &APPBLE_IntervalConnMax);

                /* On démarre le mode advertising */
                (void)BLE_SetConnectable(APPBLE_serviceUUID4Scan, sizeof(APPBLE_serviceUUID4Scan), APPBLE_DeviceName, &bleNameLen,
                        APPBLE_IntervalAdvMin, APPBLE_IntervalAdvMax, APPBLE_IntervalConnMin, APPBLE_IntervalConnMax);
            }
        }
    }

    return lReturn;
} /*** end of APPBLE_Init ***/


