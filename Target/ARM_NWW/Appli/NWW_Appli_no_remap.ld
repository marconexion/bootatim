/*******************************************************************************
* BlueNRG-2 generic linker file for ATOLLIC
* Main linker variables to control it are:
*
*******************************************************************************/

/*******************************************************************************
* Memory Definitions
*******************************************************************************/

_MEMORY_RAM_BEGIN_   					=  0x20000000; 
_MEMORY_RAM_SIZE_   					=  0x6000;          /* 24KB  */
_MEMORY_RAM_END_     					=  0x20005FFF; 

_MEMORY_FLASH_BEGIN_ 					= 0x10040000;  
_MEMORY_FLASH_SIZE_  					= 0x40000;          /* 256KB */ 
_MEMORY_FLASH_END_   					= 0x1007FFFF;  

_MEMORY_ROM_BEGIN_   					= 0x10000000;    
_MEMORY_ROM_SIZE_    					= 0x800;            /* 2KB */ 
_MEMORY_ROM_END_     					= 0x100007FF;  

/* Reserved the flash readout proection */
_MEMORY_FLASH_READ_PROTECTION_BEGIN_	= 0x1007FFF8;
_MEMORY_FLASH_READ_PROTECTION_SIZE_		= 8;				/* 8 Bytes */ 
_MEMORY_FLASH_READ_PROTECTION_END_		= _MEMORY_FLASH_END_;

/* Reserved for Applicatiion parameters  */
_MEMORY_FLASH_PARAM_BEGIN_				= 0x1007F800;
_MEMORY_FLASH_PARAM_SIZE_				= 0x800 - 8;		/* 2KB - 8*/ 
_MEMORY_FLASH_PARAM_END_				= _MEMORY_FLASH_END_ - _MEMORY_FLASH_READ_PROTECTION_SIZE_;

/* Reserved for BTLE stack non volatile memory */
_MEMORY_FLASH_NVM_BEGIN_				= 0x1007E800;
_MEMORY_FLASH_NVM_SIZE_					= (4*1024);			/* 4KB */ 
_MEMORY_FLASH_NVM_END_					= _MEMORY_FLASH_PARAM_BEGIN_ - 1;

RAM_VECT_TABLE_OFFSET					= (0xc0);
  /*
     Memorie Mapping for bootloader and user app
     +-----------------------+ 0x20005FFF
     | RAM (24K-4)           |
     +-----------------------+ 0x20000004
     |                       |
     |                       |
     +-----------------------+ 0x10080000
     |                       |
     | FLASH PARAM (2K)      |
     +-----------------------+ 0x1007F800
     |                       |
     | NVM(4K)               |
     +-----------------------+ 0x1007E800
     |                       |
     |                       |
     |                       |
     | FLASH (256K)          |
     |                       |
     |                       |
     +-----------------------+ 0x10040000
     |                       |
     +-----------------------| 0x100007FF
     | ROM (2K)              |
     +-----------------------+ 0x10000000
  */

MEMORY_RAM_APP_OFFSET = DEFINED(MEMORY_RAM_APP_OFFSET) ? (MEMORY_RAM_APP_OFFSET) : (0x2CC) ;
/* For the Application firmware, the App flash characteristics are : */
MEMORY_FLASH_APP_BEGIN = _MEMORY_FLASH_BEGIN_;
MEMORY_FLASH_APP_SIZE = _MEMORY_FLASH_SIZE_ - _MEMORY_FLASH_NVM_SIZE_  - _MEMORY_FLASH_PARAM_SIZE_;


EXTERNAL_BLE_STACK_FLASH_OFFSET = DEFINED(EXTERNAL_BLE_STACK_FLASH_OFFSET) ? EXTERNAL_BLE_STACK_FLASH_OFFSET : 0;
cmd_call_table = _MEMORY_FLASH_BEGIN_ + 0xC0 + EXTERNAL_BLE_STACK_FLASH_OFFSET;
ev_call_table = 0x200002cc;


/* Entry Point */
ENTRY(RESET_HANDLER)


/* Generate a link error if heap and stack don't fit into RAM */
_Min_Heap_Size = 0x0;      /* required amount of heap  */
_Min_Stack_Size = 0xC00; /* required amount of stack */

/* Specify the memory areas */
MEMORY
{
  REGION_RAM (xrw)         		: ORIGIN = _MEMORY_RAM_BEGIN_, LENGTH = _MEMORY_RAM_SIZE_
  REGION_FLASH (rx)  			: ORIGIN =  MEMORY_FLASH_APP_BEGIN, LENGTH = MEMORY_FLASH_APP_SIZE
  REGION_NVM (rx)          		: ORIGIN = _MEMORY_FLASH_NVM_BEGIN_, LENGTH = _MEMORY_FLASH_NVM_SIZE_
  REGION_ROM (rx)          		: ORIGIN = _MEMORY_ROM_BEGIN_, LENGTH = _MEMORY_ROM_SIZE_
}

/* Define output sections */
SECTIONS
{  
  /* The startup code goes first into FLASH */
  .intvec (ORIGIN(REGION_FLASH)) :
  {
    . = ALIGN(4);
    
    KEEP(*(.intvec)) /* Startup code */
    
    . = ALIGN(4);
  } >REGION_FLASH

  .checksum (ORIGIN(REGION_FLASH) + RAM_VECT_TABLE_OFFSET) :
  {
    . = ALIGN(4);
    KEEP(*(.checksum))
    
    . = ALIGN(4);
  } >REGION_FLASH
  
  /* The program code and other data goes into FLASH */
  .text :
  {
    . = ALIGN(4);

    KEEP(*(.cmd_call_table))
    *(.text)           /* .text sections (code) */
    *(.text*)          /* .text* sections (code) */
    *(i.*)             /* i.* sections (code) */
    *(.rodata)         /* .rodata sections (constants, strings, etc.) */
    *(.rodata*)        /* .rodata* sections (constants, strings, etc.) */
    *(.constdata)
    *(.glue_7)         /* glue arm to thumb code */
    *(.glue_7t)        /* glue thumb to arm code */
    KEEP(*(.ble_api))

    . = ALIGN(4);
     _etext = .;
  } >REGION_FLASH

  /* used by the startup to initialize data */
  _sidata = LOADADDR(.data);
  /* used by the startup to initialize data */
  _sidata2 = LOADADDR(.ram_preamble);


  /* RAM preamble no init */
  .ram_preamble_noinit (0x20000000/* + RAM_VECT_TABLE_OFFSET*/) (NOLOAD) :
  {
    . = ALIGN(4);
    . = 0x04 ; /* There is a waste of RAM here */
    KEEP(*(.ota_sw_activation))
  }  >REGION_RAM

  /* RAM preamble initialized */
  .ram_preamble (0x20000008/* + RAM_VECT_TABLE_OFFSET*/)/*(NOLOAD)*/ :
  {
    _sdata2 = .;        /* create a global symbol at data start */
    /* This is used by the startup in order to initialize the .bss section */
    KEEP(*(.savedMSP))
    . = 0x04 ;
    KEEP(*(.wakeupFromSleepFlag))
    . = 0x08 ;
    KEEP(*(.app_base))
    . = 0x0C ;
    KEEP(*(.flash_sw_lock))
    . = 0x28 ; 
    KEEP(*(.__blueflag_RAM))
      _edata2 = .;        /* create a global symbol at data end (.__crash_RAM is skipped since it must not be initialized) */
  }  >REGION_RAM AT> REGION_FLASH

  /* RAM preamble no init */
  .ram_preamble_noinit2 (0x20000034/* + RAM_VECT_TABLE_OFFSET*/)(NOLOAD) :
  {
    KEEP(*(.__crash_RAM))
  }  >REGION_RAM
  /* Uninitialized data section */
   
  .bss.blueRAM (0x200000C0/* + RAM_VECT_TABLE_OFFSET*/):
  {
    . = ALIGN(4);
    _sbssblue = .;         /* define a global symbol at .bss.blueRAM start */
    KEEP(*(.bss.__blue_RAM))
    . = ALIGN(4);
    _ebssblue = .;         /* define a global symbol at .bss.blueRAM end */
    
    . = 0x20c + MEMORY_RAM_APP_OFFSET - 0x2cc;
    
  } >REGION_RAM  
  
    /* Data section that will not be initialized to any value. */
  .noinit (NOLOAD):
  {
    . = ALIGN(4);
    *(.noinit)
    . = ALIGN(4);
  } >REGION_RAM
  
  .bss  :
  {
    . = ALIGN(4);
    _sbss = .;         /* define a global symbol at bss start */
    *(.bss.ev_call_table)
    *(.bss)
    *(.bss*)
    *(COMMON)
    . = ALIGN(4);
    _ebss = .;         /* define a global symbol at bss end */
  } >REGION_RAM
  
  /* Initialized data sections goes into RAM, load LMA copy after code */
  .data :
  {
    . = ALIGN(4);
    _sdata = .;        /* create a global symbol at data start */
    *(.data)           /* .data sections */
    *(.data*)          /* .data* sections */

    . = ALIGN(4);
    _edata = .;        /* define a global symbol at data end */
  } >REGION_RAM AT> REGION_FLASH


/**
* The last 4KB sector of FLASH is reserved for firmware to use. The BLE Host
* stores its security database in this area. The linker needs to make sure this
* 4KB sector is left empty.
*/
  BLOCK_STACKLIB_FLASH_DATA (_MEMORY_FLASH_NVM_BEGIN_) (NOLOAD) :
  {
    . = ALIGN(2048);
    
    KEEP(*(.noinit.stacklib_flash_data))
    KEEP(*(.noinit.stacklib_stored_device_id_data))
    
  } >REGION_NVM

  /* This is to emulate place at end of IAR linker */
  CSTACK (ORIGIN(REGION_RAM) + LENGTH(REGION_RAM) - _Min_Stack_Size) (NOLOAD) :
  {
    . = ALIGN(4);
    _estack = . + _Min_Stack_Size ;         /* define a global symbol at bss end */
    . = ALIGN(4);
  } > REGION_RAM

  
  .rom_info (NOLOAD) :
  {
    . = ALIGN(4);
    KEEP(*(.rom_info)) 
    . = ALIGN(4);
  } >REGION_ROM
  
  /* Remove information from the standard libraries */
  /DISCARD/ :
  {
    libc.a ( * )
    libm.a ( * )
    libgcc.a ( * )
  }

  .ARM.attributes 0 : { *(.ARM.attributes) }
}
