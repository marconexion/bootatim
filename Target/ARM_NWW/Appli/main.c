/*
    / \	    _   |_|
   / _ \  _| |_  _  _____
  / /_\ \(_   _)| ||	 |
 / _____ \ | |  | || | | |
/_/	    \_\\__) |_||_|_|_|
	(C)2018 Atim

Description: Application

License:

Maintainer: Youen Lebret

*/

/****************************************************************************************
* Include files
****************************************************************************************/
#include "ble.h"
#include "hal_types.h"
#include "system_BlueNRG.h"
#include "clock.h"
#include "uart.h"
#include "app_ble.h"
#include <string.h>

/****************************************************************************************
* Define
****************************************************************************************/
/** Periodic send of frame for Test */
#define SEND_C_PERIOD           1000u
/** Maximum authorized lenght for Rx Reception */
#define RX_MAX_DATA             255u
/** ATIM reset command  */
#define ATIM_RESET_COMMAND      "ATR"

#define PADDING_SIZE            0x2C000u


/****************************************************************************************
* Function prototypes
****************************************************************************************/
/* reception packet */
static uint8_t xcpCtoReqPacket[RX_MAX_DATA];

static void Init(void);
static void Reprog_Wait(void);


/************************************************************************************//**
** \brief     This is the entry point for the bootloader application and is called
**            by the reset interrupt vector after the C-startup routines executed.
** \return    Program return code.
**
****************************************************************************************/
int main(void)
{
  tClockTime currentTime = 0;
  static tClockTime sendTime = 0;

  /* Initialization */
  Init();

  /* Initialize the UART. */
  UART_InitExt();

  UART_TransmitPacket((uint8_t *)"App Starting\n", sizeof("App Starting\n"));

  /* Start the infinite program loop. */
  while (1)
  {
    /* Envoie toutes les secondes */
    currentTime = Clock_Time();

    if((currentTime - sendTime) >= SEND_C_PERIOD)
    {
        UART_TransmitPacket((uint8_t *)"App is Now Running\n", sizeof("App is Now Running"));
        sendTime = currentTime;
    }

    /* wait for UART reset */
    Reprog_Wait();

    /* BlueNRG-1 stack tick */
    BLE_StackTick();
  }
} /*** end of main ***/

/************************************************************************************//**
** \brief     Initializes the microcontroller.
** \return    none.
**
****************************************************************************************/
static void Init(void)
{
  /* System Init */
  SystemInit();

  /* Clock Init */
  Clock_Init();

  /* Initialization of Stack Ble and App Service, Characteristic */
  (void)APPBLE_Init();
} /*** end of Init ***/


/************************************************************************************//**
** \brief     Wait for a CONNECT command to start a firmware update
** \return    none.
**
****************************************************************************************/
static void Reprog_Wait(void)
{
  uint8_t xcpPacketLen;

  if (UART_ReceivePacket(&xcpCtoReqPacket[0], &xcpPacketLen) == TRUE)
  {
    /* Check if it is a ATR request*/
    if(0 == strncmp((char *)xcpCtoReqPacket, ATIM_RESET_COMMAND, sizeof(ATIM_RESET_COMMAND) - 1))
    {
      /* Perform a Soft Reset to start the update with the bootloader */
      NVIC_SystemReset();
    }
  }
} /*** end of Init ***/


/*********************************** end of main.c *************************************/
